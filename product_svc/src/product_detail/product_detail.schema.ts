export const productDetailSchema = {
  params: {
    type: "object",
    properties: {
      id: { type: "string" },
    },
  },
};

import { FastifyInstance, FastifyRegisterOptions } from "fastify";
import { productDetailController } from "./product_detail.controller";
import { productDetailSchema } from "./product_detail.schema";

export function productDetailRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  fastify.get("/:id", { schema: productDetailSchema }, productDetailController);

  done();
}

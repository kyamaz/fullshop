import { Prisma, PrismaClient } from ".prisma/client";
import { ErrorCustom, throwCustomError } from "@shop/shared_svc";

export class ProductDetailRepository {
  constructor(private readonly prismaProduct: PrismaClient["product"]) {}
  getProductById(id: string) {
    return this.prismaProduct
      .findUnique({
        where: {
          uuid: id,
        },
        include: { sel_product_quantity: true },
      })

      .then((item) => {
        if (!item) {
          throwCustomError("product not found", "Not found", 404);
        }
        return item;
      })
      .catch((e: Prisma.PrismaClientKnownRequestError | ErrorCustom) => {
        if (e instanceof Prisma.PrismaClientKnownRequestError) {
          console.warn(
            "[getProductById]: prisma error with code: ",
            e.code,
            e.clientVersion
          );

          if (e.code === "P2023") {
            throwCustomError("invalid product id", "Bad request", 400);
          }
          console.warn(
            "[getProductById]: customise msg with this code",
            e.code
          );
          throwCustomError("something went wrong", "Bad request", 400);
        }
        throw e;
      });
  }
}

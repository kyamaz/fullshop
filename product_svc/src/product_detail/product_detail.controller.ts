import { FastifyReply, FastifyRequest } from "fastify";
import { productSvc } from "../app";

type ProductDetailRequest = FastifyRequest<{
  Params: { id: string };
}>;

export async function productDetailController(
  req: ProductDetailRequest,
  reply: FastifyReply
) {
  try {
    const {
      params: { id },
    } = req;
    const data: unknown = await productSvc.getProductById(id); //TODO ugly ditch Prisma data

    reply.status(200).send({ data });
  } catch (error: unknown) {
    console.error(error);
    throw error;
  }
}

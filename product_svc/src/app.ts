import { PrismaClient } from ".prisma/client";
import cors from "@fastify/cors";
import { setApiErrorResp, throwCustomError } from "@shop/shared_svc";
import fastify, { FastifyRequest } from "fastify";
import { productDetailRoute } from "./product_detail/product_detail.routes";
import { ProductDetailRepository } from "./product_detail/product_detail.service";
import { productListRoute } from "./product_list/product_list.routes";
import { ProductListRepository } from "./product_list/product_list.svc";

const server = fastify({
  logger: false,
});

server.register(cors, {
  origin: true, //TODO dev only
  credentials: true,
});

// global error response handler

server.setErrorHandler(function (err, _request, reply) {
  //  console.warn(error);
  const { status, message, errorName, error } = setApiErrorResp(err);
  reply.status(status).send({ message, statusCode: status, errorName, error });
});

//ROUTES
server.register(productListRoute, { prefix: "/svc/product" });
server.register(productDetailRoute, { prefix: "/svc/product" });

server.get("*", function (req: FastifyRequest, res: any) {
  console.warn(req.url, "product svc url not found");

  throwCustomError(req.url.concat(" not found"), "Not found", 404);
});

const prisma = new PrismaClient();

const productSvc = new ProductDetailRepository(prisma.product);
const productListSvc = new ProductListRepository(prisma.product);

export { server, prisma, productSvc, productListSvc };

import { FastifyInstance, FastifyRegisterOptions } from "fastify";
import { productListController } from "./product_list.controller";
import { productListschema } from "./product_list.schema";
export function productListRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  fastify.get(
    "/",
    { schema: productListschema, attachValidation: true },
    productListController
  );

  done();
}

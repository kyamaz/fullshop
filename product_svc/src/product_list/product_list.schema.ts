export const productListschema = {
  querystring: {
    type: "object",
    properties: {
      skip: { type: "number" },
      take: { type: "number" },
    },
  },
};

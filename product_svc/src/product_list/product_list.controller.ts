import { throwCustomError } from "@shop/shared_svc";
import Shop from "@shop/shared_types";
import { FastifyReply, FastifyRequest } from "fastify";
import { productListSvc } from "../app";

type ProductsRequest = FastifyRequest<{
  Querystring?: { skip?: number; take?: number };
  Params: { id: string };
}>;

export async function productListController(
  { query }: ProductsRequest,
  reply: FastifyReply
) {
  const { skip, take } = query ?? { skip: 0, take: 0 };
  const derivedSkip = (skip ?? 0) * (take ?? 0);

  try {
    const data: unknown = await productListSvc.getList(derivedSkip, take); //TODO ugly ditch Prisma data

    reply.status(200).send({
      data,
      query,
    } as Shop.ProductCollectionRepositoryResponse<Shop.ProductModel>);
  } catch (error) {
    throwCustomError("Something went wrong", "Internal Error", 500);
  }
}

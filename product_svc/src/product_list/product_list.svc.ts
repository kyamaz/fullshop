import { PrismaClient } from ".prisma/client";

export class ProductListRepository {
  constructor(private readonly prismaProduct: PrismaClient["product"]) {}

  getList(skip: number = 0, take: number = 30) {
    return this.prismaProduct
      .findMany({
        skip,
        take,
        orderBy: {
          id: "asc",
        },
        include: { sel_product_quantity: true },
      })
      .catch((e) => {
        throw e;
      })
      .then((collection) => {
        return collection.map((p) => {
          const { sel_product_quantity, ...tail } = p;
          return {
            ...tail,
            num_available: sel_product_quantity?.num_available,
          };
        });
      });
  }
}

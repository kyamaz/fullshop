"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const client_1 = require(".prisma/client");
const faker_1 = require("@faker-js/faker");
const prisma = new client_1.PrismaClient();
function getRandomInt() {
    const min = Math.ceil(1);
    const max = Math.floor(5);
    return Math.floor(Math.random() * (max - min)) + min;
}
const fakerProduct = (id) => ({
    uuid: faker_1.faker.datatype.uuid(),
    type: "physical",
    name: faker_1.faker.commerce.product(),
    image_main_url: faker_1.faker.image.fashion(640, 480, true),
    image_alt_url: Array(getRandomInt).map((_) => faker_1.faker.image.fashion(280, 360, true)),
    description_long: faker_1.faker.commerce.productDescription(),
    price_without_taxes: faker_1.faker.datatype.number({
        max: 100,
        min: 1,
        precision: 0.01,
    }),
    ratings: faker_1.faker.datatype.number({ max: 5, min: 0, precision: 0.1 }),
    number_ratings: faker_1.faker.datatype.number({ min: 10, max: 900 }),
    quantity_id_fk: null,
    personnalized_content: null,
    category_id_fk: null,
    weight_gram: 500,
    height_cm: 1000,
});
const fakerQuantity = (uuid) => {
    const mockNum = faker_1.faker.datatype.number({ min: 0, max: 29 });
    return {
        num_available: mockNum === 5 ? null : mockNum,
        product_id_fk: uuid,
    };
};
async function main() {
    var _a;
    const fakerRounds = 150;
    for (let i = 0; i < fakerRounds; i++) {
        const product = await prisma.product.create({ data: fakerProduct(i) });
        await prisma.quantity.create({
            data: fakerQuantity(product.uuid),
        });
    }
    const quantityColl = await prisma.quantity.findMany({});
    for (let q of quantityColl) {
        await prisma.product.update({
            where: {
                uuid: (_a = q.product_id_fk) !== null && _a !== void 0 ? _a : undefined,
            },
            data: {
                quantity_id_fk: q.id,
            },
        });
    }
}
main()
    .then(async () => {
    await prisma.$disconnect();
})
    .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
});
//# sourceMappingURL=seed.js.map
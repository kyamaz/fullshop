import { PrismaClient } from ".prisma/client";
import { faker } from "@faker-js/faker";

const prisma = new PrismaClient();
function getRandomInt() {
  const min = Math.ceil(1);
  const max = Math.floor(5);
  return Math.floor(Math.random() * (max - min)) + min;
}
const fakerProduct = (id: number): any => ({
  uuid: faker.datatype.uuid(),
  type: "physical",
  name: faker.commerce.product(),
  image_main_url: faker.image.fashion(640, 480, true),
  image_alt_url: Array(getRandomInt).map((_) =>
    faker.image.fashion(280, 360, true)
  ),
  description_long: faker.commerce.productDescription(),
  price_without_taxes: faker.datatype.number({
    max: 100,
    min: 1,
    precision: 0.01,
  }),
  ratings: faker.datatype.number({ max: 5, min: 0, precision: 0.1 }),
  number_ratings: faker.datatype.number({ min: 10, max: 900 }),
  quantity_id_fk: null,
  personnalized_content: null,
  category_id_fk: null,
  weight_gram: 500,
  height_cm: 1000,
});

const fakerQuantity = (uuid: string): any => {
  const mockNum = faker.datatype.number({ min: 0, max: 29 });
  return {
    num_available: mockNum === 5 ? null : mockNum,
    product_id_fk: uuid,
  };
};

async function main() {
  const fakerRounds = 150;

  for (let i = 0; i < fakerRounds; i++) {
    const product = await prisma.product.create({ data: fakerProduct(i) });
    await prisma.quantity.create({
      data: fakerQuantity(product.uuid),
    });
  }

  const quantityColl = await prisma.quantity.findMany({});

  for (let q of quantityColl) {
    await prisma.product.update({
      where: {
        uuid: q.product_id_fk ?? undefined,
      },
      data: {
        quantity_id_fk: q.id,
      },
    });
  }
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });

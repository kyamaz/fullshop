/*
  Warnings:

  - You are about to alter the column `ratings` on the `Product` table. The data in that column could be lost. The data in that column will be cast from `DoublePrecision` to `Decimal(65,30)`.

*/
-- AlterTable
ALTER TABLE "Product" ALTER COLUMN "ratings" SET DATA TYPE DECIMAL(65,30);

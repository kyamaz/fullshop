/*
  Warnings:

  - The `price_without_taxes` column on the `Product` table would be dropped and recreated. This will lead to data loss if there is data in the column.
  - You are about to alter the column `ratings` on the `Product` table. The data in that column could be lost. The data in that column will be cast from `Decimal(3,0)` to `Decimal(3,1)`.

*/
-- AlterTable
ALTER TABLE "Product" DROP COLUMN "price_without_taxes",
ADD COLUMN     "price_without_taxes" MONEY,
ALTER COLUMN "ratings" SET DATA TYPE DECIMAL(3,1);

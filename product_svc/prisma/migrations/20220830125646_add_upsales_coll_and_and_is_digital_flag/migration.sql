/*
  Warnings:

  - Added the required column `is_digital` to the `Quantity` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Product" ADD COLUMN     "upsale_product" INTEGER[] DEFAULT ARRAY[]::INTEGER[];

-- AlterTable
ALTER TABLE "Quantity" ADD COLUMN     "is_digital" BOOLEAN NOT NULL;

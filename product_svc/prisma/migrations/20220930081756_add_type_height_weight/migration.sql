/*
  Warnings:

  - You are about to drop the column `is_digital` on the `Quantity` table. All the data in the column will be lost.
  - Added the required column `type` to the `Product` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "ProductType" AS ENUM ('physical', 'digital', 'subscription');

-- AlterTable
ALTER TABLE "Product" ADD COLUMN     "height_cm" INTEGER,
ADD COLUMN     "type" "ProductType" NOT NULL,
ADD COLUMN     "weight_gram" INTEGER;

-- AlterTable
ALTER TABLE "Quantity" DROP COLUMN "is_digital";

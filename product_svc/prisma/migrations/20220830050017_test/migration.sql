/*
  Warnings:

  - You are about to alter the column `ratings` on the `Product` table. The data in that column could be lost. The data in that column will be cast from `Decimal(65,30)` to `DoublePrecision`.

*/
-- AlterTable
ALTER TABLE "Product" ALTER COLUMN "ratings" SET DATA TYPE DOUBLE PRECISION;

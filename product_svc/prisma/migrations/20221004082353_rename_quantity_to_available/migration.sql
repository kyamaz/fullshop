/*
  Warnings:

  - You are about to drop the column `quantity` on the `Quantity` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "Quantity" DROP COLUMN "quantity",
ADD COLUMN     "num_available" INTEGER;

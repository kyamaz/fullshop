-- AlterTable
ALTER TABLE "Product" ALTER COLUMN "category_id_fk" DROP NOT NULL,
ALTER COLUMN "category_id_fk" DROP DEFAULT;
DROP SEQUENCE "Product_category_id_fk_seq";

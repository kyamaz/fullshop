-- CreateTable
CREATE TABLE "Product" (
    "id" SERIAL NOT NULL,
    "uuid" UUID NOT NULL,
    "name" VARCHAR(700) NOT NULL,
    "personnalized_content" TEXT,
    "category_id_fk" SERIAL,
    "price_without_taxes" REAL,
    "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "description_long" TEXT,
    "image_main_url" CHAR(500) NOT NULL,
    "image_alt_url" CHAR(500)[],
    "number_ratings" SMALLINT DEFAULT 0,
    "quantity_id_fk" INTEGER,
    "ratings" DECIMAL(3,0) DEFAULT 0,
    "updated_at" TIMESTAMPTZ(6) NOT NULL,

    CONSTRAINT "Product_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Quantity" (
    "id" SERIAL NOT NULL,
    "quantity" INTEGER NOT NULL DEFAULT 0,
    "product_id_fk" UUID,

    CONSTRAINT "Quantity_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "Product_uuid_key" ON "Product"("uuid");

-- CreateIndex
CREATE UNIQUE INDEX "Product_quantity_id_fk_key" ON "Product"("quantity_id_fk");

-- CreateIndex
CREATE INDEX "Product_quantity_id_fk_idx" ON "Product"("quantity_id_fk");

-- CreateIndex
CREATE UNIQUE INDEX "Quantity_product_id_fk_key" ON "Quantity"("product_id_fk");

-- CreateIndex
CREATE INDEX "Quantity_product_id_fk_idx" ON "Quantity"("product_id_fk");

-- AddForeignKey
ALTER TABLE "Product" ADD CONSTRAINT "Product_quantity_id_fk_fkey" FOREIGN KEY ("quantity_id_fk") REFERENCES "Quantity"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Quantity" ADD CONSTRAINT "Quantity_product_id_fk_fkey" FOREIGN KEY ("product_id_fk") REFERENCES "Product"("uuid") ON DELETE SET NULL ON UPDATE CASCADE;

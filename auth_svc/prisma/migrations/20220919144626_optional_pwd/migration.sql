-- CreateTable
CREATE TABLE "user" (
    "uuid" UUID NOT NULL,
    "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ(6) NOT NULL,
    "email" TEXT NOT NULL,
    "order_id_fk" UUID,
    "pwd" TEXT,
    "address1" TEXT,
    "address2" TEXT,
    "city" TEXT,
    "postalcode" TEXT,
    "country" TEXT,
    "phone" TEXT,
    "fax" TEXT,
    "billing_address1" TEXT,
    "billing_address2" TEXT,
    "billing_city" TEXT,
    "billing_postalcode" TEXT,
    "billing_country" TEXT,
    "shipping_address1" TEXT,
    "shipping_address2" TEXT,
    "shipping_city" TEXT,
    "shipping_postalcode" TEXT,
    "shipping_country" TEXT,

    CONSTRAINT "user_pkey" PRIMARY KEY ("uuid")
);

-- CreateIndex
CREATE UNIQUE INDEX "user_email_key" ON "user"("email");

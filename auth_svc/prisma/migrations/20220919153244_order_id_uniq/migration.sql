/*
  Warnings:

  - A unique constraint covering the columns `[order_id_fk]` on the table `user` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "user_order_id_fk_key" ON "user"("order_id_fk");

/*
  Warnings:

  - A unique constraint covering the columns `[order_id_fk,email]` on the table `user` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "user_uuid_email_key";

-- CreateIndex
CREATE UNIQUE INDEX "user_order_id_fk_email_key" ON "user"("order_id_fk", "email");

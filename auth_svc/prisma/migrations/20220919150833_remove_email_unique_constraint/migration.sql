/*
  Warnings:

  - A unique constraint covering the columns `[uuid,email]` on the table `user` will be added. If there are existing duplicate values, this will fail.

*/
-- DropIndex
DROP INDEX "user_email_key";

-- CreateIndex
CREATE UNIQUE INDEX "user_uuid_email_key" ON "user"("uuid", "email");

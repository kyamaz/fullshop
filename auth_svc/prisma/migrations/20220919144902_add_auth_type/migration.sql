/*
  Warnings:

  - Added the required column `auth_type` to the `user` table without a default value. This is not possible if the table is not empty.

*/
-- CreateEnum
CREATE TYPE "AuthType" AS ENUM ('ANONYMOUS', 'AUTH');

-- AlterTable
ALTER TABLE "user" ADD COLUMN     "auth_type" "AuthType" NOT NULL;

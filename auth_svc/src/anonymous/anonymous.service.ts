import { throwCustomError } from "@shop/shared_svc/build";
import { prisma } from "../app";

export function createAnonymousOrder(
  { email, order_id }: { email: string; order_id: string },
  jwtSign: Function
) {
  return prisma.user
    .create({
      data: { email, order_id_fk: order_id, auth_type: "ANONYMOUS" },
    })

    .then((data) => {
      return jwtSign({
        uuid: data.uuid,
        email: data.email,
        order_id: data.order_id_fk,
      });
    })
    .catch((e) => {
      const msg =
        e.code === "P2002" ? "order already exists" : "something went wrong";
      throwCustomError(msg, e.code, 400);
    });
}

export function findAnonymousOrder(
  { email, order_id }: { email; order_id },
  jwtSign: Function
) {
  return prisma.user
    .findFirstOrThrow({
      where: { email, order_id_fk: order_id, auth_type: "ANONYMOUS" },
    })
    .then((data) => {
      return jwtSign({
        uuid: data.uuid,
        email: data.email,
        order_id: data.order_id_fk,
      });
    })

    .catch((e) => {
      throwCustomError(
        e?.meta?.message ?? "findAnonymousOrder failed",
        e.code,
        400
      );
    });
}

export function getAnonymousDetail(
  { email, order_id }: { email; order_id },
  jwtSign: Function
) {
  return prisma.user
    .findFirstOrThrow({
      where: { email, order_id_fk: order_id, auth_type: "ANONYMOUS" },
    })
    .then((data) => {
      return data;
    })

    .catch((e) => {
      throwCustomError(e.meta?.message ?? "failed get order", e.code, 400);
    });
}

import { throwCustomError } from "@shop/shared_svc";
import { FastifyReply, FastifyRequest } from "fastify";
import { server } from "./../app";
import {
  createAnonymousOrder,
  findAnonymousOrder,
  getAnonymousDetail,
} from "./anonymous.service";
type AnonymousAuthRequest = FastifyRequest<{
  Body: { email: string; order_id: string };
}>;

export function signupController(
  request: AnonymousAuthRequest,
  reply: FastifyReply
) {
  const { email, order_id } = request.body;

  const fn = reply.jwtSign.bind(reply);

  return createAnonymousOrder({ email, order_id }, fn)
    .then((token) => {
      return reply
        .status(201)
        .setCookie("token", token, {
          path: "/",
          secure: false, // send cookie over HTTPS only
          httpOnly: true,
          sameSite: "strict", // alternative CSRF protection
        })
        .send({ token });
    })
    .catch((e) => {
      return e;
    });
}

export function signinController(
  request: AnonymousAuthRequest,
  reply: FastifyReply
) {
  const { email, order_id } = request.body;

  const fn = reply.jwtSign.bind(reply);
  console.debug(email, order_id);
  return findAnonymousOrder({ email, order_id }, fn)
    .then((token) => {
      return reply
        .status(200)

        .setCookie("token", token, {
          path: "/",
          secure: false, // send cookie over HTTPS only
          httpOnly: true,
          sameSite: "strict", // alternative CSRF protection
        })
        .send({ token });
    })
    .catch((e) => {
      console.debug(e);
      return e;
    });
}

export function decodeController(
  request: AnonymousAuthRequest,
  reply: FastifyReply
) {
  const token = request.headers.cookie;

  if (!token) {
    return throwCustomError("no token", "unauthoriezd", 401);
  }

  const [_, tk] = (token as string).split("=");
  const decoded = server.jwt.decode(tk);
  if (!decoded) {
    return throwCustomError("no token", "unauthoriezd", 401);
  }
  const { email, order_id } = decoded as any;
  const fn = reply.jwtSign.bind(reply);

  return getAnonymousDetail({ email, order_id }, fn)
    .then((data) => {
      return reply
        .status(200)

        .setCookie("token", tk as string, {
          path: "/",
          secure: false, // send cookie over HTTPS only
          httpOnly: true,
          sameSite: "strict", // alternative CSRF protection
        })
        .send({ data });
    })
    .catch((e) => {
      return e;
    });
}

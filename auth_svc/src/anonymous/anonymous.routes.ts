import { FastifyInstance, FastifyRegisterOptions } from "fastify";
import {
  decodeController,
  signinController,
  signupController,
} from "./anonymous.controller";
import { decodeSchema, signinSchema, signupSchema } from "./anonymous.schema";

export function anonymousRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  fastify.get(
    "current",
    { schema: decodeSchema, preHandler: [fastify.authenticate] },
    decodeController
  );
  fastify.post("signup", { schema: signupSchema }, signupController);
  fastify.post("signin", { schema: signinSchema }, signinController);

  done();
}

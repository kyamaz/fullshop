export const signupSchema = {
  body: {
    type: "object",
    required: ["email", "order_id"],

    properties: {
      email: {
        type: "string",
      },
      order_id: {
        type: "string",
      },
    },
  },
};
export const signinSchema = {
  body: {
    type: "object",
    required: ["email", "order_id"],

    properties: {
      email: {
        type: "string",
      },
      order_id: {
        type: "string",
      },
    },
  },
};

export const decodeSchema = {
  headers: {
    type: "object",
    required: ["cookie"],

    properties: {
      cookie: {
        type: "string",
      },
    },
  },
};

import { server } from "./app";

async function start() {
  try {
    /*     if (!process?.env?.jwt) {
      return new Error("env not set");
    }
    if (!process?.env?.MONGO_URI) {
      return new Error("mongo uri not set");
    }
    if (!process?.env?.NATS_URL) {
      return new Error("NATS_URL not set");
    }
    if (!process?.env?.NATS_CLUSTER_ID) {
      return new Error("NATS_CLUSTER_IDnot set");
    }
    if (!process?.env?.NATS_CLIENT_ID) {
      return new Error("NATS_CLIENT_ID not set");
    } */

    server.listen({ port: 5030, host: "0.0.0.0" }, (err, address) => {
      if (err) {
        console.error(err);
        process.exit(1);
      }
      console.log(`cart server listening at ${address} `);
    });
  } catch (e) {
    throw e;
  }
}
start();

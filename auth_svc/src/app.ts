import { PrismaClient } from ".prisma/client";
import cors from "@fastify/cors";
import {
  authPlugin,
  setApiErrorResp,
  throwCustomError,
} from "@shop/shared_svc/build/index";
import fastify, { FastifyRequest } from "fastify";
import fp from "fastify-plugin";
import { anonymousRoute } from "./anonymous/anonymous.routes";
declare module "fastify" {
  interface AuthReply {
    setCookie: Function;
    jwtSign: Function;
  }
  interface FastifyInstance {
    authenticate: any;
    jwt: any;
  }

  interface FastifyReply extends AuthReply {}
}

const server = fastify({
  logger: false,
});

server.register(cors, {
  origin: true, //TODO dev only
  credentials: true,
});
server.register(fp(authPlugin));

// global error response handler

server.setErrorHandler(function (err, _request, reply) {
  //  console.warn(error);
  const { status, message, errorName, error } = setApiErrorResp(err);
  reply.status(status).send({ message, statusCode: status, errorName, error });
});

//TODO  README https://medium.com/@fastifyjs/fastify-v4-ga-59f2103b5f0e
//ROUTES

server.register(anonymousRoute, { prefix: "/svc/anonymous/" });

server.get("*", function (req: FastifyRequest, res: any) {
  console.warn(req.url, "auth svc url not found");
  throwCustomError(req.url.concat(" not found"), "Not found", 404);
});

const prisma = new PrismaClient();

export { server, prisma };

import { FastifyInstance, FastifyRegisterOptions } from "fastify";
import {
  countryCreateController,
  countryFetchController,
} from "./country.controller";
import { countryCreateSchema } from "./country.schema";

export function countryRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  fastify.get(
    "/country",
    {
      attachValidation: false,
    },
    countryFetchController
  );

  fastify.post(
    "/country",
    {
      schema: countryCreateSchema,
    },
    countryCreateController
  );

  done();
}

export const countryCreateSchema = {
  body: {
    type: "object",
    required: ["name"],

    properties: {
      name: {
        type: "string",
      },
    },
  },
};

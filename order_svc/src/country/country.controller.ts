import { FastifyReply, FastifyRequest } from "fastify";
import { countrySvcCreate, countrySvcFetch } from "./country.svc";

type CountryRequest = FastifyRequest<{
  Body: {
    name: string;
  };
}>;
export function countryFetchController(
  request: CountryRequest,
  reply: FastifyReply
) {
  return countrySvcFetch().then((data) => {
    return reply.status(200).send({ data: data });
  });
}

export function countryCreateController(
  request: CountryRequest,
  reply: FastifyReply
) {
  const payload = request.body;

  return countrySvcCreate(payload).then((data) => {
    reply.status(201).send({ data });
  });
}

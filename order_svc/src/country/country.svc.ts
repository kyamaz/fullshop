import { prisma } from "../app";

export function countrySvcFetch() {
  return prisma.country.findMany();
}

export function countrySvcCreate(payload: { name: string }) {
  return prisma.country.create({
    data: payload,
  });
}

import { prisma } from "../app";
import { OrderAddressPayload } from "../model/index";

export function billingAddressSvcGet(id: string) {
  return prisma.billingAddress.findUniqueOrThrow({
    where: {
      id: parseInt(id),
    },
  });
}

export function billingAddressSvcCreate(payload: OrderAddressPayload) {
  return prisma.billingAddress.create({
    data: payload,
  });
}
export function billingAddressSvcPatch(
  id: string,
  payload: OrderAddressPayload
) {
  return prisma.billingAddress.update({
    where: {
      id: parseInt(id),
    },
    data: payload as any,
  });
}

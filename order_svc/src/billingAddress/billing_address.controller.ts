import { FastifyReply, FastifyRequest } from "fastify";
import { OrderAddressPayload } from "../model/index";
import {
  billingAddressSvcCreate,
  billingAddressSvcGet,
  billingAddressSvcPatch,
} from "./billing_address.svc";

type BillingRequest = FastifyRequest<{
  Params: {
    billing_address_id: string;
  };
  Body: OrderAddressPayload;
}>;
export function billingAddressDetailController(
  request: BillingRequest,
  reply: FastifyReply
) {
  const id = request.params.billing_address_id;
  return billingAddressSvcGet(id).then((data) => {
    return reply.status(200).send({ data: data });
  });
}

export function billingAddressCreateController(
  request: BillingRequest,
  reply: FastifyReply
) {
  const payload = request.body;

  return billingAddressSvcCreate(payload).then((data) => {
    reply.status(201).send({ data });
  });
}
export function billingAddressPatchController(
  request: BillingRequest,
  reply: FastifyReply
) {
  const id = request.params.billing_address_id;
  const payload = request.body;
  return billingAddressSvcPatch(id, payload).then((data) => {
    return reply.status(200).send({ data: data });
  });
}

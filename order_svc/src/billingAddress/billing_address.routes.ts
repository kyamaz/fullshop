import { FastifyInstance, FastifyRegisterOptions } from "fastify";
import {
  billingAddressCreateController,
  billingAddressDetailController,
  billingAddressPatchController,
} from "./billing_address.controller";
import {
  billingAddressCreateSchema,
  billingAddressParamsSchema,
  billingAddressPatchSchema,
} from "./billing_address.schema";

export function billingAddressRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  fastify.get(
    "/billing-address/:billing_address_id",
    {
      schema: billingAddressParamsSchema,
      preHandler: [fastify.authenticate],
    },
    billingAddressDetailController
  );
  fastify.patch(
    "/billing-address/:billing_address_id",
    {
      schema: billingAddressPatchSchema,
      preHandler: [fastify.authenticate],
    },
    billingAddressPatchController
  );
  fastify.post(
    "/billing-address",
    {
      schema: billingAddressCreateSchema,
      preHandler: [fastify.authenticate],
    },
    billingAddressCreateController
  );

  done();
}

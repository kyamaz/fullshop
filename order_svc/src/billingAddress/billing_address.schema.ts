export const billingAddressParamsSchema = {
  params: {
    type: "object",
    properties: {
      billing_address_id: {
        type: "string",
      },
    },
  },
};

export const billingAddressCreateSchema = {
  body: {
    type: "object",
    required: ["address", "postal_code", "city", "country_id"],
    properties: {
      name: {
        type: "string",
      },
      address: {
        type: "string",
      },
      address_more: {
        type: "string",
      },
      postal_code: {
        type: "string",
      },
      city: {
        type: "string",
      },
      country_id: {
        type: "number",
      },
      phone: {
        type: "string",
      },
    },
  },
};
export const billingAddressPatchSchema = {
  body: {
    type: "object",
    required: ["id"],
    properties: {
      id: {
        type: "number",
      },
      address: {
        type: "string",
      },
      address_more: {
        type: "string",
      },
      postal_code: {
        type: "string",
      },
      city: {
        type: "string",
      },
      country_id: {
        type: "number",
      },
      phone: {
        type: "string",
      },
    },
  },
};

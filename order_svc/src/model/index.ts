export interface OrderAddressPayload {
  id?: number;
  address: string;
  address_more: string;
  postal_code: string;
  city: string;
  country?: any;
  country_id: number;
  phone: string;
  orders?: any[];
  name?: string;
}

import { FastifyInstance, FastifyRegisterOptions } from "fastify";
import {
  orderCreateController,
  orderGetController,
  orderGetDetailController,
  orderPatchController,
  orderUpdateAddressController,
  orderUpdateCreateAddressController,
} from "./order.controller";
import {
  orderCreateSchema,
  orderParamsSchema,
  orderPatchSchema,
  orderUpdateAddressSchema,
} from "./order.schema";

export function orderRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  fastify.get(
    "/order/:order_id",
    {
      schema: orderParamsSchema,

      preHandler: [fastify.authenticate],
    },
    orderGetController
  );

  fastify.patch(
    "/order/:order_id",
    {
      schema: orderPatchSchema,
      preHandler: [fastify.authenticate],
    },
    orderPatchController
  );

  fastify.get(
    "/order/:order_id/details",
    {
      schema: orderParamsSchema,

      preHandler: [fastify.authenticate],
    },
    orderGetDetailController
  );

  fastify.put(
    "/order",
    {
      schema: orderCreateSchema,
    },
    orderCreateController
  );

  fastify.post(
    "/order/complete-address",
    {
      schema: orderUpdateAddressSchema,
      preHandler: [fastify.authenticate],
    },
    orderUpdateCreateAddressController
  );
  fastify.patch(
    "/order/complete-address",
    {
      schema: orderUpdateAddressSchema,
      preHandler: [fastify.authenticate],
    },
    orderUpdateAddressController
  );

  done();
}

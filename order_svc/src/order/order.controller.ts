import { throwCustomError } from "@shop/shared_svc/build";
import { FastifyReply, FastifyRequest } from "fastify";
import { orders, shippingAddress } from "prisma";
import { OrderAddressPayload } from "./../model/index";
import {
  orderSvcCompleteAddress,
  orderSvcCreate,
  orderSvcGet,
  orderSvcPatch,
} from "./order.svc";

const SVC_URI = "http://order_svc:5020";

interface ProductItem {
  id: string;
  quantity: number;
  uuid: string;
  price: string;
}
type OrderRequest = FastifyRequest<{
  Params: { order_id: string };
  Body: {
    products: ProductItem[];
    discount_code?: string;
    email: string;
    ref: string;
  };
}>;

type OrderUpdateRequest = FastifyRequest<{
  Params: { order_id: string };
  Body: Record<string, unknown>;
}>;

type OrderCompleteCreateAddressRequest = FastifyRequest<{
  Params: { order_id: string };
  Body: {
    order_uuid: string;
    shipping_address: OrderAddressPayload;
    billing_address: OrderAddressPayload;
  };
}>;

type OrderCompleteUpdateAddressRequest = FastifyRequest<{
  Params: { order_id: string };
  Body: {
    order_uuid: string;
    shipping_address: OrderAddressPayload;
    billing_address: OrderAddressPayload;
  };
}>;
function setDefaultShpping(shipping: null | shippingAddress) {
  return (
    shipping ?? {
      name: null,
      address: null,
      address_more: null,
      postal_code: null,
      city: null,
      phone: null,
      country_id: null,
    }
  );
}
function formatOrder(datum: orders) {
  const { ...tails } = datum;

  return {
    ...tails,
    shipping: setDefaultShpping(datum?.sel_orders_shippingAddress ?? null),
    billing: setDefaultShpping(datum?.sel_orders_billingAddress ?? null),
  };
}

function getDetails(datum: orders) {
  return datum?.sel_orders_orderDetails ?? [];
}

export function orderGetController(request: OrderRequest, reply: FastifyReply) {
  const orderId = request.params.order_id;

  return orderSvcGet(orderId).then((data) => {
    return reply.status(200).send({ data: formatOrder(data) });
  });
}

export function orderPatchController(
  request: OrderUpdateRequest,
  reply: FastifyReply
) {
  const orderId = request.params.order_id;

  const payload = request.body;
  return orderSvcPatch(orderId, payload).then((data) => {
    return reply.status(200).send({ data: formatOrder(data) });
  });
}

export function orderGetDetailController(
  request: OrderRequest,
  reply: FastifyReply
) {
  const orderId = request.params.order_id;

  return orderSvcGet(orderId).then((data) => {
    return reply.status(200).send({ data: getDetails(data) });
  });
}
function randomString(len: number, charSet?: string) {
  charSet =
    charSet || "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  var randomString = "";
  for (var i = 0; i < len; i++) {
    var randomPoz = Math.floor(Math.random() * charSet.length);
    randomString += charSet.substring(randomPoz, randomPoz + 1);
  }
  return randomString;
}
export function orderCreateController(
  request: OrderRequest,
  reply: FastifyReply
) {
  const payload = {
    ...request.body,
    ref: randomString(10),
  };

  return orderSvcCreate(payload).then((data) => {
    reply.status(201).send({ data });
  });
}

async function updateShippingAddress(
  url: string,
  cookie: string,
  payload: OrderAddressPayload
) {
  return fetch(url, {
    method: "PATCH",
    body: JSON.stringify(payload),
    headers: {
      "Content-Type": "application/json",
      cookie,
    },
  })
    .then((res) => {
      if (!res.ok) {
        return Promise.reject("soemthing went wrong:patch");
      }

      return res.json().then((data) => {
        return data.data.id;
      });
    })
    .catch((e) => {
      console.warn(e, "update shipping ERR");

      return throwCustomError(e, "Bad request", 400);
    });
}
export async function orderUpdateAddressController(
  request: OrderCompleteUpdateAddressRequest,
  reply: FastifyReply
) {
  const payload = request.body;

  const shippingAdrId = await updateShippingAddress(
    SVC_URI.concat("/svc/shipping-address"),
    request.headers.cookie as string,

    payload.shipping_address
  );
  const billingAdrId = await updateShippingAddress(
    SVC_URI.concat("/svc/billing-address"),
    request.headers.cookie as string,
    payload.billing_address
  );

  return orderSvcCompleteAddress({
    order_uuid: payload.order_uuid,
    shippingAdrId,
    billingAdrId,
  }).then((data) => {
    reply.status(200).send({ data });
  });
}

async function createOrUpdateAddress(
  url: string,
  cookie: string,
  payload: OrderAddressPayload
) {
  const formatted = {
    address: payload.address,
    address_more: payload.address_more,
    postal_code: payload.postal_code,
    city: payload.city,
    country_id: payload.country_id,
    phone: payload.phone,
    name: payload.name,
  };
  if (typeof payload.id === "number" && payload.id > 0) {
    const id = payload.id.toString();
    return updateShippingAddress(url.concat("/", id), cookie, {
      ...formatted,
      id: payload.id,
    });
  }

  return fetch(url, {
    method: "POST",
    body: JSON.stringify(formatted),
    headers: {
      "Content-Type": "application/json",
      cookie,
    },
  })
    .then((res) => {
      if (!res.ok) {
        return Promise.reject("soemthing went wrong");
      }

      return res.json().then((data) => {
        return data.data.id;
      });
    })
    .catch((e) => {
      console.warn(e, createOrUpdateAddress, "update shipping ERR");

      return null;
    });
}

export async function orderUpdateCreateAddressController(
  request: OrderCompleteCreateAddressRequest,
  reply: FastifyReply
) {
  //TODO  check if order has already address id/ throw if id is set
  const payload = request.body;
  const shippingAdrId = await createOrUpdateAddress(
    SVC_URI.concat("/svc/shipping-address"),

    request.headers.cookie as string,
    payload.shipping_address
  );
  const billingAdrId = await createOrUpdateAddress(
    SVC_URI.concat("/svc/billing-address"),
    request.headers.cookie as string,
    payload.billing_address
  );

  return orderSvcCompleteAddress({
    order_uuid: payload.order_uuid,
    shippingAdrId,
    billingAdrId,
  }).then((data) => {
    reply.status(200).send({ data });
  });
}

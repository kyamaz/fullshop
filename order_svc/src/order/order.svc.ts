import { prisma } from "../app";

export function orderSvcGet(orderId: string) {
  return prisma.orders.findFirst({
    where: {
      uuid: orderId,
    },
    select: {
      uuid: true,
      created_at: true,
      updated_at: true,
      expires_at: true,
      email: true,
      user_id: true,
      ref: true,
      payement_id_fk: true,
      transaction_status: true,
      payment_date: true,
      shipping_fees_id_fk: true,
      sel_orders_orderDetails: {
        where: {
          order_id_fk: orderId,
        },
      },

      sel_orders_shippingAddress: {
        select: {
          id: true,
          name: true,
          address: true,
          address_more: true,
          postal_code: true,
          city: true,
          phone: true,
          country_id: true,
        },
      },
      sel_orders_billingAddress: {
        select: {
          id: true,
          name: true,
          address: true,
          address_more: true,
          postal_code: true,
          city: true,
          phone: true,
          country_id: true,
        },
      },
    },
  });
}

export function orderSvcPatch(orderId: string, payload: any) {
  return prisma.orders.update({
    where: {
      uuid: orderId,
    },
    data: payload,
  });
}

export async function orderSvcCreate({
  email,
  products,
  ref,
}: {
  products: ProductItem[];
  discount_code?: string;
  email: string;
  ref: string;
}) {
  return prisma.orders
    .create({
      data: {
        email,
        ref,
      } as any,
    })
    .then((_orders) => {
      return prisma.orderDetails
        .createMany({
          data: products.map((i) => makeOrderDetails(i, _orders.uuid, sum(i))),
        })
        .then((_) => ({
          orders_id: _orders.uuid,
        }));
    });
}

export async function orderSvcCompleteAddress({
  order_uuid,
  shippingAdrId,
  billingAdrId,
}: {
  order_uuid: string;
  shippingAdrId: any;
  billingAdrId: any;
}) {
  return prisma.orders.update({
    where: {
      uuid: order_uuid,
    },
    data: {
      shipping_address_id_fk: shippingAdrId ?? null,
      billing_address_id_fk: billingAdrId ?? null,
    },
  });
}

interface ProductItem {
  id: string;
  quantity: number;
  uuid: string;
  price: string;
}

function sum(item: ProductItem) {
  return +(parseFloat(item.price) * item.quantity).toFixed(2);
}
function makeOrderDetails(item: ProductItem, order_id: string, total: number) {
  return {
    price_without_taxes: parseFloat(item.price),
    quantity: item.quantity,
    sales_tax: 10,
    total,
    product_id_fk: item.uuid,
    order_id_fk: order_id,
    discount: 0,
  };
}

export const orderParamsSchema = {
  params: {
    type: "object",
    properties: {
      order_id: {
        type: "string",
      },
    },
  },
};

export const orderPatchSchema = {
  params: {
    type: "object",
    properties: {
      order_id: {
        type: "string",
      },
    },
  },
  body: {
    type: "object",
  },
};

export const orderCreateSchema = {
  body: {
    type: "object",
    required: ["email", "products"],

    properties: {
      email: {
        type: "string",
      },
      products: {
        type: "array",
        items: { $ref: "#/$defs/product" },
      },
      discount: {
        type: "string",
      },
    },
    $defs: {
      product: {
        type: "object",
        required: ["id", "uuid", "price", "quantity"],
        properties: {
          id: {
            type: "number",
          },
          uuid: {
            type: "string",
          },

          price: {
            type: "string",
          },
          quantity: {
            type: "number",
          },
        },
      },
    },
  },
};

export const orderUpdateAddressSchema = {
  body: {
    type: "object",
    required: ["order_uuid"],
    properties: {
      order_uuid: {
        type: "string",
      },
      shipping_address: {
        type: "object",
        $ref: "#/$defs/address",
      },
      billing_address: {
        type: "object",
        $ref: "#/$defs/address",
      },
    },
    $defs: {
      address: {
        type: "object",
        //TOOO required id update
        properties: {
          id: {
            type: "number",
          },

          address: {
            type: "string",
          },
          address_more: {
            type: "string",
          },
          postal_code: {
            type: "string",
          },
          city: {
            type: "string",
          },
          country_id: {
            type: "number",
          },
          phone: {
            type: "string",
          },
        },
      },
    },
  },
};

import { FastifyInstance, FastifyRegisterOptions } from "fastify";
import {
  shippingAddressCreateController,
  shippingAddressDetailController,
  shippingAddressPatchController,
} from "./shipping_address.controller";
import {
  shippingAddressCreateSchema,
  shippingAddressParamsSchema,
  shippingAddressPatchSchema,
} from "./shipping_address.schema";

export function shippingAddressRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  fastify.get(
    "/shipping-address/:shipping_address_id",
    {
      schema: shippingAddressParamsSchema,
      preHandler: [fastify.authenticate],
    },
    shippingAddressDetailController
  );
  fastify.patch(
    "/shipping-address/:shipping_address_id",
    {
      schema: shippingAddressPatchSchema,
      preHandler: [fastify.authenticate],
    },
    shippingAddressPatchController
  );

  fastify.post(
    "/shipping-address",
    {
      schema: shippingAddressCreateSchema,
      preHandler: [fastify.authenticate],
    },
    shippingAddressCreateController
  );

  done();
}

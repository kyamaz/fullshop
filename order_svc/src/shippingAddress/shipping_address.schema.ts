export const shippingAddressParamsSchema = {
  params: {
    type: "object",
    properties: {
      shipping_address_id: {
        type: "string",
      },
    },
  },
};

export const shippingAddressCreateSchema = {
  body: {
    type: "object",
    required: ["address", "postal_code", "city", "country_id"],

    properties: {
      address: {
        type: "string",
      },
      address_more: {
        type: "string",
      },
      postal_code: {
        type: "string",
      },
      city: {
        type: "string",
      },
      country_id: {
        type: "number",
      },
      phone: {
        type: "string",
      },
    },
  },
};
export const shippingAddressPatchSchema = {
  body: {
    type: "object",
    required: ["id"],

    properties: {
      id: {
        type: "number",
      },
      address: {
        type: "string",
      },
      address_more: {
        type: "string",
      },
      postal_code: {
        type: "string",
      },
      city: {
        type: "string",
      },
      country_id: {
        type: "number",
      },
      phone: {
        type: "string",
      },
    },
  },
};

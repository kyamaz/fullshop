import { FastifyReply, FastifyRequest } from "fastify";
import { OrderAddressPayload } from "../model/index";
import {
  shippingAddressSvcCreate,
  shippingAddressSvcGet,
  shippingAddressSvcPatch,
} from "./shipping_address.svc";

type ShippingRequest = FastifyRequest<{
  Params: {
    shipping_address_id: string;
  };
  Body: OrderAddressPayload;
}>;
export function shippingAddressDetailController(
  request: ShippingRequest,
  reply: FastifyReply
) {
  const id = request.params.shipping_address_id;
  return shippingAddressSvcGet(id).then((data) => {
    return reply.status(200).send({ data: data });
  });
}

export function shippingAddressCreateController(
  request: ShippingRequest,
  reply: FastifyReply
) {
  const payload = request.body;

  return shippingAddressSvcCreate(payload).then((data) => {
    reply.status(201).send({ data });
  });
}

export function shippingAddressPatchController(
  request: ShippingRequest,
  reply: FastifyReply
) {
  const id = request.params.shipping_address_id;
  const payload = request.body;

  return shippingAddressSvcPatch(id, payload).then((data) => {
    return reply.status(200).send({ data });
  });
}

import { prisma } from "../app";
import { OrderAddressPayload } from "../model/index";

export function shippingAddressSvcGet(id: string) {
  return prisma.shippingAddress.findUniqueOrThrow({
    where: {
      id: parseInt(id),
    },
  });
}

export function shippingAddressSvcCreate(payload: OrderAddressPayload) {
  return prisma.shippingAddress.create({
    data: payload,
  });
}
export function shippingAddressSvcPatch(
  id: string,
  payload: OrderAddressPayload
) {
  return prisma.shippingAddress.update({
    where: {
      id: parseInt(id),
    },
    data: payload as any,
  });
}

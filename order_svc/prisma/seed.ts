import { PrismaClient } from ".prisma/client";

const prisma = new PrismaClient();

const country = ["France", "Germany", "Japan"];
async function main() {
  for (let name of country) {
    await prisma.country.create({ data: { name } });
  }
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });

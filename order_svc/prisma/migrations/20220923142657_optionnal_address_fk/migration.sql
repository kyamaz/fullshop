-- DropForeignKey
ALTER TABLE "orders" DROP CONSTRAINT "orders_billing_address_id_fk_fkey";

-- DropForeignKey
ALTER TABLE "orders" DROP CONSTRAINT "orders_shipping_address_id_fk_fkey";

-- AlterTable
ALTER TABLE "orders" ALTER COLUMN "billing_address_id_fk" DROP NOT NULL,
ALTER COLUMN "shipping_address_id_fk" DROP NOT NULL;

-- AddForeignKey
ALTER TABLE "orders" ADD CONSTRAINT "orders_shipping_address_id_fk_fkey" FOREIGN KEY ("shipping_address_id_fk") REFERENCES "shippingAddress"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orders" ADD CONSTRAINT "orders_billing_address_id_fk_fkey" FOREIGN KEY ("billing_address_id_fk") REFERENCES "billingAddress"("id") ON DELETE SET NULL ON UPDATE CASCADE;

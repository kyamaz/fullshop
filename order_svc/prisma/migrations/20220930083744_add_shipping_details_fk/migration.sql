/*
  Warnings:

  - You are about to drop the column `freight` on the `orderDetails` table. All the data in the column will be lost.
  - You are about to drop the column `ship_date` on the `orderDetails` table. All the data in the column will be lost.
  - You are about to drop the column `shipper_id` on the `orderDetails` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "orderDetails" DROP COLUMN "freight",
DROP COLUMN "ship_date",
DROP COLUMN "shipper_id";

-- AlterTable
ALTER TABLE "orders" ADD COLUMN     "sel_order_shipping_details" JSONB,
ADD COLUMN     "shipping_details_id_fk" INTEGER;

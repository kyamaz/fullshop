/*
  Warnings:

  - Added the required column `billing_address_id_fk` to the `orders` table without a default value. This is not possible if the table is not empty.
  - Added the required column `ref` to the `orders` table without a default value. This is not possible if the table is not empty.
  - Added the required column `shipping_address_id_fk` to the `orders` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "orders" ADD COLUMN     "billing_address_id_fk" INTEGER NOT NULL,
ADD COLUMN     "ref" TEXT NOT NULL,
ADD COLUMN     "shipping_address_id_fk" INTEGER NOT NULL;

-- CreateTable
CREATE TABLE "shippingAddress" (
    "id" SERIAL NOT NULL,
    "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ(6) NOT NULL,
    "address" TEXT NOT NULL,
    "address_more" TEXT NOT NULL,
    "postal_code" TEXT NOT NULL,
    "city" TEXT NOT NULL,
    "country_id" INTEGER NOT NULL,
    "phone" TEXT NOT NULL,

    CONSTRAINT "shippingAddress_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "billingAddress" (
    "id" SERIAL NOT NULL,
    "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ(6) NOT NULL,
    "address" TEXT NOT NULL,
    "address_more" TEXT NOT NULL,
    "postal_code" TEXT NOT NULL,
    "city" TEXT NOT NULL,
    "country_id" INTEGER NOT NULL,
    "phone" TEXT NOT NULL,

    CONSTRAINT "billingAddress_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "country" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "country_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "orders" ADD CONSTRAINT "orders_shipping_address_id_fk_fkey" FOREIGN KEY ("shipping_address_id_fk") REFERENCES "shippingAddress"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "orders" ADD CONSTRAINT "orders_billing_address_id_fk_fkey" FOREIGN KEY ("billing_address_id_fk") REFERENCES "billingAddress"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "shippingAddress" ADD CONSTRAINT "shippingAddress_country_id_fkey" FOREIGN KEY ("country_id") REFERENCES "country"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "billingAddress" ADD CONSTRAINT "billingAddress_country_id_fkey" FOREIGN KEY ("country_id") REFERENCES "country"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- CreateEnum
CREATE TYPE "OrderStatus" AS ENUM ('CREATED', 'PENDING', 'REJECTED', 'ERROR', 'PAID');

-- CreateTable
CREATE TABLE "orders" (
    "uuid" TEXT NOT NULL,
    "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ(6),
    "expires_at" TIMESTAMPTZ(6),
    "email" TEXT NOT NULL,
    "user_id" TEXT,
    "payement_id_fk" INTEGER,
    "transaction_status" "OrderStatus" NOT NULL DEFAULT 'CREATED',
    "payment_date" TIMESTAMPTZ(6),
    "paid" MONEY,

    CONSTRAINT "orders_pkey" PRIMARY KEY ("uuid")
);

-- CreateTable
CREATE TABLE "orderDetails" (
    "id" SERIAL NOT NULL,
    "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ(6) NOT NULL,
    "order_id_fk" TEXT NOT NULL,
    "price_without_taxes" MONEY NOT NULL,
    "quantity" INTEGER NOT NULL,
    "discount" MONEY NOT NULL,
    "sales_tax" MONEY NOT NULL,
    "total" MONEY NOT NULL,
    "ship_date" TIMESTAMPTZ(6),
    "shipper_id" INTEGER,
    "freight" MONEY,
    "bill_date" TIMESTAMPTZ(6),
    "product_id_fk" TEXT NOT NULL,

    CONSTRAINT "orderDetails_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "orders_email_uuid_key" ON "orders"("email", "uuid");

-- AddForeignKey
ALTER TABLE "orderDetails" ADD CONSTRAINT "orderDetails_order_id_fk_fkey" FOREIGN KEY ("order_id_fk") REFERENCES "orders"("uuid") ON DELETE RESTRICT ON UPDATE CASCADE;

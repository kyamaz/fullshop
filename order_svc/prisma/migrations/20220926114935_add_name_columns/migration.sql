-- AlterTable
ALTER TABLE "billingAddress" ADD COLUMN     "name" TEXT,
ALTER COLUMN "address" DROP NOT NULL,
ALTER COLUMN "address_more" DROP NOT NULL,
ALTER COLUMN "postal_code" DROP NOT NULL,
ALTER COLUMN "city" DROP NOT NULL;

-- AlterTable
ALTER TABLE "shippingAddress" ADD COLUMN     "name" TEXT,
ALTER COLUMN "address" DROP NOT NULL,
ALTER COLUMN "address_more" DROP NOT NULL,
ALTER COLUMN "postal_code" DROP NOT NULL,
ALTER COLUMN "city" DROP NOT NULL;

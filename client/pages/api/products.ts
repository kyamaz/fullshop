// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { fetchCatch } from "@shop/shared_svc/build";
import Shop, { ApiErrorResponse } from "@shop/shared_types";
import type { NextApiRequest, NextApiResponse } from "next";

/**
 * todo add a type bff
 *
 * @param {Shop.ProductCollectionRepositoryResponse<Partial<Shop.ProductModel>>} svcResp
 * @returns
 */
function formatData(
  svcResp: Shop.ProductCollectionRepositoryResponse<Partial<Shop.ProductModel>>
) {
  const { data, query } = svcResp;
  return {
    data: data.map((item) => {
      const {
        id,
        created_at,
        updated_at,
        description_long,
        image_alt_url,
        related_product,
        num_available,
        quantity_id_fk,
        upsale_product,
        ...tail
      } = item;
      return tail;
    }),
    query,
  };
}

export default async function products(
  req: NextApiRequest,
  response: NextApiResponse<
    | Shop.ProductCollectionRepositoryResponse<Partial<Shop.ProductModel>>
    | ApiErrorResponse
  >
) {
  const { take, skip } = req.query;
  return fetchCatch<
    Shop.ProductCollectionRepositoryResponse<Partial<Shop.ProductModel>>
  >(
    `http://product_svc:5000/svc/product?take=${take ?? "20"}&skip=${
      skip ?? "0"
    }`
  )
    .then((data) => {
      response.setHeader(
        "Cache-Control",
        "public, s-maxage=30, stale-while-revalidate=59"
      );

      return response.status(200).send(formatData(data));
    })
    .catch((e) =>
      response.status(500).send({ msg: "something went wrong", status: 500 })
    );
}

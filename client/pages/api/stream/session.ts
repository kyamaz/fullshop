import { fetchCatch } from "@shop/shared_svc/build";
import { NextApiRequest, NextApiResponse } from "next";
interface CartSessionSlice {
  name: string;
  expire_date: string;
}
const sessionIntervalSecond = 1000 * 30; //30 seconde
export default async function cartApi(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  // Sends a SSE every 3 seconds on a single connection.
  let intrv = setInterval(function () {
    emitSSE(res);
  }, sessionIntervalSecond);

  function emitSSE(res: any) {
    return fetchCatch<CartSessionSlice | null>(
      "http://cart_svc:5010/svc/cart-session"
    )
      .then((resp) => {
        const cartState = resp;

        console.debug("START SSE", new Date().toISOString());
        if (!cartState || !cartState?.expire_date) {
          clearInterval(intrv);
          throw new Error("session does not exist");
        }

        res.write("data: pending\n\n");
        res.flush();
      })
      .catch((e) => {
        clearInterval(intrv);

        res.write("data: expired\n\n");
        res.flush();
      });
  }

  res.writeHead(200, {
    "Content-Type": "text/event-stream",
    "Cache-Control": "no-cache",
    Connection: "keep-alive",
  });

  emitSSE(res);
}

import { passWebCredentials } from "@shared/api";
import { ErrorCustom, fetchCatch } from "@shop/shared_svc/build";
import { NextApiRequest, NextApiResponse } from "next";
interface OrderPackage {
  weight: {
    value: number;
    unit: string;
  };
}
interface EstimateShippingApiRequest extends NextApiRequest {
  body: {
    ship_to: Record<string, unknown>;
    service_code: string;
    package: OrderPackage[];
  };
}
export default function estimateShipping(
  req: EstimateShippingApiRequest,
  response: NextApiResponse<{ data: any } | ErrorCustom>
) {
  return fetchCatch<{ data: any } | ErrorCustom>(
    `http://shipping_svc:5050/svc/shipping/estimate`,
    {
      method: "POST",
      body: JSON.stringify(req.body),
      ...passWebCredentials(req.headers?.cookie),
    }
  )
    .then((resp) => {
      response.status(201).send(resp);
    })
    .catch((e) => {
      console.error(e, "failed to get product");

      return response.status(e.statusCode).send(e);
    });
}

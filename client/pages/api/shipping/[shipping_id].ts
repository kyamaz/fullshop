import { passWebCredentials } from "@shared/api";
import { ErrorCustom, fetchCatch } from "@shop/shared_svc/build";
import { NextApiRequest, NextApiResponse } from "next";

export default function getShipping(
  req: NextApiRequest,
  response: NextApiResponse<{ data: { value: string }[] } | ErrorCustom>
) {
  const { shipping_id } = req.query;
  return fetchCatch<{ data: { value: string }[] } | ErrorCustom>(
    `http://shipping_svc:5050/svc/shipping/${shipping_id}`,
    {
      method: "GET",
      ...passWebCredentials(req.headers?.cookie),
    }
  )
    .then((resp) => {
      response.status(200).send(resp);
    })
    .catch((e) => {
      console.error(e, "failed to get product");

      return response.status(e.statusCode).send(e);
    });
}

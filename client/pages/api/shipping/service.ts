import { passWebCredentials } from "@shared/api";
import { ErrorCustom, fetchCatch } from "@shop/shared_svc/build";
import { NextApiRequest, NextApiResponse } from "next";
export default function getShippinSvc(
  req: NextApiRequest,
  response: NextApiResponse<{ data: any } | ErrorCustom>
) {
  return fetchCatch<{ data: { value: string }[] } | ErrorCustom>(
    `http://shipping_svc:5050/svc/shipping/delivery-service`,
    {
      method: "GET",
      ...passWebCredentials(req.headers?.cookie),
    }
  )
    .then((resp) => {
      response.setHeader("Cache-Control", "public, max-age=31536000");
      response.status(200).send(resp);
    })
    .catch((e) => {
      console.error(e, "failed to get product");

      return response.status(e.statusCode).send(e);
    });
}

import { ErrorCustom, fetchCatch } from "@shop/shared_svc/build";
import { NextApiRequest, NextApiResponse } from "next";

interface ApiResponse {
  data: Shop.ProductModel & { quantity: number | null };
}
export default function productItem(
  req: NextApiRequest,
  response: NextApiResponse<ApiResponse | ErrorCustom>
) {
  const { uuid } = req.query;
  return fetchCatch<ApiResponse | ErrorCustom>(
    `http://product_svc:5000/svc/product/${uuid}`
  )
    .then((resp) => {
      const {
        data: { sel_product_quantity, ...tail },
      } = resp as ApiResponse;

      //TODO handle idigital product

      response.status(200).send({
        data: {
          ...tail,
          quantity: sel_product_quantity?.num_available ?? null,
        },
      });
    })
    .catch((e) => {
      console.error(e, "failed to get product");

      return response.status(e.statusCode).send(e);
    });
}

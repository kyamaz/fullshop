import { deprecateCookie } from "@shared/api";
import { fetchCatch, getUtcDate } from "@shop/shared_svc";
import { ErrorCustom } from "@shop/shared_svc/build";
import { ApiErrorResponse } from "@shop/shared_types";
import { getCookie, hasCookie, setCookie } from "cookies-next";
import type { NextApiRequest, NextApiResponse } from "next";
/**
 * INTERFACE
 *
 */
export interface CartDetailItem {
  id: string;
  quantity: number;
  uuid: string;
  name: string;
  quantity_id_fk: number;
  price: string;
  image_main_url: string;
}
export interface CartDetailResponse {
  itemCollection: CartDetailItem[];
  expire_date: string | null;
}
interface CartSessionSlice {
  name: string;
  expire_date: string;
}
/**
 * CONSTANT
 *
 */
export const CartCookieName = "cart_session";
const cartSessionURI = "http://cart_svc:5010/svc/cart-session";
const cookieExpiresInMin = 5;

/**
 * HELPER
 *
 */

function encodeCookieValue(str: CartDetailResponse) {
  return Buffer.from(JSON.stringify(str), "utf8").toString("hex");
}
function decodeCookieValue(obj: string): CartDetailResponse {
  const _t = Buffer.from(obj, "hex").toString("utf8");
  return JSON.parse(_t);
}

function mergeCartItem(collection: CartDetailItem[], item: CartDetailItem) {
  const posInCollection = collection.findIndex((el) => el.id === item.id);
  if (posInCollection >= 0) {
    return collection.map((it, idx) => {
      if (idx === posInCollection) {
        return {
          ...it,
          quantity: it.quantity + item.quantity,
        };
      }
      return it;
    });
  }

  return collection.concat([item]);
}
/**
 * persist cart && send 200 or 201
 *
 * @param {({
 *     req: NextApiRequest;
 *     res: NextApiResponse<any | ApiErrorResponse>;
 *   })} {
 *     req,
 *     res,
 *   }
 * @param {CartSessionSlice} sessionPayload
 * @param {CartDetailResponse} cartPayload
 * @param {number} [respStatus=200]
 * @param {*} [cookieOpt={}]
 * @returns
 */
function persistSession(
  {
    req,
    res,
  }: {
    req: NextApiRequest;
    res: NextApiResponse<any | ApiErrorResponse>;
  },
  sessionPayload: CartSessionSlice,
  cartPayload: CartDetailResponse,
  cookieOpt = {},
  respStatus = 200
) {
  return fetchCatch<Response | ErrorCustom>(cartSessionURI, {
    method: "POST",
    body: JSON.stringify(sessionPayload),
  })
    .then((r) => {
      setCookie(CartCookieName, encodeCookieValue(cartPayload), {
        req,
        res,
        ...cookieOpt,
      });

      return res.status(respStatus).send({ data: cartPayload });
    })
    .catch((e: ErrorCustom) => {
      return res.status(e.statusCode).send(e);
    });
}

function createSession(
  {
    req,
    res,
  }: {
    req: NextApiRequest;
    res: NextApiResponse<any | ApiErrorResponse>;
  },
  payload: CartDetailItem
) {
  const expireDate = getUtcDate();
  expireDate.setMinutes(expireDate.getMinutes() + cookieExpiresInMin);
  const maxAge = cookieExpiresInMin * 60;

  const cartData = {
    itemCollection: [payload],
    expire_date: expireDate.toISOString(),
  };
  const sessionSlice = {
    name: CartCookieName,
    expire_date: expireDate.toISOString(),
  } as CartSessionSlice;

  const coookieOpt = {
    httpOnly: false,
    sameSite: "strict",
    expires: expireDate,
    maxAge,
  };
  return persistSession({ req, res }, sessionSlice, cartData, coookieOpt, 201);
}
function cartPersist({
  req,
  res,
}: {
  req: NextApiRequest;
  res: NextApiResponse<any | ApiErrorResponse>;
}) {
  const isCookieSet = hasCookie(CartCookieName, { req, res });
  const payload = JSON.parse(req.body);
  if (isCookieSet) {
    const prev = decodeCookieValue(
      getCookie(CartCookieName, { req, res }) as string
    );

    const cartUpdate = {
      ...prev,
      itemCollection: mergeCartItem(prev.itemCollection, payload),
    };

    const sessionSlice = {
      name: CartCookieName,
      expire_date: cartUpdate.expire_date,
    } as CartSessionSlice;

    //just check if cart has expired
    //if still valid send data form cookie
    //else update its expration date
    return fetchCatch<{ data: CartSessionSlice | ErrorCustom }>(cartSessionURI)
      .then((r) => {
        const expireD = new Date(prev.expire_date as string);
        // resend cookie opt which expire date/duraiotn are lost
        const maxAge = expireD.getTime() - getUtcDate().getTime();
        const coookieOpt = {
          httpOnly: false,
          sameSite: "strict",
          expires: expireD,
          maxAge: maxAge / 1000,
        };

        return persistSession(
          { req, res },
          sessionSlice,
          cartUpdate,
          coookieOpt
        );
      })
      .catch((e: ErrorCustom) => {
        return createSession({ req, res }, payload);
      });
  }
  return createSession({ req, res }, payload);
}

function getCart({
  req,
  res,
}: {
  req: NextApiRequest;
  res: NextApiResponse<any | ApiErrorResponse>;
}) {
  const isCookieSet = hasCookie(CartCookieName, { req, res });
  if (isCookieSet) {
    const data = decodeCookieValue(
      getCookie(CartCookieName, { req, res }) as string
    );
    //just check if cart has expired
    //if still valid send data form cookie
    //else fore coookie deletion by browser

    return fetchCatch<{ data: CartSessionSlice | ErrorCustom }>(cartSessionURI)
      .then((_) => {
        return res.status(200).send({ data });
      })
      .catch((e: ErrorCustom) => {
        deprecateCookie(req, res, CartCookieName);

        return res.status(e.statusCode).send(e);
      });
  }

  return res.status(400).send({ msg: "Cart not found" });
}

export default async function cartApi(
  req: NextApiRequest,
  response: NextApiResponse<any | ApiErrorResponse>
) {
  if (req.method?.toUpperCase() === "POST") {
    return cartPersist({ req, res: response });
  }

  return getCart({ req, res: response });
}

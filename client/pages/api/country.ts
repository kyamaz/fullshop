import { ErrorCustom, fetchCatch } from "@shop/shared_svc/build";
import { NextApiRequest, NextApiResponse } from "next";
export default function getCountry(
  req: NextApiRequest,
  response: NextApiResponse<
    { data: { id: number; name: string }[] } | ErrorCustom
  >
) {
  return fetchCatch<{ data: { id: number; name: string }[] } | ErrorCustom>(
    `http://order_svc:5020/svc/country`
  )
    .then((resp) => {
      response.setHeader("Cache-Control", "public, max-age=31536000");
      response.status(200).send(resp);
    })
    .catch((e) => {
      console.error(e, "failed to get product");

      return response.status(e.statusCode).send(e);
    });
}

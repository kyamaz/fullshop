import { passWebCredentials } from "@shared/api";
import { fetchCatch } from "@shop/shared_svc";
import { ErrorCustom } from "@shop/shared_svc/build";
import { NextApiRequest, NextApiResponse } from "next";
interface OrderCreateResponse {
  data: { orders_id: string };
}

interface ApiRequest extends NextApiRequest {
  query: { order_id: string };
}

function handleGetDetail(
  req: ApiRequest,
  response: NextApiResponse<OrderCreateResponse | ErrorCustom>
) {
  const { order_id } = req.query as any;

  const orderDetailUri = `http://order_svc:5020/svc/order/${order_id}`;

  return fetchCatch(orderDetailUri, {
    method: "GET",
    ...passWebCredentials(req.headers?.cookie),
  })
    .then((resp: any) => {
      return response.status(200).send(resp);
    })
    .catch((e) => {
      console.warn(e);
      return response.status(401).send(e);
    });
}

function handleOrderUpdate(
  req: ApiRequest,
  response: NextApiResponse<OrderCreateResponse | ErrorCustom>
) {
  const { shipping_fee, ...tails } = req.body;
  const orderCreateAddress = `http://order_svc:5020/svc/order/complete-address`;
  const orderCreateShipping = `http://shipping_svc:5050/svc/shipping/create`;

  return fetchCatch(orderCreateAddress, {
    method: "POST",
    body: JSON.stringify(tails),
    ...passWebCredentials(req.headers?.cookie),
  })
    .then((orderAddress: any) => {
      const payload = {
        service_code: shipping_fee.service_code,
        guaranteed_service: shipping_fee.guaranteed_service,
        estimated_delivery_date: shipping_fee.estimated_delivery_date,
        carrier_id_fk: shipping_fee.carrier_id_fk,
        shipping_amount: shipping_fee.shipping_amount,
        insurance_amount: shipping_fee.insurance_amount,
        confirmation_amount: shipping_fee.confirmation_amount,
        other_amount: shipping_fee.other_amount,
        tax_amount: shipping_fee.tax_amount,
        currency: shipping_fee.currency,
        negotiated_rate: shipping_fee.negotiated_rate,
        shipping_address_id_fk: orderAddress.data.shipping_address_id_fk,
        orders_id_fk: orderAddress.data.uuid,
        delivery_days: shipping_fee.delivery_days,
      };

      return fetchCatch(orderCreateShipping, {
        method: "POST",
        ...passWebCredentials(req.headers?.cookie),
        body: JSON.stringify(payload),
      }).then((shippingResp) => ({ orderAddress, shippingResp }));
    })
    .then(({ orderAddress, shippingResp }: any) => {
      const orderPatchlUri = `http://order_svc:5020/svc/order/${orderAddress.data.uuid}`;

      return fetchCatch(orderPatchlUri, {
        method: "PATCH",
        ...passWebCredentials(req.headers?.cookie),
        body: JSON.stringify({
          shipping_fees_id_fk: shippingResp.data.id,
        }),
      }).catch((e) => {
        console.warn(e);

        throw e;
      });
    })
    .then((resp: any) => {
      return response.status(200).send(resp);
    })
    .catch((e) => {
      console.warn(e);

      return response.status(401).send(e);
    });
}

export default function orderGet(
  req: ApiRequest,
  response: NextApiResponse<OrderCreateResponse | { msg: string } | ErrorCustom>
) {
  if (req.method === "GET") {
    return handleGetDetail(req, response);
  }

  if (req.method === "POST") {
    return handleOrderUpdate(req, response);
  }

  return response.status(405).send({ msg: "method not implemented" });
}

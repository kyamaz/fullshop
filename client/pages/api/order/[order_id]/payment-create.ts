import { passWebCredentials } from "@shared/api";
import { fetchCatch } from "@shop/shared_svc";
import { ErrorCustom } from "@shop/shared_svc/build";
import { NextApiRequest, NextApiResponse } from "next";
interface OrderCreateResponse {
  data: any;
}

interface ApiRequest extends NextApiRequest {
  body: {
    order_id: string;
    payment_intent: string;
    payment_intent_client_secret: string;
  };
}

function getStripe(client_secret: string, payment_id: string) {
  return fetchCatch(
    "https://api.stripe.com/v1/payment_intents/".concat(
      payment_id,
      "?",
      new URLSearchParams({ client_secret }).toString()
    ),
    {
      headers: {
        Authorization: "Bearer ".concat(
          process.env?.NEXT_PUBLIC_STRIPE_PUBLIC_KEY ?? ""
        ),
      },
    }
  )
    .then((res) => res)
    .catch((e) => {
      console.warn(e, "failed to get stripe");
      throw new Error("failed to get stripe validation");
    });
}

export default function createPaymentIntent(
  req: ApiRequest,
  response: NextApiResponse<OrderCreateResponse | { msg: string } | ErrorCustom>
) {
  const paymentCreatetUri = `http://payment_svc:5040/svc/payment`;

  const { order_id, payment_intent, payment_intent_client_secret } = req.body;

  const orderPatchlUri = `http://order_svc:5020/svc/order/${order_id}`;

  return getStripe(payment_intent_client_secret, payment_intent)
    .then((stripeRes: any) => {
      if (stripeRes.status !== "succeeded") {
        throw new Error("invalid payment state");
      }
      return fetchCatch(paymentCreatetUri, {
        method: "POST",

        body: JSON.stringify({
          stripe_id: stripeRes.id,
          stripe_client_secret: stripeRes.client_secret,
        }),
        ...passWebCredentials(req.headers?.cookie),
      });
    })
    .then((payment: any) => {
      return fetchCatch(orderPatchlUri, {
        method: "PATCH",

        body: JSON.stringify({
          payement_id_fk: payment.data.id,
          transaction_status: "PAID",
        }),
        ...passWebCredentials(req.headers?.cookie),
      });
    })
    .then((data: any) => {
      return response.status(200).send(data);
    })
    .catch((e) => {
      console.warn(e, "register payement");
      return response.status(500).send(e);
    });
}

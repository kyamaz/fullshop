import { passWebCredentials } from "@shared/api";
import { fetchCatch } from "@shop/shared_svc";
import { ErrorCustom } from "@shop/shared_svc/build";
import { NextApiRequest, NextApiResponse } from "next";
interface OrderCreateResponse {
  data: any;
}

interface ApiRequest extends NextApiRequest {
  query: { order_id: string };
}

export default function createPaymentIntent(
  req: ApiRequest,
  response: NextApiResponse<OrderCreateResponse | { msg: string } | ErrorCustom>
) {
  const { order_id } = req.query as any;

  const paymentIntentUri = `http://payment_svc:5040/svc/payment/intent`;

  return fetchCatch(paymentIntentUri, {
    method: "POST",

    body: JSON.stringify({ order_id }),
    ...passWebCredentials(req.headers?.cookie),
  })
    .then((resp: any) => {
      return response.status(200).send(resp);
    })
    .catch((e) => {
      console.warn(e, "INTENT");
      return response.status(400).send(e);
    });
}

import { ErrorCustom } from "@shop/shared_svc/build";
import { NextApiRequest, NextApiResponse } from "next";
import { createAnonymousUser } from "pages/api/auth/signup";
interface OrderCreateResponse {
  data: { orders_id: string };
}

interface ApiRequest extends NextApiRequest {
  body: string;
}
/**
 * 
    email: string;
    products: Product[];
 */

export default function orderCreate(
  req: ApiRequest,
  response: NextApiResponse<OrderCreateResponse | ErrorCustom>
) {
  const { email } = req.body as any;

  return fetch(`http://order_svc:5020/svc/order`, {
    method: "PUT",
    body: JSON.stringify(req.body),
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((r) => {
      if (!r.ok) {
        return Promise.reject("create failed");
      }

      return r.json();
    })

    .then((createData) =>
      fetch(`http://cart_svc:5010/svc/cart-session`, {
        method: "DELETE",
      }).then((_) => Promise.resolve(createData))
    )
    .then((resp: any) => {
      if (resp.hasOwnProperty("error")) {
        response.status(500).send(resp);
      }
      const {
        data: { orders_id },
      } = resp;

      return createAnonymousUser(
        {
          order_id: orders_id,
          email,
        },
        req,
        response
      );
    });
}

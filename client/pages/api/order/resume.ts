import { passHeaderstoNextjsRes } from "@shared/api";
import { NextApiRequest, NextApiResponse } from "next";
interface OrderResumeResponse {
  token: string;
}

interface ErrorResponse {
  msg: string;
}
interface ApiRequest extends NextApiRequest {
  body: string;
}

export default function orderResume(
  req: ApiRequest,
  response: NextApiResponse<OrderResumeResponse | ErrorResponse>
) {
  return fetch(`http://auth_svc:5030/svc/anonymous/signin`, {
    method: "POST",
    body: JSON.stringify(req.body),
    credentials: "include",
    headers: {
      "Content-Type": "application/json",
    },
  })
    .then((res) => {
      if (!res.ok) {
        throw "error signin ";
      }

      return res.json().then((token) => {
        //  SVC_URI(res, response);
        passHeaderstoNextjsRes(res, response);

        return response.status(200).send({ token });
      });
    })
    .catch((e) => {
      console.warn(e);
      response.status(500).send({ msg: "something went wrong" });
    });
}

import { deprecateCookie, passHeaderstoNextjsRes } from "@shared/api";
import { throwCustomError } from "@shop/shared_svc/build";
import { NextApiRequest, NextApiResponse } from "next";
import { CartCookieName } from "pages/api/cart";
interface UserCreateResponse {
  token: string;
}

interface ApiRequest extends NextApiRequest {
  body: string;
  query: { auth: "anonymous" | "credentials" };
}

export function createAnonymousUser(
  payload: {
    email: string;
    order_id: string;
  },
  req: NextApiRequest,
  response: NextApiResponse
) {
  return fetch(`http://auth_svc:5030/svc/anonymous/signup`, {
    method: "POST",
    body: JSON.stringify(payload),
    headers: {
      "Content-Type": "application/json",
    },
    credentials: "include", // include, *same-origin, omit
  })
    .then((res) => {
      passHeaderstoNextjsRes(res, response);
      //invalidate cart cookie
      deprecateCookie(req, response, CartCookieName);

      return res.json().then((token: any) => {
        return response
          .status(201)
          .send({ data: { order_id: payload.order_id } });
      });
    })
    .catch((e) => {
      console.error(e, "failed to get product");

      throwCustomError("something wen wrong", "internal error", 500);
    });
}
export default function signup(
  req: ApiRequest,
  response: NextApiResponse<UserCreateResponse>
) {
  const { auth } = req.query;
  return createAnonymousUser(JSON.parse(req.body), req, response).catch((e) => {
    console.error(e, "failed to get product");
  });
}

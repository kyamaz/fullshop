/** @type {import('next').NextConfig} */
const path = require("node:path");
/* const path = require("node:path"); experimental: { output sFileTracingRoot: path.join(__dirname, '../../'),} */
const nextConfig = {
  reactStrictMode: true,
  images: {
    domains: ["loremflickr.com"],
  },
  webpack(config, options) {
    config.module.rules.push({
      test: /\.svg$/,
      use: ["@svgr/webpack"],
    });

    return config;
  },
  eslint: {
    dirs: ["pages", "components", "hooks", "ui", "shared"],
  },
  async headers() {
    return [
      {
        source: "/:path*",
        headers: [{ key: "Accept-Encoding", value: "gzip" }],
      },
      {
        // matching all API routes
        source: "/api/:path*",
        headers: [
          { key: "Access-Control-Allow-Credentials", value: "true" },
          {
            key: "Access-Control-Allow-Origin",
            value: process.env.NEXT_PUBLIC_APP_URI,
          },
          {
            key: "Access-Control-Allow-Methods",
            value: "GET,OPTIONS,PATCH,DELETE,POST,PUT",
          },
          {
            key: "Access-Control-Allow-Headers",
            value:
              "X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version, Set-Cookie, Cookie",
          },
        ],
      },
    ];
  },
  // experimental: { outputFileTracingRoot: path.join(__dirname, "..") },
  output: "standalone",
  env: {
    // declare here all your variables
    NEXT_PUBLIC_APP_URI: process.env.NEXT_PUBLIC_APP_URI,
    NEXT_PUBLIC_STRIPE_PUBLIC_KEY: process.env.STRIPE_PUBLIC_KEY,
  },
  experimental: {
    fontLoaders: [{ loader: "@next/font/google" }],
    appDir: true,
  },
  //https://github.com/vercel/next.js/issues/32987
  // https://github.com/vercel/next.js/discussions/38435
};

module.exports = nextConfig;

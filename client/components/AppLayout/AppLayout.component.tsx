import { CartLink } from "@components/CartLink/CartLink.component";
import { WhishListDialog } from "@components/WishListDialog/WishListDialog.component";
import Image from "next/image";
import Link from "next/link";
import { Suspense } from "react";
import styles from "./styles.module.css";

function AppFooter() {
  return (
    <footer className={styles.footer}>
      <Link
        href="#header"
        aria-label="top of page"
        className={styles.home_link}
      >
        <Image
          src="/undraw_logo.svg"
          alt="logo"
          width={40}
          height={40}
          priority
        />
        <span> App name - 2022</span>
      </Link>
    </footer>
  );
}

function AppHeader() {
  return (
    <header className={styles.header} id="header">
      <Link href="/" aria-label="home page" className={styles.home_link}>
        <Image src="/undraw_logo.svg" alt="logo" width={60} height={60} />
        <span> App name</span>
      </Link>

      <div className={styles.search}>
        <form className={styles.search_form}>
          <input
            id="search_product"
            className={styles.search_input}
            placeholder="look for ..."
          />
          <span className={styles.search_icon}>
            <Image
              src="/search.svg"
              alt="look for a product"
              width={16}
              height={16}
            />
          </span>
        </form>
      </div>

      <div className={styles.action_list}>
        <CartLink />
        <WhishListDialog />

        <Link href="/order/resume">resume checkout</Link>

        <Link href="/login">login</Link>
      </div>
    </header>
  );
}
export function AppLayout({ children }: { children: React.ReactNode }) {
  return (
    <div className={styles.container}>
      <Suspense fallback="loading header...">
        <AppHeader />
      </Suspense>
      {/*      <Suspense fallback="loading main...">
        <main>{children}</main>
      </Suspense> */}

      <main>{children}</main>

      <AppFooter />
    </div>
  );
}

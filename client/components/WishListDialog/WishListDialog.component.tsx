"use client";
import * as Dialog from "@radix-ui/react-dialog";
import { Btn } from "@ui/Btn/Btn.component";
import { createRef, RefObject } from "react";
import styles from "./styles.module.css";
import HeartIcon from "/public/heart.svg";

const iconBtnRef: RefObject<HTMLButtonElement> = createRef();
const closeBtnRef: RefObject<HTMLButtonElement> = createRef();

export function WhishListDialog() {
  return (
    <Dialog.Root>
      <Dialog.Trigger asChild>
        <Btn
          ref={iconBtnRef}
          className={styles.btn}
          aria-label="open wish list popin"
        >
          <HeartIcon className={styles.icon} />
        </Btn>
      </Dialog.Trigger>
      <Dialog.Portal>
        <Dialog.Overlay className={styles.overlay} />
        <Dialog.Content className={styles.content}>
          <Dialog.Title>whis list</Dialog.Title>
          <Dialog.Description>some tex</Dialog.Description>

          <div>
            <Btn>
              <span>save</span>
            </Btn>
            <Dialog.Close asChild>
              <Btn ref={closeBtnRef} aria-label="close wish list popin">
                <span>close</span>
              </Btn>
            </Dialog.Close>
          </div>
        </Dialog.Content>
      </Dialog.Portal>
    </Dialog.Root>
  );
}

import Link from "next/link";
import { CartBadge } from "app/checkout/(component)/CartBadge/CartBadge.component";
import styles from "./styles.module.css";
import CartIcon from "/public/cart.svg";
export function CartLink() {
  return (
    <div>
      <Link
        href="/checkout/cart"
        className={styles.link}
        aria-label="link to cart"
      >
        <CartIcon fill="var(--dark-cyan)" height="20" width="20" />
      </Link>
      <CartBadge />
    </div>
  );
}

import styles from "./style.module.css";
import Star from "../../public/star.svg";

interface Props {
  ratings: string;
  uuid: string;
  number_ratings: number;
}
export function RatingStars({ ratings, uuid, number_ratings }: Props) {
  return (
    <span className={styles.ratings}>
      {Array(parseInt(ratings))
        .fill(null)
        .map((_, idx) => (
          <Star className={styles.star} key={`${uuid}__${idx}`} />
        ))}
      <span className={styles.reviews}>({number_ratings})</span>
    </span>
  );
}

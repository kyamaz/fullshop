import styles from "./styles.module.css";
interface InputErrorProps {
  showMsg: boolean;
  errMsg?: string;
}

export function InputError({
  showMsg,
  errMsg = "invalid field",
}: InputErrorProps) {
  return (
    <div className={styles.row}>
      {showMsg ? <span className={styles.invalid_txt}>{errMsg}</span> : null}
    </div>
  );
}

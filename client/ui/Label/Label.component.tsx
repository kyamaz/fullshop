import styles from "./styles.module.css";
type Props = {
  children: React.ReactNode;
  htmlFor: string;
};
export function Label({ children, ...tail }: Props) {
  return (
    <label className={styles.label} {...tail}>
      {children}
    </label>
  );
}

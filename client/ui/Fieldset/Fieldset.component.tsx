import styles from "./styles.module.css";

type Props = {
  children: React.ReactNode;
};
export function Fieldset({ children }: Props) {
  return <fieldset className={styles.fieldset}>{children}</fieldset>;
}

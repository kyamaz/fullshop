import {
  ForwardedRef,
  forwardRef,
  FunctionComponent,
  RefAttributes,
} from "react";
import styles from "./styles.module.css";
type Props = RefAttributes<HTMLButtonElement> &
  React.ButtonHTMLAttributes<HTMLButtonElement> & {
    children?: React.ReactNode;
    ref?: ForwardedRef<HTMLButtonElement>;
    btnState?: "idle" | "loading";
  };

export const Btn: FunctionComponent<Props> = forwardRef<HTMLButtonElement>(
  function BtnRef(props, forwardedRef) {
    const { children, btnState = "idle", ...tails } = props as Props;

    return (
      <div
        style={
          btnState === "loading"
            ? {
                opacity: 0.7,
                pointerEvents: "none",
                position: "relative",
                zIndex: 99,
              }
            : undefined
        }
      >
        <button
          type="button"
          ref={forwardedRef}
          className={styles.btn}
          data-btn-state="idle"
          {...tails}
        >
          {btnState === "loading" ? <span>loading</span> : children}
        </button>
      </div>
    );
  }
);

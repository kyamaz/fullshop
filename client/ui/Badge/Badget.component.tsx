import styles from "./styles.module.css";

interface Props {
  num: string | number;
}
export function Badge({ num }: Props) {
  return <span className={styles.container}>{num}</span>;
}

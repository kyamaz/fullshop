import styles from "./styles.module.css";
export function ProgressBar() {
  return (
    <div className={styles.container}>
      <div className={`${styles.filler} ${styles.pgr}`} data-progress-bar></div>
    </div>
  );
}

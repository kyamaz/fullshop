import { ForwardedRef, forwardRef, RefAttributes, ElementRef } from "react";
import styles from "./styles.module.css";
type Props = RefAttributes<HTMLInputElement> &
  React.InputHTMLAttributes<HTMLInputElement> & {
    ref?: ForwardedRef<HTMLInputElement>;
  };
export const Input: React.FunctionComponent<Props> = forwardRef<HTMLInputElement>(
  function InputEl(props, forwardedRef) {
    return <input className={styles.input} ref={forwardedRef} {...props} />;
  }
);

import * as RadixSelect from "@radix-ui/react-select";
import styles from "./style.module.css";
interface Props<T> {
  onSelected: (v: string) => void;
  placeholder?: string;
  data: Array<T>;
  getValue?: Function;
  getDisplayValue?: Function;
  key_pre: string;
}
export function Select<T>({
  onSelected,
  placeholder = "",
  data = [],
  getValue = () => {},
  getDisplayValue = () => {},
  key_pre,
}: Props<T>) {
  return (
    <RadixSelect.Root onValueChange={onSelected}>
      <RadixSelect.Trigger className={styles.select_trigger}>
        <RadixSelect.Value placeholder={placeholder} />
        <RadixSelect.Icon />
      </RadixSelect.Trigger>
      <RadixSelect.Content className={styles.select_content}>
        <RadixSelect.Viewport>
          {data.map((datum, idx) => (
            <RadixSelect.Item
              className={styles.select_item}
              key={`${idx}__${key_pre}`}
              value={getValue(datum)}
            >
              <RadixSelect.ItemText>
                {getDisplayValue(datum)}
              </RadixSelect.ItemText>
            </RadixSelect.Item>
          ))}
        </RadixSelect.Viewport>
      </RadixSelect.Content>
    </RadixSelect.Root>
  );
}

import { AppLayout } from "@components/AppLayout/AppLayout.component";
import { ProgressBar } from "@ui/ProgressBar/ProgressBar.component";
import RootProvider from "./(Components)/RootProvider/RootProvider.client";
import "./global.css";

export default function RootLayout({
  // Layouts must accept a children prop.
  // This will be populated with nested layouts or pages
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
        <title>Next 13</title>
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </head>
      <body>
        <RootProvider>
          <ProgressBar />
          <AppLayout>{children}</AppLayout>
        </RootProvider>
      </body>
    </html>
  );
}

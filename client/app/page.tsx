import { fetchCatch } from "@shop/shared_svc/build";
import styles from "@styles/page.module.css";
import { HomeAppContent } from "./(Home)/HomeAppContent/HomeAppContent";
import { HomeLayout } from "./(Home)/HomeLayout/HomeLayout.component";
//https://stackoverflow.com/questions/62377316/docker-hostnames-not-resolving-in-next-js-prod-but-working-in-dev-mode-error-g
//https://stackoverflow.com/questions/67326850/react-next-js-docker-build-failing-when-trying-to-reach-out-to-local-api-endpoin
//http://host.docker.internal:5000/svc/product?take=20&skip=0`
async function load() {
  return fetchCatch<
    Shop.ProductCollectionRepositoryResponse<Partial<Shop.ProductModel>>
  >(`http://host.docker.internal:5000/svc/product?take=20&skip=0`, {
    next: { revalidate: 30 },
  })
    .then((response) => {
      return response;
    })
    .catch((e) => {
      console.warn(e);
      return {
        data: [],
        query: { skip: 0, take: 0 },
      };
    });
}
export default async function MyApp() {
  const products = await load();
  return (
    <HomeLayout>
      <div className={styles.container}>
        <div className={styles.main}>
          <HomeAppContent {...products} />
        </div>
      </div>
    </HomeLayout>
  );
}

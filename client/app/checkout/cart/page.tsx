import { APP_URL } from "@shared/const";
import { ErrorCustom, fetchCatch } from "@shop/shared_svc/build";
import { headers } from "next/headers";
import { CartDetailResponse } from "pages/api/cart";
import { CheckoutCart } from "../(component)/CheckoutCart/CheckoutCart.component";

interface ApiResponse {
  data: CartDetailResponse;
}

async function loadCheckout(cookie: string) {
  return fetchCatch<ApiResponse | ErrorCustom>(`${APP_URL}/api/cart`, {
    headers: {
      cookie,
    },
  })
    .then((resp) => {
      return (resp as ApiResponse).data;
    })
    .catch((e) => {
      console.warn(e, cookie, "error fetch cart");
      return { itemCollection: [], expire_date: null };
    });
}

export default async function CheckoutCartPage() {
  const headersList = headers();
  const cookie = headersList.get("cookie") ?? "";
  const data = await loadCheckout(cookie);
  //@ts-expect-error ts bug
  return <CheckoutCart datum={data} />;
}

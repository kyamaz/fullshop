import { CartDetailItem, CartDetailResponse } from "pages/api/cart";
import { atom } from "jotai";
import { atomWithReset } from "jotai/utils";

export const cartSessionAtom = atomWithReset<CartDetailResponse>({
  itemCollection: [],
  expire_date: null,
});

export const createCartSessionAtom = atomWithReset<boolean>(false);

export const startCartSessionAtom = atom(null, (get, set, _) => {
  set(createCartSessionAtom, true);
});

export const clearCartSessionAtom = atom(null, (get, set, _) => {
  set(cartSessionAtom, {
    itemCollection: [],
    expire_date: null,
  });
  set(createCartSessionAtom, false);
});

export const cartSelectionPayloadAtom = atom((get) => {
  const cart: CartDetailResponse = get(cartSessionAtom);

  return cart.itemCollection.map((c: CartDetailItem) => ({
    id: c.id,
    uuid: c.uuid,
    price: c.price,
    quantity: c.quantity,
  }));
}, null);

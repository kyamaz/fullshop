import { APP_URL } from "@shared/const";
import { useCallback, useEffect } from "react";
function createSource(url: string, resetCb: Function) {
  let source = new EventSource(url);

  source.addEventListener(
    "message",
    function (e) {
      const isExpired = e.data.trim() === "expired";
      if (isExpired) {
        resetCb()();
        source.close();
      }
    },
    false
  );

  source.addEventListener(
    "open",
    function (e) {
      console.log("opened");
    },
    false
  );

  source.addEventListener(
    "error",
    function (e) {
      console.log(e, "error");
      resetCb()();
      source.close();
    },
    false
  );

  return source;
}
let source: EventSource | null = null;
/**
 *
 * sessionEvent default null
 * when creating pass any string
 * @export
 * @template T
 * @param {Function} resetCb
 * @param {(string | null)} sessionEvent
 */
export function useCartSessionSSE<T>(resetCb: Function, sessionEvent: boolean) {
  const fnMemo = useCallback(() => {
    return resetCb;
  }, [resetCb]);

  useEffect(() => {
    if (sessionEvent) {
      const url = `${APP_URL}/api/stream/session`;
      //url is your server url
      if (source && source.readyState === 2) {
        source.close();
        source = createSource(url, fnMemo);
      }
      if (!source) {
        source = createSource(url, fnMemo);
      }

      //do not close connextion on unmount.
      // as it will recreate X number of connection
      //TODO back handle sse cancellation
      //just reuse the existing one
    }

    //handle leaking session
    if (!sessionEvent && source) {
      source.close();
    }
  }, [sessionEvent, fnMemo]);
}

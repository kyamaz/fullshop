"use client";
import { APP_URL } from "@shared/const";
import { Btn } from "@ui/Btn/Btn.component";
import { Input } from "@ui/Input/Input.component";
import { InputError } from "@ui/InputError/InputError.component";
import { Label } from "@ui/Label/Label.component";
import { cartSelectionPayloadAtom } from "app/checkout/(component)/cart.atoms";
import { ExpirationCounter } from "app/checkout/(component)/ExpirationCounter/ExpirationCounter.component";
import { useAtom } from "jotai";
import { atomWithValidate } from "jotai-form";
import { useAtomValue } from "jotai/utils";
import { useRouter } from "next/navigation";
import { ChangeEvent, FormEvent, useState } from "react";
import { z } from "zod";
import styles from "./styles.module.css";
const emailSchema = z.string().email();

// creating the atom with an async validation function

export const emailInputAtom = atomWithValidate("", {
  validate: (v) => {
    return emailSchema.parse(v).trim();
  },
});

function AnonymousForm() {
  const router = useRouter();

  const [email, setEmail] = useAtom(emailInputAtom);
  const products = useAtomValue(cartSelectionPayloadAtom);
  const [asyncBtn, setAsyncBtn] = useState<"idle" | "loading">("idle");

  function next(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();

    if (!email.isValid) {
      return;
    }

    setAsyncBtn("loading");
    const payload = {
      products,
      email: email.value,
    };

    fetch(`${APP_URL}/api/order/create`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    })
      .then((res) => res.json())
      .then((resp: any) => {
        const orderId = resp.data.order_id;

        router.push(`/order/${orderId}/shipping`);
      });
  }
  return (
    <form
      className={styles.form}
      method="POST"
      action="/order/test/shipping"
      onSubmit={(e) => next(e)}
    >
      <Label htmlFor="email">email</Label>
      <Input
        id="email"
        value={email.value}
        onChange={(e: ChangeEvent<HTMLInputElement>) =>
          setEmail(e.target.value)
        }
        name="email"
        placeholder="email"
      />
      <InputError
        showMsg={!email.isValid && email.isDirty}
        errMsg="please check ypur email"
      />
      <div className={styles.btn__row}>
        <Btn type="submit" btnState={asyncBtn}>
          <span>order</span>
        </Btn>
      </div>
    </form>
  );
}

function customFormat(min: number, sec: number) {
  return `${min}:${sec}`;
}
interface Props {
  expireTime: string;
}
export function CheckoutCartAside({ expireTime }: Props) {
  const router = useRouter();
  const expireTimeDate = new Date(expireTime);
  function handleExpire() {
    router.refresh();
  }
  return (
    <aside className={styles.aside}>
      <header className={styles.heading}>
        <h2 className={styles.page__title}>Checkout</h2>

        <div className={styles.count}>
          Yout card is reserverd for
          <ExpirationCounter
            expireTime={expireTimeDate}
            format={customFormat}
            onHasExpired={handleExpire}
          />
        </div>
      </header>
      <div className={styles.content}>
        <h3 className={styles.title}>Contact information</h3>
        <div className={styles.contact}>
          <span className={styles.login}>Already have an account?Log in</span>

          <span className={styles.divider}>or</span>
          <AnonymousForm />
        </div>
      </div>
    </aside>
  );
}

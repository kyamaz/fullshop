"use client";
import { roundToTwo } from "@shared/helper";
import { cartSessionAtom } from "app/checkout/(component)/cart.atoms";
import { useUpdateAtom } from "jotai/utils";
import { CartDetailResponse } from "pages/api/cart";

function sumItems(price: string, quantity: number) {
  return roundToTwo(parseInt(price) * quantity);
}
interface Props {
  datum: CartDetailResponse;
}

//TODO hack to use state
export function CheckoutCartAtom({ datum }: Props) {
  const setDatum = useUpdateAtom(cartSessionAtom);

  setDatum(datum);

  return null;
}

import { APP_URL } from "@shared/const";
import { roundToTwo, sumAll } from "@shared/helper";
import { fetchCatch } from "@shop/shared_svc";
import { Badge } from "@ui/Badge/Badget.component";
import { headers } from "next/headers";
import Image from "next/image";
import { CartDetailResponse } from "pages/api/cart";
import { CheckoutCartAside } from "../CheckoutCartAside/CheckoutCartAside.component";
import { CheckoutCartAtom } from "./CheckoutCart.atom";
import styles from "./styles.module.css";
async function loadCart(cookie: string) {
  return fetchCatch<{ data: CartDetailResponse }>(`${APP_URL}/api/cart`, {
    credentials: "include",
    headers: {
      "Content-Type": "application/json",
      cookie,
    },
  })
    .then((resp) => {
      return resp.data;
    })
    .catch((e) => {
      return { itemCollection: [], expire_date: null };
    });
}
function sumItems(price: string, quantity: number) {
  return roundToTwo(parseInt(price) * quantity);
}
interface Props {
  datum: CartDetailResponse;
}

export async function CheckoutCart({ datum }: Props) {
  const headersList = headers();
  const cookie = headersList.get("cookie") ?? "";
  const { itemCollection, expire_date } = await loadCart(cookie);

  const orderSub = roundToTwo(sumAll(itemCollection ?? []));
  if (itemCollection.length === 0 || !expire_date) {
    return (
      <section className={styles.empty}>
        <h2>Panier vide</h2>
      </section>
    );
  }
  return (
    <section className={styles.container}>
      <CheckoutCartAtom datum={{ itemCollection, expire_date }} />

      <div className={styles.content}>
        <ul className={styles.list__wrap}>
          {itemCollection.map((c) => {
            return (
              <li key={c.id} className={styles.content__row}>
                <div className={styles.content__product}>
                  <Image
                    className={styles.img}
                    src={c.image_main_url}
                    alt={c.name}
                    width={60}
                    height={60}
                    priority
                  />
                  <Badge num={c.quantity} />
                  <div className={styles.content__desc}>
                    <h3 className={styles.content__name}>{c.name}</h3>
                  </div>
                </div>

                <div className={styles.content__sub}>
                  {sumItems(c.price, c.quantity)}€
                </div>
              </li>
            );
          })}

          <li className={styles.content__row}>discount code</li>
          <li className={styles.content__row}>
            <div>
              <p className={styles.label}>sub total</p>
              <p className={styles.label}>shipping</p>
            </div>

            <div className={styles.txt__end}>
              <p className={styles.content__sub}>{orderSub}€</p>
              <span className={styles.label}>Calculated at next step</span>
            </div>
          </li>
          <li className={styles.content__row}>
            <p className={styles.label}>Total</p>

            <span className={styles.sum__total}>{orderSub}€</span>
          </li>
        </ul>
      </div>
      <CheckoutCartAside expireTime={expire_date} />
    </section>
  );
}

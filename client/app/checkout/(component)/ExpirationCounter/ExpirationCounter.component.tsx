import { useEffect, useState } from "react";

interface Props {
  expireTime: Date;
  onHasExpired?: Function;
  styles?: Object;
  format?: Function;
}

export function ExpirationCounter({
  expireTime,
  onHasExpired = () => {},
  styles = {},
  format = function (min: number, sec: number) {
    const fM = min.toString().padStart(2, "0");
    const fS = sec.toString().padStart(2, "0");

    return ` ${fM} minutes and ${fS} seconds`;
  },
}: Props) {
  const dateExpire = new Date(expireTime).getTime();
  const [diff, setDiff] = useState(Math.abs(new Date().getTime() - dateExpire));
  const min = Math.floor(diff / 60000);
  const sec = ((diff % 60000) / 1000).toFixed(0);

  useEffect(() => {
    const intervalId = setInterval(() => {
      //extra seconds to rerender past expire time
      if (new Date().getTime() - 2000 < dateExpire) {
        setDiff(Math.abs(new Date().getTime() - dateExpire));
      }

      if (new Date().getTime() >= dateExpire) {
        //TODO api undo reservation
        //On select -> update quantity list ?

        onHasExpired();
        clearInterval(intervalId);

        return () => clearInterval(intervalId);
      }
    }, 1000);

    return () => {
      clearInterval(intervalId);
    };
  }, [dateExpire, onHasExpired]);

  if (new Date().getTime() >= dateExpire) {
    return null;
  }
  return (
    <span suppressHydrationWarning style={styles}>
      {format(min, sec)}
    </span>
  );
}

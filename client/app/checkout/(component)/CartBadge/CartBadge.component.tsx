"use client";
import { Badge } from "@ui/Badge/Badget.component";
import { cartSessionAtom } from "app/checkout/(component)/cart.atoms";
import { useAtomValue } from "jotai";

export function CartBadge() {
  const cartContent = useAtomValue(cartSessionAtom);

  const countItems = cartContent.itemCollection.length;
  return countItems > 0 ? <Badge num={countItems} /> : null;
}

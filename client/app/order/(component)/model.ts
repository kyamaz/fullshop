export interface OrderAddress {
  id?: number;
  name: null | string;
  address: null | string;
  address_more: null | string;
  postal_code: null | string;
  city: null | string;
  phone: null | string;
  country_id: null | number;
}
export interface ProductOrderDetail {
  id: number;
  created_at: string;
  updated_at: string;
  order_id_fk: string;
  price_without_taxes: string;
  quantity: number;
  discount: string;
  sales_tax: string;
  total: string;
  ship_date: null;
  shipper_id: null;
  freight: null;
  bill_date: null;
  product_id_fk: string;
}
export interface ProductOrder {
  id?: number;
  uuid: string;
  created_at: string;
  updated_at: string;
  expires_at: null | string;
  email: string;
  user_id: null | string;
  ref: string;
  transaction_status: string;
  payment_date: null;
  sel_orders_orderDetails: ProductOrderDetail[];
  billing: OrderAddress;
  shipping: OrderAddress;
  weight_gram: number;
  height_cm: number;
  type: string;

  shipping_fees_id_fk: string;
}
export interface ProductBff extends Shop.ProductModel {
  images: string[];
}

export interface ShippingFeeEstimate {
  carrier_id_fk: number;
  carrier_name: string;
  confirmation_amount: number;
  currency: string;
  delivery_days: number;
  estimated_delivery_date: string;
  guaranteed_service: true;
  insurance_amount: number;
  negotiated_rate: boolean;
  other_amount: null | number;
  service_code: string;
  ship_date: string;
  shipping_amount: number;
  tax_amount: number;
  trackable: true;
}

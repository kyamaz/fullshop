import { OrderAside } from "../OrderAside/OrderAside.component";
import styles from "./styles.module.css";
interface Props {
  children: React.ReactNode;
}
export function OrderLayout({ children }: Props) {
  return (
    <section className={styles.section}>
      {children}
      {/* @ts-expect-error Server Component */}
      <OrderAside />
    </section>
  );
}

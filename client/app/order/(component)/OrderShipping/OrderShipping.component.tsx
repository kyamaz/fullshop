"use client";
import { zodResolver } from "@hookform/resolvers/zod";
import { APP_URL } from "@shared/const";
import { fetchCatch } from "@shop/shared_svc/build";
import { Btn } from "@ui/Btn/Btn.component";
import { Fieldset } from "@ui/Fieldset/Fieldset.component";
import { InputFg } from "app/order/(component)/InputFg/InputFg.component";
import {
  OrderAddress,
  ProductOrder,
  ShippingFeeEstimate,
} from "app/order/(component)/model";
import { shippingFeeAtoms } from "app/order/(component)/order.atoms";
import { SelectFg } from "app/order/(component)/SelectFg/SelectFg.component";
import { ShippingField } from "app/order/(component)/ShippingField/Shipping.component";
import { useAtomValue } from "jotai/utils";
import { AppRouterInstance } from "next/dist/shared/lib/app-router-context";
import { useRouter } from "next/navigation";
import { BaseSyntheticEvent, FormEvent, useEffect } from "react";
import { FieldErrors, FieldValues, useForm } from "react-hook-form";
import styles from "./styles.module.css";
import { formValdiationSchema } from "./validator";
interface Props {
  data: ProductOrder;
}

function getContent(c: { name: string; id: number }) {
  return c.name;
}
function getValue(c: { name: string; id: number }) {
  return c.id.toString();
}

function handleInvalid(
  errorState: FieldErrors<FieldValues>,
  _e: BaseSyntheticEvent | undefined
) {
  console.log(errorState);
}
interface ShippingPayload extends ProductOrder {
  shipping_fee: ShippingFeeEstimate;
}
function handleValid(data: ShippingPayload, router: AppRouterInstance) {
  return (formValue: unknown) => {
    const { shipping, billing } = formValue as {
      shipping: OrderAddress;
      billing: OrderAddress;
    };

    const payload = {
      order_uuid: data.uuid,
      shipping_address: {
        ...shipping,
        country_id: parseInt((shipping as any).country_id as string),
        id: data?.shipping?.id ?? null,
      },
      billing_address: {
        ...billing,
        country_id: parseInt((shipping as any).country_id as string),
        id: data?.billing?.id ?? null,
      },
      shipping_fee: data?.shipping_fee ?? null,
    };

    fetchCatch(APP_URL.concat("/api/order/", data.uuid), {
      method: "POST",
      body: JSON.stringify(payload),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => {
        return router.push(`/order/${data.uuid}/payment`);
      })
      .catch((e) => {
        console.warn(e);
      });
  };
}
function fetchCountry() {
  return fetchCatch<{
    data: { name: string; id: number }[];
  }>(`${APP_URL}/api/country`);
}
export default function OrderShipping({ data }: Props) {
  const { billing, shipping, transaction_status } = data;
  const resolver = zodResolver(formValdiationSchema);
  const {
    register,
    handleSubmit,
    watch,
    trigger,
    formState,
    getFieldState,
    control,
    getValues,
    setValue,
  } = useForm({
    resolver,
    mode: "onChange",
  });
  const router = useRouter();
  const shippingFee = useAtomValue(shippingFeeAtoms);

  useEffect(() => {
    //force revalidation on blur for billing
    const subscription = watch((_, { name }) => {
      if (name?.startsWith("billing")) {
        trigger("billing");
      }
    });

    return () => {
      subscription.unsubscribe();
    };
  }, [watch, trigger]);

  function onSubmit(e: FormEvent) {
    e.preventDefault();
    if (!shippingFee) {
      return;
    }

    const part = handleValid({ ...data, shipping_fee: shippingFee }, router);

    handleSubmit(part, handleInvalid)(e);
  }
  if (!billing || !shipping) {
    return <div>loading ...</div>;
  }

  if (transaction_status === "PAID") {
    return <div> order paid. You cannot change the order </div>;
  }

  return (
    <form className={styles.container} onSubmit={onSubmit}>
      <ShippingField
        shipping={shipping}
        register={register}
        getFieldState={getFieldState}
        formState={formState}
        control={control}
        trigger={trigger}
        getValues={getValues}
        setValue={setValue}
        shippingDelivery={shippingFee?.service_code ?? null}
      />

      <h3> Billing </h3>
      <Fieldset>
        <div className={styles.row__full}>
          <InputFg
            label="Name"
            labelAttr="billing_name"
            registerPath="billing.name"
            inputProps={{
              id: "billing_name",
              defaultValue: billing.name,
            }}
            register={register}
            getFieldState={getFieldState}
            formState={formState}
          />
        </div>
        <div className={styles.row__two}>
          <InputFg
            label="Address"
            labelAttr="billing_address"
            registerPath="billing.address"
            inputProps={{
              id: "billing_address",
              defaultValue: billing.address,
            }}
            register={register}
            getFieldState={getFieldState}
            formState={formState}
          />
          <InputFg
            label="Appartement, suite, etc.. (optional)"
            labelAttr="billing_address_more"
            registerPath="billing.address_more"
            inputProps={{
              id: "blling_address_more",
              defaultValue: billing.address_more,
            }}
            register={register}
            getFieldState={getFieldState}
            formState={formState}
          />
        </div>
        <div className={styles.row__three}>
          <InputFg
            label="Postal code"
            labelAttr="billing_postal_code"
            registerPath="billing.postal_code"
            inputProps={{
              id: "billing_postal_code",
              defaultValue: billing.postal_code,
            }}
            register={register}
            getFieldState={getFieldState}
            formState={formState}
          />

          <InputFg
            label="City"
            labelAttr="blling_city"
            registerPath="billing.city"
            inputProps={{
              id: "billing_city",
              defaultValue: billing.city,
            }}
            register={register}
            getFieldState={getFieldState}
            formState={formState}
          />

          <div>
            <SelectFg
              label="Country *"
              control={control}
              getFieldState={getFieldState}
              registerPath="billing.country_id"
              getContent={getContent}
              getValue={getValue}
              formState={formState}
              defaultValue={billing.country_id?.toString() as any}
              asyncDataGetter={fetchCountry}
            />
          </div>
        </div>
      </Fieldset>

      <Btn type="submit">
        <span>order</span>
      </Btn>
    </form>
  );
}

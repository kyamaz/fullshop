import z, { SafeParseError, ZodError } from "zod";

const nameValidator = z
  .string()
  .min(1)
  .refine(
    (val) => {
      return val.length <= 160;
    },
    {
      message: "Name can't be more than 160 characters",
    }
  );
const addressValidator = z.string().min(1);
const cpValidator = z.string().min(1);
const cityValidator = z.string().min(1);
const countryValidator = z.string().min(1);

const shippingValidator = z.object({
  name: nameValidator,
  address: addressValidator,
  address_more: z.string().optional(),
  postal_code: cpValidator,
  city: cityValidator,
  country_id: countryValidator,
  phone: z.string().optional(),
});
const deliveryValidator = z.string().min(1);
const billingValidator = z
  .object({
    name: z.string(),
    address: z.string(),
    address_more: z.string(),
    postal_code: z.string(),
    city: z.string(),
    country_id: z.string().nullable(),
    phone: z.string(),
  })
  .partial()
  .superRefine((val, ctx) => {
    const formHasValue = doValidateBillingForm(val);
    //only way ? to add validation if any field has value
    // tried to dynamically update validation rule. no easy way
    //https://github.com/colinhacks/zod/issues/1394
    if (formHasValue) {
      const nameError = triggerError(nameValidator, val.name, "name");
      if (nameError) {
        ctx.addIssue(nameError);
      }

      const addressError = triggerError(
        addressValidator,
        val.address,
        "address"
      );
      if (addressError) {
        ctx.addIssue(addressError);
      }
      const cpError = triggerError(cpValidator, val.postal_code, "postal_code");
      if (cpError) {
        ctx.addIssue(cpError);
      }

      const cityError = triggerError(cityValidator, val.city, "city");
      if (cityError) {
        ctx.addIssue(cityError);
      }

      const countryError = triggerError(
        countryValidator,
        val.country_id,
        "country_id"
      );
      if (countryError) {
        ctx.addIssue(countryError);
      }
    }
  });

function triggerError(
  validator:
    | z.ZodString
    | z.ZodEffects<any, string, string>
    | z.ZodNullable<z.ZodString>,
  value: unknown,
  path: string
) {
  const nameParse = validator.safeParse(value);

  if (!nameParse.hasOwnProperty("error")) {
    return null;
  }
  const err: ZodError<any> = (nameParse as SafeParseError<any>).error;

  return {
    code: z.ZodIssueCode.custom,
    path: [path],
    message: err.errors[0]?.message,
  };
}
function doValidateBillingForm(form: Record<string, unknown>) {
  const values = Object.values(form);
  for (let v of values) {
    if (Boolean(v)) {
      return true;
    }
  }
  return false;
}

export const formValdiationSchema = z.object({
  shipping: shippingValidator,
  billing: billingValidator,
  delivery: deliveryValidator,
});

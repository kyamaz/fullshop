"use client";

import { APP_URL } from "@shared/const";
import {
  PaymentElement,
  useElements,
  useStripe,
} from "@stripe/react-stripe-js";
import { Btn } from "@ui/Btn/Btn.component";
import { FormEvent } from "react";
import styles from "./styles.module.css";

interface Props {
  data: any;
  order_id: string;
}

export default function OrderPayment({ order_id }: Props) {
  const stripe = useStripe();
  const elements = useElements();

  async function completePayment(event: FormEvent) {
    // We don't want to let default form submission happen here,
    // which would refresh the page.
    event.preventDefault();

    if (!stripe || !elements) {
      // Stripe.js has not yet loaded.
      // Make sure to disable form submission until Stripe.js has loaded.
      return;
    }

    const result = await stripe.confirmPayment({
      //`Elements` instance that was used to create the Payment Element
      elements,
      confirmParams: {
        return_url: `${APP_URL}/order/`.concat(order_id, "/transition"),
      },
    });

    if (result.error) {
      // Show error to your customer (for example, payment details incomplete)
      console.log(result.error.message);
    } else {
      // Your customer will be redirected to your `return_url`. For some payment
      // methods like iDEAL, your customer will be redirected to an intermediate
      // site first to authorize the payment, then redirected to the `return_url`.
    }
  }
  return (
    <form className={styles.container} onSubmit={completePayment}>
      <div className={styles.content}>
        <h3> Credit card </h3>

        <PaymentElement></PaymentElement>

        <Btn type="submit">
          <span>order</span>
        </Btn>
      </div>
    </form>
  );
}

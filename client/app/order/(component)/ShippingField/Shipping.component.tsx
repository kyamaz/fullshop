import styles from "./styles.module.css";
import { Fieldset } from "@ui/Fieldset/Fieldset.component";
import {
  Control,
  FieldValues,
  FormState,
  UseFormGetFieldState,
  UseFormGetValues,
  UseFormRegister,
  UseFormSetValue,
  UseFormTrigger,
  useWatch,
} from "react-hook-form";
import { InputFg } from "app/order/(component)/InputFg/InputFg.component";
import { SelectFg } from "app/order/(component)/SelectFg/SelectFg.component";
import { OrderAddress, ShippingFeeEstimate } from "app/order/(component)/model";
import { useAtomValue, useUpdateAtom } from "jotai/utils";
import {
  productPackageAtoms,
  shippingFeeAtoms,
} from "app/order/(component)/order.atoms";
import { fetchCatch } from "@shop/shared_svc/build";
import { useEffect, useRef } from "react";
import { debounce, isSame } from "@shared/helper";
import { APP_URL } from "@shared/const";
interface Props {
  shipping: OrderAddress;
  register: UseFormRegister<FieldValues>;
  formState: FormState<FieldValues>;
  getFieldState: UseFormGetFieldState<FieldValues>;
  control: Control<FieldValues, any>;
  trigger: UseFormTrigger<Record<string, unknown>>;
  getValues: UseFormGetValues<Record<string, unknown>>;
  setValue: UseFormSetValue<Record<string, unknown>>;
  shippingDelivery: string | null;
}

function getContent(c: { name: string; id: number }) {
  return c.name;
}
function getValue(c: { name: string; id: number }) {
  return c.id.toString();
}
function fetchCountry() {
  return fetchCatch<{
    data: { name: string; id: number }[];
  }>(`${APP_URL}/api/country`);
}
function getShippingSvc(c: { value: string }) {
  return c.value;
}
function fetchShippingSvc() {
  return fetchCatch<{ data: Array<{ value: string }> }>(
    `${APP_URL}/api/shipping/service`
  );
}

function fetchEstimation(
  fg: any,
  order_package: Array<{ weight: { value: number; unit: string } }>,
  setShippingFee: any
) {
  const payload = {
    ship_to: fg.shipping,
    service_code: fg.delivery,
    package: order_package,
  };

  fetchCatch<{ data: ShippingFeeEstimate }>(
    `${APP_URL}/api/shipping/estimate`,
    {
      method: "POST",
      body: JSON.stringify(payload),
      headers: {
        "Content-Type": "application/json",
      },
    }
  )
    .then((d) => {
      setShippingFee(d.data);
    })
    .catch((e) => {
      console.warn(e, "errro");
    });
}

const partFetchEstimation = debounce(fetchEstimation, 800);

export function ShippingField({
  shipping,
  register,
  getFieldState,
  formState,
  control,
  trigger,
  setValue,
  shippingDelivery,
}: Props) {
  const orderPackages = useAtomValue(productPackageAtoms);

  const setShippingFee = useUpdateAtom(shippingFeeAtoms);

  const _form = useWatch({
    control,
    name: ["shipping", "delivery"],
  });

  const { error: shippingError } = getFieldState("shipping");
  const { error: deliveryError } = getFieldState("delivery");
  const { isValidating } = formState;

  const prevFormInstance = useRef(_form);

  useEffect(() => {
    trigger(["shipping", "delivery"]);
  }, [trigger]);

  useEffect(() => {
    const isDirty = _form.filter(Boolean).length === 2;
    //TODO reset order packages if no tunnel
    //TODO if fess already do not fetch it
    /*   console.debug(orderPackages);*/

    /*     console.debug(
      "go for check",
      shippingError,
      deliveryError,
      isDirty,
      isValidating
    ); */

    if (
      shippingError === undefined &&
      deliveryError === undefined &&
      isDirty &&
      !isValidating
    ) {
      //  console.debug("go for check", _form, prevFormInstance.current);

      if (!isSame(_form, prevFormInstance.current)) {
        const [shipping, delivery] = _form;
        partFetchEstimation(
          {
            shipping,
            delivery,
          },
          orderPackages,
          setShippingFee
        );
        prevFormInstance.current = structuredClone(_form);
      }
    }
  }, [
    shippingError,
    deliveryError,
    isValidating,
    _form,
    orderPackages,
    setShippingFee,
  ]);

  return (
    <div>
      <h3> Shipping address</h3>
      <Fieldset>
        <div className={styles.row__full}>
          <InputFg
            label="Name*"
            labelAttr="shipping_name"
            registerPath="shipping.name"
            inputProps={{
              id: "shipping_name",
              defaultValue: shipping.name,
            }}
            register={register}
            getFieldState={getFieldState}
            formState={formState}
          />
        </div>
        <div className={styles.row__two}>
          <InputFg
            label="Address*"
            labelAttr="shipping_address"
            registerPath="shipping.address"
            inputProps={{
              id: "shipping_address",
              defaultValue: shipping.address,
            }}
            register={register}
            getFieldState={getFieldState}
            formState={formState}
          />
          <InputFg
            label="Appartement, suite, etc.. (optional)"
            labelAttr="address_more"
            registerPath="shipping.address_more"
            inputProps={{
              id: "address_more",
              defaultValue: shipping.address_more,
            }}
            register={register}
            getFieldState={getFieldState}
            formState={formState}
          />
        </div>
        <div className={styles.row__three}>
          <InputFg
            label="Postal code*"
            labelAttr="shipping_postal_code"
            registerPath="shipping.postal_code"
            inputProps={{
              id: "shipping_postal_code",
              defaultValue: shipping.postal_code,
            }}
            register={register}
            getFieldState={getFieldState}
            formState={formState}
          />

          <InputFg
            label="City*"
            labelAttr="shipping_city"
            registerPath="shipping.city"
            inputProps={{
              id: "shipping_city",
              defaultValue: shipping.city,
            }}
            register={register}
            getFieldState={getFieldState}
            formState={formState}
          />

          <SelectFg
            label="Country *"
            control={control}
            getFieldState={getFieldState}
            registerPath="shipping.country_id"
            getContent={getContent}
            getValue={getValue}
            formState={formState}
            defaultValue={shipping.country_id?.toString() ?? null}
            asyncDataGetter={fetchCountry}
          />
        </div>
        <div className={styles.row__full}>
          <InputFg
            label="Phone"
            labelAttr="shipping_phone"
            registerPath="shipping.phone"
            inputProps={{
              id: "shipping_phone",
              defaultValue: shipping.phone,
            }}
            register={register}
            getFieldState={getFieldState}
            formState={formState}
          />
        </div>

        <div className={styles.row__two}>
          <SelectFg
            label="Delivery mode *"
            control={control}
            getFieldState={getFieldState}
            registerPath="delivery"
            getContent={getShippingSvc}
            getValue={getShippingSvc}
            formState={formState}
            asyncDataGetter={fetchShippingSvc}
            defaultValue={shippingDelivery ?? "normal"}
          />
          <div className={styles.warn__wrap}>
            <p className={styles.warn}>
              final cost estimation with shipping address and shipping mod
            </p>
          </div>
        </div>
      </Fieldset>
    </div>
  );
}

import { APP_URL } from "@shared/const";
import { roundToTwo, sumItem } from "@shared/helper";
import { fetchCatch } from "@shop/shared_svc/build";
import { Badge } from "@ui/Badge/Badget.component";
import { ProductOrderDetail } from "app/order/(component)/model";
import { headers } from "next/headers";
import Image from "next/image";
import styles from "./styles.module.css";

interface Props {
  product_id: string;
  order_detail: ProductOrderDetail;
}

interface ProductBff extends Shop.ProductModel {
  images: string[];
}
export async function loadProductDetail(
  productId: string | undefined,
  cookie: string
) {
  if (!productId || !cookie) {
    return null;
  }
  return fetchCatch<any>(`${APP_URL}/api/product/${productId}`, {
    credentials: "include",
    headers: {
      "Content-Type": "application/json",
      cookie,
    },
  })
    .then((productDetail) => productDetail?.data ?? {})
    .catch((e) => {
      console.debug(e);
      return null;
    });
}
export async function OrderDetail({ product_id, order_detail }: Props) {
  const headersList = headers();
  const cookie = headersList.get("cookie") ?? "";
  const data = await loadProductDetail(product_id, cookie);
  const selectionSubTotal = roundToTwo(
    sumItem(order_detail.price_without_taxes, order_detail.quantity.toString())
  );

  if (!data) {
    return <div>oups something went wrong</div>;
  }
  return (
    <li className={styles.order__item}>
      <div className={styles.order__row}>
        <div className={styles.product}>
          <div className={styles.image_container}>
            <Image
              src={data.image_alt_url[0]}
              alt={data.description_long ?? data.name}
              width={80}
              height={60}
              priority={true}
              className={styles.img}
            />

            <Badge num={order_detail.quantity ?? 0} />
          </div>

          <h3 className={styles.name}>{data.name}</h3>
        </div>

        <p className={styles.order__price}>{selectionSubTotal}€</p>
      </div>
    </li>
  );
}

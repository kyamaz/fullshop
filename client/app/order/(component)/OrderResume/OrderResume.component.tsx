"use client";

import { APP_URL } from "@shared/const";
import { fetchCatch } from "@shop/shared_svc/build";
import { Btn } from "@ui/Btn/Btn.component";
import { Input } from "@ui/Input/Input.component";
import { InputError } from "@ui/InputError/InputError.component";
import { Label } from "@ui/Label/Label.component";
import {
  emailInputAtom,
  resumeFormAtom,
  textRequiredInputAtom,
} from "app/order/(component)/order.atoms";
import { useAtom } from "jotai";
import { useRouter } from "next/navigation";
import { ChangeEvent, FormEvent, useRef } from "react";
import styles from "./styles.module.css";
export function OrderResume() {
  const router = useRouter();
  const [email, setEmail] = useAtom(emailInputAtom);
  const [orderId, setOrderId] = useAtom(textRequiredInputAtom);
  const [formGroup] = useAtom(resumeFormAtom);

  //https://github.com/jotai-labs/jotai-form/pull/8
  const orderIdRef = useRef(false);

  function next(e: FormEvent<HTMLFormElement>) {
    e.preventDefault();

    if (!formGroup.isValid) {
      return;
    }
    const payload = formGroup.values;

    return fetchCatch(`${APP_URL}/api/order/resume`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(payload),
    })
      .then((_) => {
        router.push(`/order/${payload.order_id}/shipping`);
      })
      .catch((e) => console.warn(e));
  }
  return (
    <div className={styles.container}>
      <h2 className={styles.title}>Resume order</h2>
      <p className={styles.desc}>
        Pickup the order you left off with email and orderid
      </p>
      <form
        className={styles.form}
        method="POST"
        action="/order/test/shipping"
        onSubmit={(e) => next(e)}
      >
        <div>
          <Label htmlFor="email">email</Label>
          <Input
            id="email"
            value={formGroup.values.email}
            onChange={(e: ChangeEvent<HTMLInputElement>) =>
              setEmail(e.target.value)
            }
            name="email"
            placeholder="email"
          />
          <InputError
            showMsg={!email.isValid && email.isDirty}
            errMsg="please check your email"
          />
        </div>

        <div>
          <Label htmlFor="order_id">order id</Label>
          <Input
            id="order_id"
            value={orderId.value}
            onChange={(e: ChangeEvent<HTMLInputElement>) => {
              orderIdRef.current = true;
              setOrderId(e.target.value);
            }}
            name="order_id"
            placeholder="order id"
          />
          <InputError
            showMsg={!orderId.isValid && orderIdRef.current}
            errMsg="required field"
          />
        </div>

        <div className={styles.btn__row}>
          <Btn type="submit">
            <span>order</span>
          </Btn>
        </div>
      </form>
    </div>
  );
}

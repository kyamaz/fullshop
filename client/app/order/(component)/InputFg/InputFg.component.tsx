import { Input } from "@ui/Input/Input.component";
import { InputError } from "@ui/InputError/InputError.component";
import { Label } from "@ui/Label/Label.component";
import {
  FieldValues,
  FormState,
  UseFormGetFieldState,
  UseFormRegister,
} from "react-hook-form";

export function InputFg({
  label,
  labelAttr,
  inputProps,
  registerPath,
  register,
  getFieldState,
  formState,
}: {
  label: string;
  labelAttr: string;
  registerPath: `${string}` | `${string}.${string}` | `${string}.${number}`;
  inputProps: any;
  register: UseFormRegister<FieldValues>;
  formState: FormState<FieldValues>;
  getFieldState: UseFormGetFieldState<FieldValues>;
}) {
  const { error, isDirty, isTouched } = getFieldState(registerPath, formState);
  const { isSubmitted } = formState;
  return (
    <div>
      <Label htmlFor={labelAttr}>{label}</Label>
      <Input {...inputProps} {...register(registerPath)} />
      <InputError
        showMsg={Boolean(error?.message) && (isTouched || isSubmitted)}
        errMsg={error?.message}
      />
    </div>
  );
}

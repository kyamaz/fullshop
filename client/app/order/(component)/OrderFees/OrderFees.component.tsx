import { APP_URL } from "@shared/const";
import { roundToTwo } from "@shared/helper";
import { fetchCatch } from "@shop/shared_svc/build";
import { ProductOrderDetail } from "app/order/(component)/model";
import { headers } from "next/headers";
import { OrderFeesDetail } from "../OrderFeesDetail/OrderFeesDetail.component";

interface Props {
  fees_id: string | null;
  uuid: string | null;
  order_details: ProductOrderDetail[];
}
function loadFeeDetail(fees_id: string | null, cookie: string) {
  if (!fees_id || !cookie) {
    return null;
  }
  return fetchCatch<any>(`${APP_URL}/api/shipping/${fees_id}`, {
    credentials: "include",
    headers: {
      "Content-Type": "application/json",
      cookie,
    },
  })
    .then((fees) => {
      return fees?.data ?? null;
    })
    .catch((e) => {
      console.debug(e, "err shipping");
      return null;
    });
}
function total(subtotal: number, shipping: number, tax: number) {
  const taxed = subtotal * (tax / 100);
  return roundToTwo(subtotal + taxed + shipping);
}

export async function OrderFees({ fees_id, uuid, order_details }: Props) {
  const headersList = headers();
  const cookie = headersList.get("cookie") ?? "";
  const fees = await loadFeeDetail(fees_id, cookie);

  return <OrderFeesDetail fees={fees} order_details={order_details} />;
}

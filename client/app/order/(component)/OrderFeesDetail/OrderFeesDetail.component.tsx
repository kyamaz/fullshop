"use client";
import { roundToTwo, sumAll } from "@shared/helper";
import {
  ProductOrderDetail,
  ShippingFeeEstimate,
} from "app/order/(component)/model";
import { shippingFeeAtoms } from "app/order/(component)/order.atoms";
import { useAtom } from "jotai";
import styles from "./styles.module.css";
interface Props {
  order_details: ProductOrderDetail[];
  fees: ShippingFeeEstimate;
}

function total(subtotal: number, shipping: number, tax: number) {
  const taxed = subtotal * (tax / 100);
  return roundToTwo(subtotal + taxed + shipping);
}

export function OrderFeesDetail({ fees, order_details }: Props) {
  const [shippingFee, setShippingFee] = useAtom(shippingFeeAtoms);
  if (!shippingFee) {
    setShippingFee(fees);
  }
  const subTotalFees = roundToTwo(sumAll(order_details));

  const totalFees = total(
    subTotalFees ?? 0,
    shippingFee?.shipping_amount ?? 0,
    shippingFee?.tax_amount ?? 0
  );
  return (
    <>
      <li className={styles.block}>
        <div className={styles.order__row}>
          <p className={styles.order__label}>Subtotal</p>
          <p className={styles.order__price}>{subTotalFees}€</p>
        </div>
      </li>
      <li className={styles.order__item}>
        <div className={styles.order__row}>
          <p className={styles.order__label}> shipping fee</p>
          <p className={styles.order__price}>{shippingFee?.shipping_amount}€</p>
        </div>
      </li>
      <li>
        <div className={styles.order__row}>
          <p className={styles.order__label}> tax</p>
          <p className={styles.order__price}> {shippingFee?.tax_amount}%</p>
        </div>
      </li>

      <li className={styles.order__delivery}>
        <div className={styles.order__row}>
          <p className={styles.order__label}> delivery estimate:</p>
          <p className={styles.order__price}>
            {shippingFee?.delivery_days} days
          </p>
        </div>
      </li>

      <li className={styles.order__recap}>
        <p>total</p>

        <p>{totalFees} €</p>
      </li>
    </>
  );
}

import { APP_URL } from "@shared/const";
import { fetchCatch } from "@shop/shared_svc/build";
import { OrderDetail } from "app/order/(component)/OrderDetail/OrderDetail.component";
import { OrderFees } from "app/order/(component)/OrderFees/OrderFees.component";
import { headers } from "next/headers";
import { ProductOrderDetail } from "../model";
import styles from "./styles.module.css";
function loadOrder(orderId: string | undefined, cookie: string) {
  if (!orderId || !cookie) {
    return null;
  }
  return fetchCatch<any>(`${APP_URL}/api/order/${orderId}`, {
    credentials: "include",
    headers: {
      "Content-Type": "application/json",
      cookie,
    },
  })
    .then((orders) => {
      return orders?.data ?? null;
    })
    .catch((e) => {
      console.debug(e);
      return null;
    });
}
export async function OrderAside() {
  const h = headers();
  const referer = h.get("referer") ?? "";
  const cookie = h.get("cookie") ?? "";

  const path = referer.split("/") ?? [];
  const order_id = path.at(4);
  const data = await loadOrder(order_id, cookie);
  const details: ProductOrderDetail[] = data?.sel_orders_orderDetails ?? [];

  return (
    <aside className={styles.aside}>
      <h3 className={styles.order__title}>Your order</h3>

      <ul className={styles.order__list}>
        {details.map((d) => {
          return (
            <div key={d.id}>
              {/* @ts-expect-error Server Component */}
              <OrderDetail
                product_id={d.product_id_fk}
                order_detail={d}
                key={d.id}
              />
            </div>
          );
        })}
        {data ? (
          <>
            {/* @ts-expect-error Server Component */}
            <OrderFees
              fees_id={data?.shipping_fees_id_fk ?? null}
              uuid={data?.uuid ?? null}
              order_details={details}
            />
          </>
        ) : null}
      </ul>
    </aside>
  );
}

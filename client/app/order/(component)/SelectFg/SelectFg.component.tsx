import * as SelectPrimitive from "@radix-ui/react-select";
import { InputError } from "@ui/InputError/InputError.component";
import { Label } from "@ui/Label/Label.component";
import {
  ForwardedRef,
  forwardRef,
  FunctionComponent,
  useCallback,
  useEffect,
  useState,
} from "react";
import {
  Control,
  Controller,
  FieldValues,
  FormState,
  UseFormGetFieldState,
} from "react-hook-form";
import styles from "./styles.module.css";

type FcSelectAltProps = SelectPrimitive.SelectProps & {
  onChange: (value: string) => void;
};
const Select: FunctionComponent<FcSelectAltProps> = forwardRef<
  HTMLButtonElement,
  FcSelectAltProps
>(function SelectRef(
  { children, onChange, ...props },
  forwardedRef: ForwardedRef<HTMLButtonElement | null>
) {
  return (
    <SelectPrimitive.Root onValueChange={onChange} {...props}>
      <SelectPrimitive.Trigger
        ref={forwardedRef}
        className={styles.select_trigger}
      >
        <SelectPrimitive.Value placeholder="country" />
        <SelectPrimitive.Icon />
      </SelectPrimitive.Trigger>
      <SelectPrimitive.Portal>
        <SelectPrimitive.Content className={styles.select_content}>
          <SelectPrimitive.Viewport>{children}</SelectPrimitive.Viewport>
        </SelectPrimitive.Content>
      </SelectPrimitive.Portal>
    </SelectPrimitive.Root>
  );
});

export const SelectItem: FunctionComponent<
  SelectPrimitive.SelectItemProps & {
    children?: React.ReactNode;
    ref?: ForwardedRef<HTMLDivElement | null>;
  }
> = forwardRef<
  HTMLDivElement | null,
  SelectPrimitive.SelectItemProps & {
    children?: React.ReactNode;
    ref?: ForwardedRef<HTMLDivElement | null>;
  }
>(function SelectItemRef(
  { children, ...props },
  forwardedRef: ForwardedRef<HTMLDivElement | null>
) {
  return (
    <SelectPrimitive.Item
      {...props}
      ref={forwardedRef}
      className={styles.select_item}
    >
      <SelectPrimitive.ItemText>{children}</SelectPrimitive.ItemText>
    </SelectPrimitive.Item>
  );
});

interface SelectFgProps {
  label: string;
  control: Control<FieldValues, any>;
  getFieldState: UseFormGetFieldState<FieldValues>;
  registerPath: `${string}` | `${string}.${string}` | `${string}.${number}`;
  formState: FormState<FieldValues>;
  getValue: Function;
  getContent: Function;
  defaultValue?: unknown | null;
  asyncDataGetter: () => Promise<{ data: Array<unknown> }>;
}

let pendingStatus = new Map();
export function SelectFg({
  label = "",
  control,
  registerPath,
  getValue = (f: { name: string; id: number }) => f,
  getContent = (f: { name: string; id: number }) => f,
  getFieldState,
  formState,
  defaultValue = null,
  asyncDataGetter = () => Promise.resolve({ data: [] }),
}: SelectFgProps) {
  const { error, isTouched } = getFieldState(registerPath, formState);
  const dataFetchingCb = useCallback(asyncDataGetter, [asyncDataGetter]);
  const [data, setData] = useState<Array<unknown>>([]);
  const { isSubmitted } = formState;

  useEffect(() => {
    if (!pendingStatus.has(registerPath)) {
      pendingStatus.set(registerPath, false);
    }

    if (pendingStatus.get(registerPath) || data.length > 0) {
      return () => {};
    }
    pendingStatus.set(registerPath, true);
    dataFetchingCb()
      .then((resp) => setData(resp.data))
      .catch((e) => {
        console.warn(e);
      })
      .finally(() => pendingStatus.set(registerPath, false));
  }, [dataFetchingCb, data.length, registerPath]);

  return (
    <div>
      <Label htmlFor={registerPath}>{label}</Label>
      <Controller
        name={registerPath}
        control={control}
        defaultValue={defaultValue}
        render={({ field }) => (
          <Select {...field}>
            {data.map((datum: any, idx: number) => (
              <SelectItem
                key={`${registerPath}__${idx}`}
                value={getValue(datum)}
              >
                {getContent(datum)}
              </SelectItem>
            ))}
          </Select>
        )}
      />

      <InputError
        showMsg={Boolean(error?.message) && (isTouched || isSubmitted)}
        errMsg={error?.message}
      />
    </div>
  );
}

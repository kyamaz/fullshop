import { ProductBff, ProductOrder } from "app/order/(component)/model";
import { atom } from "jotai";
import { atomWithValidate, validateAtoms } from "jotai-form";
import { z } from "zod";
import { ShippingFeeEstimate } from "./model";

export const orderAtoms = atom<ProductOrder | null>(null);

const _asyncProductDetailsAtoms = atom<Set<ProductBff>>(new Set<ProductBff>());

export const productDetailsAtoms = atom<Set<ProductBff>, ProductBff>(
  (get) => get(_asyncProductDetailsAtoms),
  (get, set, value) => {
    const current = get(_asyncProductDetailsAtoms);
    current.add(value);
    set(_asyncProductDetailsAtoms, current);
    set(productPackageAtoms, value);
  }
);

const _asyncProductPackageAtoms = atom<
  Array<{ weight: { value: number; unit: string } }>
>([]);

export const productPackageAtoms = atom<
  Array<{ weight: { value: number; unit: string } }>,
  ProductBff
>(
  (get) => get(_asyncProductPackageAtoms),
  (get, set, value) => {
    const current = get(_asyncProductPackageAtoms);
    current.push({
      weight: {
        value: value["weight_gram"],
        unit: "gram",
      },
    });

    set(_asyncProductPackageAtoms, current);
  }
);

export const shippingFeeAtoms = atom<ShippingFeeEstimate | null>(null);

const emailSchema = z.string().email();

// creating the atom with an async validation function

export const emailInputAtom = atomWithValidate("", {
  validate: (v) => {
    return emailSchema.parse(v).trim();
  },
});

const requiredSchema = z.string().min(1);

export const textRequiredInputAtom = atomWithValidate("", {
  validate: (v) => {
    return requiredSchema.parse(v).trim();
  },
});

export const resumeFormAtom = validateAtoms(
  {
    email: emailInputAtom,
    order_id: textRequiredInputAtom,
  },
  (values) => {
    const reqField = requiredSchema.parse(values.order_id);
    const emailField = emailSchema.parse(values.email);

    if (!emailField || !reqField) {
      throw "form is invalid ";
    }
  }
);
// ,

export const _productPackageAtoms = atom<
  Array<{ weight: { value: number; unit: string } }>
>([]);

export const productDetailsPackagesAtoms = atom<
  Array<{ weight: { value: number; unit: string } }>,
  Set<ProductBff>
>(
  (get) => get(_productPackageAtoms),
  (get, set, value) => {
    let order_package = [] as Array<any>;
    for (let o of value) {
      order_package.push({
        weight: {
          value: o.weight_gram,
          unit: "gram",
        },
      });
    }

    set(_productPackageAtoms, order_package);
  }
);

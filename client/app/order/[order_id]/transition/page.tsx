import { APP_URL } from "@shared/const";
import { redirectCheckoutError } from "@shared/helper";
import { ErrorCustom, fetchCatch } from "@shop/shared_svc/build";
import { headers } from "next/headers";
import styles from "./styles.module.css";
import { Redirect } from "./temp";
function getOrderDetail(uri: string, cookie: string) {
  return fetchCatch<ApiResponse | ErrorCustom>(uri, {
    headers: {
      cookie: cookie,
    },
  })
    .then((resp) => {
      return {
        data: (resp as ApiResponse).data,
      };
    })
    .catch((e) => {
      return { data: null };
    });
}

interface ApiResponse {
  data: any;
}

async function processPayment({
  params,
  searchParams,
}: {
  params?: Record<string, string>;
  searchParams?: Record<string, unknown>;
}) {
  //http://localhost/order/a8432389-5320-4032-a4ce-1a080e66b751/transition?payment_intent=pi_3M7ueMF05HBIvC9W1geCXVkQ&payment_intent_client_secret=pi_3M7ueMF05HBIvC9W1geCXVkQ_secret_ROs8CSNuuDZY61dZigC8iGwBJ&redirect_status=succeeded
  if (
    !params?.order_id ||
    !searchParams?.redirect_status ||
    !searchParams?.payment_intent ||
    !searchParams?.payment_intent_client_secret
  ) {
    console.debug(params, searchParams, "missing parops");

    throw new Error("error ");
  }

  const { order_id } = params;

  const {
    redirect_status,
    payment_intent,
    payment_intent_client_secret,
  } = searchParams;
  if (redirect_status !== "succeeded") {
    throw new Error("error ");
  }
  const headersList = headers();
  const cookie = headersList.get("cookie") ?? "";

  const order = await getOrderDetail(
    `${APP_URL}/api/order/${order_id}`,
    cookie
  );
  if (!order.data) {
    return redirectCheckoutError(
      { msg: "order not found" },
      order_id as string
    );
  }
  const completedUrl = APP_URL.concat(
    "/order/",
    order_id as string,
    "/completed"
  );

  if (order.data?.transaction_status !== "CREATED") {
    //redirect(completedUrl)

    return completedUrl;
  }

  return fetchCatch<string | ErrorCustom>(
    `${APP_URL}/api/order/${order_id}/payment-create`,
    {
      headers: {
        cookie,
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        payment_intent,
        payment_intent_client_secret,
        order_id,
      }),
    }
  )
    .then((resp) => {
      //redirect(completedUrl)
      return completedUrl;
    })
    .catch((e) => {
      return redirectCheckoutError(e, order_id as string);
    });
}

export default async function OrderTransition(ctx: {
  params?: Record<string, string>;
  searchParams?: Record<string, unknown>;
}) {
  try {
    const success = await processPayment(ctx);
    if (success) {
      return (
        <section className={styles.section}>
          <Redirect url={success} />;
        </section>
      );
    }
    return (
      <section className={styles.section}>
        <p>payment processing </p>;
      </section>
    );
  } catch (error) {
    //ERROR next redirect
    console.debug(error);
    return (
      <section className={styles.section}>
        <p>payment failed </p>;
      </section>
    );
  }
}

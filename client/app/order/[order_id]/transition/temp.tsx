"use client";

import { redirect } from "next/navigation";

export function Redirect({ url }: { url: string }) {
  return redirect(url);
}

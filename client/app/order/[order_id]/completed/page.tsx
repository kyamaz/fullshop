import { APP_URL } from "@shared/const";
import { redirectCheckoutError } from "@shared/helper";
import { ErrorCustom, fetchCatch } from "@shop/shared_svc/build";
import OrderCompleted from "app/order/(component)/OrderCompleted/OrderCompleted.component";
import { headers } from "next/headers";
import { CartDetailResponse } from "pages/api/cart";

interface ApiResponse {
  data: CartDetailResponse;
}
async function loadOrder(order_id: string = "") {
  const headersList = headers();
  const cookie = headersList.get("cookie") ?? "";

  return fetchCatch<ApiResponse | ErrorCustom>(
    `${APP_URL}/api/order/${order_id}`,
    {
      headers: {
        cookie,
      },
    }
  )
    .then((resp) => {
      return (resp as ApiResponse).data;
    })
    .catch((e) => {
      console.warn(e, "error ");
      return redirectCheckoutError(e, order_id as string);
    });
}
export default async function OrderCompletedPage({
  params,
}: {
  params?: { order_id?: string };
}) {
  const order_id = params?.order_id;

  const data = await loadOrder(order_id);
  return <OrderCompleted />;
}

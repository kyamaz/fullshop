import { APP_URL } from "@shared/const";
import { redirectCheckoutError } from "@shared/helper";
import { fetchCatch } from "@shop/shared_svc/build";
import { ProductOrder } from "app/order/(component)/model";
import OrderShipping from "app/order/(component)/OrderShipping/OrderShipping.component";
import { headers } from "next/headers";

function loadShiping(order_id: string, cookie: string) {
  return fetchCatch<{ data: ProductOrder }>(
    `${APP_URL}/api/order/${order_id}`,
    {
      credentials: "include",
      headers: {
        "Content-Type": "application/json",
        cookie,
      },
    }
  )
    .then((resp) => {
      return resp.data;
    })
    .catch((e) => {
      console.debug(e, "fetch load shipping");

      return redirectCheckoutError(e, order_id);
    });
}
//on prev, create order
//if user has account, ,page of pending order
//if just by mail, call service
// handle order state
//should have expiration date. stock is locked then released
export default async function OrderShippingPage({
  params,
}: {
  params?: { order_id: string };
}) {
  const order_id = params?.order_id;

  const headersList = headers();
  const cookie = headersList.get("cookie") ?? "";

  if (!cookie || !order_id) {
    console.debug(cookie, "<--cookie", order_id, "<-- order id", "redirect");
    return redirectCheckoutError(
      "cookies or order id not set. fetch aborded",
      order_id as string
    );
  }
  const data = await loadShiping(order_id, cookie);
  return <OrderShipping data={data} />;
}

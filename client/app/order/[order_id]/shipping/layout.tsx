import { OrderLayout } from "@app/order/(component)/OrderLayout/OrderLayout.component";

export default function getLayout({ children }: { children: React.ReactNode }) {
  return <OrderLayout>{children}</OrderLayout>;
}

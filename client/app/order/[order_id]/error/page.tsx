import { AppLayout } from "@components/AppLayout/AppLayout.component";
import styles from "./styles.module.css";

function decodeError(obj?: string): Object {
  if (!obj) {
    return {};
  }
  const _t = Buffer.from(obj, "hex").toString("utf8");
  return JSON.parse(_t);
}
function parseError(searchParams?: Record<string, unknown>) {
  const _err = searchParams?.error ?? "";

  try {
    const e = decodeError(_err as string);

    return e;
  } catch (e) {
    console.warn(e, "failed to parse error");

    return {};
  }
}
export default async function OrderError(ctx: {
  params?: Record<string, string>;
  searchParams?: Record<string, unknown>;
}) {
  const e = parseError(ctx?.searchParams);
  return (
    <AppLayout>
      <div className={styles.full__height}>
        <p>something went wrong</p>
      </div>
    </AppLayout>
  );
}

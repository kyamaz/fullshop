import { APP_URL } from "@shared/const";
import { redirectCheckoutError } from "@shared/helper";
import { fetchCatch } from "@shop/shared_svc/build";
import { headers } from "next/headers";
import { StripeWrap } from "./StripeWrap.component";

function loadPayment(order_id = "") {
  const headersList = headers();
  const cookie = headersList.get("cookie") ?? "";
  return fetchCatch(`${APP_URL}/api/order/${order_id}/payment-intent`, {
    headers: {
      "Content-Type": "application/json",
      cookie,
    },
  })
    .then((resp: any) => {
      if (!(resp as any).data?.client_secret) {
        throw new Error("client id not set ");
      }
      return {
        order_id,
        ...resp.data,
      };
    })
    .catch((e) => {
      return redirectCheckoutError(e, order_id as string);
    });
}

export default async function OrderPaymentPage({
  params,
}: {
  params?: { order_id: string };
}) {
  const order_id = params?.order_id;

  const data = await loadPayment(order_id);

  if (!data.client_secret || !data.order_id) {
    return <p>soemthing went wrong</p>;
  }

  return <StripeWrap data={data} />;
}

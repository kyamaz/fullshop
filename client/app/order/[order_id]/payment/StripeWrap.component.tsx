"use client";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import OrderPayment from "app/order/(component)/OrderPayment/OrderPayment.component";

const stripKey = process.env.NEXT_PUBLIC_STRIPE_PUBLIC_KEY ?? "";

const stripePromise = loadStripe(stripKey);

const appearance = {
  // theme: "none",

  variables: {
    colorText: "#8E4EC6",
    colorDanger: "red",
    spacingUnit: "8px",
    borderRadius: "6px",
  },
  rules: {
    ".Label": {
      color: "#8E4EC6",
    },
    ".Input": {
      padding: "12px 4px",
      border: "1px solid #8E4EC6",
    },
  },
};

export function StripeWrap({ data }: { data: any }) {
  const options = {
    // passing the client secret obtained from the server
    clientSecret: data.client_secret,
    appearance,
  };

  return (
    <Elements stripe={stripePromise} options={options}>
      <OrderPayment data={data} order_id={data.order_id} />
    </Elements>
  );
}

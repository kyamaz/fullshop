import styles from "./styles.module.css";

export default function OrderCatchAll() {
  return (
    <div className={styles.full__height}>
      <p>this page does not exit</p>
    </div>
  );
}

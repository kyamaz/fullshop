import { ErrorCustom, fetchCatch } from "@shop/shared_svc/build";
import { notFound } from "next/navigation";
import ProductDetailContent from "./(components)/ProductDetailContent/ProductDetailContent.component";

interface ApiResponse {
  data: Shop.ProductModel;
}

async function getProductDetail(product_id: string) {
  return fetchCatch<ApiResponse | ErrorCustom>(
    `http://product_svc:5000/svc/product/${product_id}`
  )
    .then((resp) => {
      const {
        data: { sel_product_quantity, ...tail },
      } = resp as ApiResponse;
      const images = [tail.image_main_url].concat(tail.image_alt_url ?? []);

      return {
        ...tail,
        num_available: sel_product_quantity?.num_available ?? null,
        images,
      };
    })
    .catch((e) => {
      return notFound();
    });
}

export default async function ProductDetail({
  params,
}: {
  params: { product_id?: string };
}) {
  const productId = params?.product_id;
  if (!productId) {
    return notFound();
  }

  const data = await getProductDetail(productId as string);

  return <ProductDetailContent datum={data} />;
}

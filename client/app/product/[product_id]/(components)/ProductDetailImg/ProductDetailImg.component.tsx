"use client";

import Image from "next/image";
import { useState } from "react";
import styles from "./styles.module.css";

interface Props {
  images: string[];
}

export function ProductDetailImg({ images }: Props) {
  const [selectedImg, setSelectedImg] = useState<number>(0);
  return (
    <>
      <div className={styles.img__wrap}>
        {images.map((img, idx) => (
          <Image
            src={img}
            width={100}
            height={100}
            key={img}
            alt="image desc maybe small"
            onClick={(_) => setSelectedImg(idx)}
            className={`${styles.img} ${
              selectedImg === idx ? styles.selected : ""
            }`}
          />
        ))}
      </div>
      <div className={styles.content}>
        <Image
          src={images[selectedImg]}
          fill
          sizes="40%"
          alt="image desc maybe big"
          priority={true}
        />
      </div>
    </>
  );
}

import { RatingStars } from "@ui/RatingStars/RatingStars.component";
import { CartAction } from "../CartAction/CartAction.component";
import { ProductDetailImg } from "../ProductDetailImg/ProductDetailImg.component";
import styles from "./styles.module.css";

interface ProductBff extends Shop.ProductModel {
  images: string[];
}
interface Props {
  datum: ProductBff;
}

export default function ProductDetailContent({ datum }: Props) {
  const imgs = datum.images ?? [];

  return (
    <section className={styles.grid}>
      <ProductDetailImg images={imgs} />
      <div className={styles.cta}>
        <h1 className={styles.title}>{datum.name}</h1>

        <RatingStars
          ratings={datum.ratings ?? ""}
          uuid={datum.uuid}
          number_ratings={datum.number_ratings ?? 0}
        />

        <p className={styles.desc}>{datum.description_long}</p>

        <CartAction
          num_available={datum.num_available ?? 0}
          price_without_taxes={datum.price_without_taxes}
          datum={datum}
        />
      </div>
    </section>
  );
}

"use client";
import { APP_URL } from "@shared/const";
import { selectionSubtotalAtoms } from "@shared/items.total.atoms";
import { fetchCatch } from "@shop/shared_svc/build";
import { Btn } from "@ui/Btn/Btn.component";
import { Select } from "@ui/Select/Select.component";
import {
  cartSessionAtom,
  startCartSessionAtom,
} from "app/checkout/(component)/cart.atoms";
import { useAtom } from "jotai";
import { useUpdateAtom } from "jotai/utils";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { CartDetailResponse } from "pages/api/cart";
import { useEffect, useState } from "react";
import styles from "./styles.module.css";
interface ProductBff extends Shop.ProductModel {
  images: string[];
}

interface CartActionProps {
  num_available: number;
  price_without_taxes: string;
  datum: ProductBff;
}
const url = `${APP_URL}/api/cart`;

export function CartAction({
  price_without_taxes,
  num_available,
  datum,
}: CartActionProps) {
  const router = useRouter();

  const [selectedQ, setSelectedQ] = useState<string>();
  const [cartItem, setCartItems] = useAtom(cartSessionAtom);
  const startSession = useUpdateAtom(startCartSessionAtom);
  const [selectionSub, setSelectionSub] = useAtom(selectionSubtotalAtoms);
  const [asyncBtn, setAsyncBtn] = useState<"idle" | "loading">("idle");

  useEffect(() => {
    setSelectionSub({ price_without_taxes, selectedQ });
  }, [price_without_taxes, selectedQ, setSelectionSub]);

  function addToCart(product: ProductBff, quantity: number) {
    if (quantity <= 0) {
      return;
    }
    const payload = {
      id: product.id,
      uuid: product.uuid,
      price: product.price_without_taxes,
      name: product.name,
      quantity_id_fk: product.quantity_id_fk,
      image_main_url: product.image_main_url,
    };
    return fetchCatch<{ data: CartDetailResponse }>(url, {
      method: "POST",
      body: JSON.stringify({ ...payload, quantity }),
      credentials: "same-origin",
    })
      .then(({ data }) => {
        setCartItems(data);
        startSession();
      })
      .catch((e) => {
        console.error(e);
      });
  }

  function buyNow(product: ProductBff, quantity: number) {
    setAsyncBtn("loading");
    addToCart(product, quantity)?.then((_) => router.push("/checkout/cart"));
  }
  const hasCart = cartItem.itemCollection.length > 0;
  return (
    <div>
      <ProductList num_available={num_available} onSelectedQ={setSelectedQ} />

      <div className={styles.payment}>
        <em className={styles.price}>{selectionSub}€</em>
      </div>

      <ul className={styles.action_list}>
        <li
          className={styles.action_item}
          style={hasCart ? undefined : { opacity: 0.6, pointerEvents: "none" }}
        >
          <Link href="/checkout/cart" className={styles.link}>
            checkout
          </Link>
        </li>
        <li className={styles.action_item}>
          <Btn onClick={(_) => addToCart(datum, parseInt(selectedQ ?? "0"))}>
            <span>add to cart</span>
          </Btn>
        </li>
        <li className={styles.action_item}>
          <Btn
            onClick={(_) => buyNow(datum, parseInt(selectedQ ?? "0"))}
            btnState={asyncBtn}
            className={`${styles.btn__buy}`}
          >
            <span> buy now</span>
          </Btn>
        </li>
      </ul>
    </div>
  );
}
interface ProductListProps {
  num_available: number | null;
  onSelectedQ: (value: string) => void;
}
function listGetter(v: number) {
  return v.toString();
}
function ProductList({ num_available, onSelectedQ }: ProductListProps) {
  const numAvailable = Array(num_available ?? 0)
    .fill(null)
    .map((_v, idx) => idx + 1);

  return (
    <Select
      onSelected={onSelectedQ}
      placeholder="number of item"
      data={numAvailable}
      key_pre="product_item"
      getValue={listGetter}
      getDisplayValue={listGetter}
    />
  );
}

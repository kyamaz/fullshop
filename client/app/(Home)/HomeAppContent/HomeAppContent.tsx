"use client";

import styles from "./styles.module.css";
import Image from "next/image";
import React, {
  useState,
  startTransition,
  Suspense,
  useEffect,
  useCallback,
} from "react";
import {
  useCancellableFetch,
  objectQueryToString,
} from "@hooks/useCancellableFetch";
import Link from "next/link";
import Shop from "@shop/shared_types";
import { RatingStars } from "@ui/RatingStars/RatingStars.component";
import { APP_URL } from "@shared/const";

/**
 * accumulate pagination result
 *handle by client rather than hooks
 * @param {*} current
 * @param {*} prev
 * @returns
 */
function accumulateData(current: any, prev: any) {
  const prevData = Array.isArray(prev) ? prev : [];
  const currentData = current?.data ?? [];
  const iterator = new Map(
    prevData.concat(currentData).map((p) => {
      return [p.uuid, p];
    })
  ).values();
  return [...iterator];
}

function LoadMore({
  initialState: { query, data },
  onQuery,
}: {
  initialState: Shop.ProductCollectionRepositoryResponse<
    Partial<Shop.ProductModel>
  >;
  onQuery: Function;
}) {
  const [fetchUrl, setFetchUrl] = useState(
    `${APP_URL}/api/products`.concat("?", objectQueryToString(query))
  );

  const [apiResp, e, status] = useCancellableFetch<{
    data: Partial<Shop.ProductModel>[];
    query: Shop.ProductQueryState | null;
  }>(
    "fetchProduct",
    {
      url: fetchUrl,
    },
    null,
    { data, query }
  );

  useEffect(() => {
    if (apiResp) {
      onQuery(apiResp);
    }
  }, [apiResp, onQuery]);

  function loadMore(
    _: React.MouseEvent<HTMLButtonElement>,
    q: Shop.ProductQueryState | null
  ) {
    if (q === null) {
      return;
    }
    const { skip, take } = q;
    setFetchUrl(`${APP_URL}/api/products?take=${take}&skip=${skip + 1}`);
  }

  function handLoadMore(
    e: React.MouseEvent<HTMLButtonElement>,
    query: Shop.ProductQueryState | null
  ) {
    startTransition(() => {
      loadMore(e, query);
    });
  }

  if (status === "error") {
    return <p>oups something went wrong</p>;
  }

  return (
    <button
      type="button"
      className={`${styles.btn} ${styles.btn__load}`}
      onClick={(e) => handLoadMore(e, apiResp?.query ?? null)}
    >
      show more
    </button>
  );
}

export function HomeAppContent({
  data,
  query,
}: Shop.ProductCollectionRepositoryResponse<Partial<Shop.ProductModel>>) {
  const [collection, setCollection] = useState<
    Shop.ProductCollectionRepositoryResponse<Partial<Shop.ProductModel>>["data"]
  >(data);

  const cbHandleQuery = useCallback(
    (
      update: Shop.ProductCollectionRepositoryResponse<
        Partial<Shop.ProductModel>
      >
    ) => {
      setCollection((prevState) => {
        return accumulateData({ data: update?.data ?? [] }, prevState);
      });
    },
    []
  );

  return (
    <div className={styles.container}>
      <ul className={styles.grid}>
        {collection.map((product: any, idx: number) => (
          <li key={product.uuid} className={styles.card}>
            <article className={styles.card}>
              <div className={styles.image_container}>
                <Image
                  src={product.image_main_url}
                  alt={product.description_long}
                  style={{ objectFit: "fill" }}
                  fill
                  sizes="25vw"
                  priority={idx < 8}
                />
              </div>
              <div className={styles.content}>
                <div className={styles.content__head}>
                  <h2 className={styles.title}>{product.name}</h2>
                  <footer className={styles.footer}>
                    <RatingStars
                      ratings={product.ratings ?? ""}
                      uuid={product.uuid}
                      number_ratings={product.number_ratings ?? 0}
                    />

                    <span className={styles.price}>
                      {product.price_without_taxes}€
                    </span>
                  </footer>
                </div>
                <div className={styles.content__tail}>
                  <Link
                    href={`/product/${product.uuid}`}
                    className={styles.link}
                  >
                    see details
                  </Link>
                  <button
                    type="button"
                    className={`${styles.btn} ${styles.btn__card}`}
                  >
                    add to cart
                  </button>
                </div>
              </div>
            </article>
          </li>
        ))}
      </ul>

      <div className={styles.list_actions}>
        <Suspense fallback="loading ...">
          <LoadMore initialState={{ query, data }} onQuery={cbHandleQuery} />
        </Suspense>
      </div>
    </div>
  );
}

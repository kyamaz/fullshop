"use client";

import { useAtomValue, useUpdateAtom } from "jotai/utils";
import { useCartSessionSSE } from "app/checkout/(component)/useCartSessionSSE";
import {
  clearCartSessionAtom,
  createCartSessionAtom,
} from "app/checkout/(component)/cart.atoms";
/**
 * wrapper to use cart session
 *
 * @param {*} { pathname }
 * @returns
 */

export function SessionSseSub() {
  const clearCartSession = useUpdateAtom(clearCartSessionAtom);
  const startCartSession = useAtomValue(createCartSessionAtom);
  //will rerender on pathname && start session change
  useCartSessionSSE(clearCartSession, startCartSession);

  return null;
}

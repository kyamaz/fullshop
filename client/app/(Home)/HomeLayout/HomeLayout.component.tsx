import styles from "./styles.module.css";
export function HomeLayout({ children }: { children: React.ReactNode }) {
  return (
    <div>
      <div className={styles.header}>
        <h1 className={styles.title}>All product </h1>
        <p className={styles.desc}>
          Javy makes specialty coffee concentrates that are packed with
          delicious flavor.
        </p>
      </div>

      <div className={styles.filter}>
        <ul className={styles.filter__list}>
          <li className={styles.filter__item}>All</li>
        </ul>
      </div>
      <div>{children}</div>
    </div>
  );
}

"use client";

import { Provider } from "jotai";
import { ProgressBar } from "@ui/ProgressBar/ProgressBar.component";
import { SessionSseSub } from "app/(Home)/SessionSseSub/SessionSseSub.client";
import { Component, ErrorInfo, ReactNode } from "react";
//TODO You're importing a component that needs Component. It only works in a Client Component but none of its parents are marked with "use client", so they're Server Components by default.

interface Props {
  children?: ReactNode;
}

interface State {
  hasError: boolean;
}
class ErrorBoundary extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    // Define a state variable to track whether is an error or not
    this.state = { hasError: false };
  }
  static getDerivedStateFromError(er: Error) {
    // Update state so the next render will show the fallback UI
    console.log(er);

    return { hasError: true };
  }
  componentDidCatch(error: Error, errorInfo: ErrorInfo) {
    // You can use your own error logging service here
    console.log({ error, errorInfo });
  }
  render() {
    // Check if the error is thrown
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return (
        <div>
          <h2>Oops, there is an error!</h2>
          <button
            type="button"
            onClick={() => this.setState({ hasError: false })}
          >
            Try again?
          </button>
        </div>
      );
    }

    // Return children components in case of no error

    return this.props.children;
  }
}
export default function RootProvider({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <Provider>
      <SessionSseSub />
      <ErrorBoundary>{children}</ErrorBoundary>
    </Provider>
  );
}

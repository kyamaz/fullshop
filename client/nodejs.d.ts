declare module NodeJS {
  export interface ProcessEnv {
    NEXT_PUBLIC_APP_URI: string;
    NEXT_PUBLIC_STRIPE_PUBLIC_KEY: string;
  }
}

import { CartDetailItem } from "@pages/api/cart";
import { notFound, redirect } from "next/navigation";
import { ProductOrderDetail } from "./../app/order/(component)/model";

export function roundToTwo(num: number) {
  return +(Math.round((num + "e+2") as any) + "e-2");
}

function encodeObj(str: Object) {
  return Buffer.from(JSON.stringify(str), "utf8").toString("hex");
}

export function redirectCheckoutError(e: Object, order_id: string) {
  const encode = encodeObj(e);

  try {
    const redirectUrl = "/order/".concat(
      order_id as string,
      "/error?error=",
      encode
    );
    return redirect(redirectUrl);
  } catch (error) {
    return notFound();
  }
}

export function isObjectLike(value: unknown) {
  return typeof value === "object" && value !== null;
}

export function isSame(a: unknown, b: unknown) {
  if (Array.isArray(a) && Array.isArray(b)) {
    const lenA = a.length;
    const lenB = b.length;
    if (lenA !== lenB) {
      return false;
    }

    for (let [index, prev] of a.entries()) {
      if (!isSame(prev, b[index])) {
        return false;
      }
    }

    return true;
  }

  if (isObjectLike(a) && isObjectLike(b)) {
    const lenA = Object.keys(a as Object).length;
    const lenB = Object.keys(b as Object).length;
    if (lenA !== lenB) {
      return false;
    }

    for (let aKey of Object.keys(a as Object)) {
      if (
        !isSame(
          (a as Record<string, unknown>)[aKey],
          (b as Record<string, unknown>)[aKey]
        )
      ) {
        return false;
      }
    }

    return true;
  }

  return Object.is(a, b);
}

export function debounce(func: Function, wait: number) {
  let timeout: number | undefined | NodeJS.Timeout;

  return function executedFunction(...args: unknown[]) {
    const later = () => {
      timeout = undefined;
      func(...args);
    };
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
}
export function sumItem(
  price: string,
  quantity: string | null | undefined | number
) {
  if (!quantity) {
    return 0;
  }
  if (typeof quantity !== "string") {
    return quantity as number;
  }

  const q = parseInt(quantity);
  if (isNaN(q)) {
    return 0;
  }

  return parseFloat(price) * q;
}

export function sumAll(cart: Array<CartDetailItem | ProductOrderDetail>) {
  return cart.reduce(
    (acc: number, curr: CartDetailItem | ProductOrderDetail) => {
      if (curr.hasOwnProperty("price")) {
        return (
          acc +
          sumItem((curr as CartDetailItem).price, curr.quantity.toString())
        );
      }

      if (curr.hasOwnProperty("price_without_taxes")) {
        return (
          acc +
          sumItem(
            (curr as ProductOrderDetail).price_without_taxes,
            curr.quantity.toString()
          )
        );
      }

      return acc;
    },

    0
  );
}

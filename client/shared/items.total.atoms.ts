import { sumAll } from "@shared/helper";
import { atom } from "jotai";
import { CartDetailItem } from "pages/api/cart";
import { ProductOrderDetail } from "../app/order/(component)/model";

export function roundToTwo(num: number) {
  return +(Math.round((num + "e+2") as any) + "e-2");
}
export function sumItem(
  price: string,
  quantity: string | null | undefined | number
) {
  if (!quantity) {
    return 0;
  }
  if (typeof quantity !== "string") {
    return quantity as number;
  }

  const q = parseInt(quantity);
  if (isNaN(q)) {
    return 0;
  }

  return parseFloat(price) * q;
}

const _orderSubtotalAtoms = atom(0);

export const orderSubtotalAtoms = atom(
  (get) => get(_orderSubtotalAtoms),
  (get, set, selection: Array<CartDetailItem | ProductOrderDetail>) => {
    const _sum = sumAll(selection);
    set(_orderSubtotalAtoms, roundToTwo(_sum));
  }
);

const _selectionSubtotalAtoms = atom(0);

export const selectionSubtotalAtoms = atom(
  (get) => get(_selectionSubtotalAtoms),
  (
    get,
    set,
    {
      price_without_taxes,
      selectedQ,
    }: { price_without_taxes: string; selectedQ: string | undefined }
  ) => {
    const _sum = sumItem(price_without_taxes, selectedQ);
    set(_selectionSubtotalAtoms, roundToTwo(_sum));
  }
);

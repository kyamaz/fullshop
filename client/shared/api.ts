import { hasCookie, setCookie } from "cookies-next";
import { NextApiRequest, NextApiResponse } from "next";
/**
 * forcesbrowser to delete cookie
 *
 * @export
 * @param {NextApiRequest} req
 * @param {NextApiResponse} res
 * @param {string} cookieName
 */
export function deprecateCookie(
  req: NextApiRequest,
  res: NextApiResponse,
  cookieName: string
) {
  if (hasCookie(cookieName, { req, res })) {
    const invalidate = new Date();
    invalidate.setFullYear(2000);
    setCookie(cookieName, "", {
      req,
      res,
      expires: invalidate,
    });
  }
}

/**
 *  set svc header to netxjs response
 * mainly inherit cookie header
 *
 * @export
 * @param {Response} nodeRes
 * @param {NextApiResponse} nextRes
 * @returns {*}
 */
export function passHeaderstoNextjsRes(
  nodeRes: Response,
  nextRes: NextApiResponse
): any {
  for (let [key, value] of nodeRes.headers) {
    nextRes.setHeader(key, value as any);
  }
}
/**
 * pass netxjs req cookie header to next fetch
 *
 * @export
 * @param {(undefined | "string=")} cookie
 * @returns {*}
 */
export function passWebCredentials(cookie: undefined | string = ""): any {
  return {
    credentials: "include",

    headers: {
      "Content-Type": "application/json",
      cookie,
      "Access-Control-Allow-Credentials": "true",
    },
  };
}

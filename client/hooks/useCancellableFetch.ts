import { useEffect, useRef, useState } from "react";
import {
  CachedFetchState,
  defaultFetchState,
  fetchStore,
} from "./persistState";
/**
 *
 *
 * @export
 * @param {Record<string, unknown>} params
 * @returns
 */
export function objectQueryToString(
  params:
    | Shop.ProductCollectionRepositoryResponse<
        Partial<Shop.ProductModel>
      >["query"]
    | {} = {}
) {
  return Object.keys(params).reduce((acc, curr, idx) => {
    return acc.concat(idx > 0 ? "&" : "", curr, "=", (params as any)[curr]);
  }, "");
}
interface RequestInitWithUrl extends RequestInit {
  url: string | null;
}

/**
 * poc custom react query  like hook
 *
 * @export
 * @template T
 * @param {string} name
 * @param {RequestInitWithUrl} [reqOption={ url: null }]
 * @param {(T | null)} [defaultState=null] //intial state
 * @param {(null | T)} [initialData=null] // provide intial value aka ssr. client fetch only after first pass
 * @returns
 */
export function useCancellableFetch<T>(
  name: string,
  reqOption: RequestInitWithUrl = { url: null },
  defaultState: T | null = null,
  initialData: null | T = null,
  cacheData = true
) {
  const { url, ...tail } = reqOption;
  const _ugly = JSON.stringify(tail);
  const [apiResponse, setApiResponse] = useState<
    [data: T | null, error: null | any, status: string]
  >([defaultState, null, "idle"]);

  const useInitValue = useRef<boolean>(false);
  const initialDataRef = useRef(initialData);

  // use initial data on first call (name value is not set) if set
  //TODO Type 'false | T | null' is not assignable to type 'boolean'
  (useInitValue.current as any) = !fetchStore.has(name) && initialData;
  const defaultStateRef = useRef(defaultState);

  useEffect(() => {
    let fetchByNameInstance = {
      current: fetchStore.get(name),
    };
    // skip first get. aka data fetch server side which set initial data
    if (useInitValue.current) {
      new Promise((resolve) => {
        fetchByNameInstance.current = fetchStore
          .set(name, {
            ...((fetchByNameInstance.current ??
              defaultFetchState) as CachedFetchState<any>),
            url,
            prevResponse: {
              ...initialDataRef.current,
            },
          })
          .values()
          .next().value;

        resolve(setApiResponse([initialDataRef.current, null, "success"]));
      });
      return () => {};
    }

    //handle refresh. basically as long as url are same , return cache.
    else if (fetchStore.get(name)?.url === url) {
      new Promise((resolve) => {
        resolve(
          setApiResponse([
            (fetchByNameInstance.current as CachedFetchState<any>).prevResponse,
            null,
            "success",
          ])
        );
      });
      return () => {};
    }
    // fetch data

    if (!url) {
      //console.warn("url is not set. did you forget to set it ?");

      setApiResponse([null, null, "idle"]);

      return () => {};
    }

    const abortControllerInstance = new AbortController();

    fetchStore.set(name, {
      ...defaultFetchState,
      ...(fetchByNameInstance.current ?? {}),
      promises: fetch(url, {
        ...JSON.parse(_ugly),
        signal: abortControllerInstance.signal,
      }),
    });

    fetchByNameInstance.current = fetchStore.get(name)!;
    setApiResponse([null, null, "querying"]);

    const promise = (fetchByNameInstance.current as CachedFetchState<any>)
      .promises;
    if (!promise) {
      console.warn("promise not set ?");

      setApiResponse([defaultStateRef.current, null, "idle"]);

      return () => {};
    }

    promise
      .then((res: any) => {
        /// workaround response already read
        return res.clone().json();
      })
      .catch((error) => {
        setApiResponse([null, null, "error"]);
      })
      .then((res: any) => {
        setApiResponse([res, null, "success"]);

        const persistData = cacheData ? url : null;
        fetchByNameInstance.current = fetchStore
          .set(name, {
            ...((fetchByNameInstance.current ??
              defaultFetchState) as CachedFetchState<any>),
            url: persistData,
            prevResponse: res,
          } as CachedFetchState<any>)
          .values()
          .next().value;
      });

    return () => {
      abortControllerInstance.abort();
    };
  }, [url, _ugly, name, cacheData]);

  const [_, __, status] = apiResponse;

  if (status === "querying" && Boolean(fetchStore.get(name))) {
    throw fetchStore.get(name)!.promises;
  }
  return apiResponse;
}
/**
 *manual remove cached data
 *
 * @export
 * @param {string} key
 * @returns
 */
export function clearStoreDataByUrl(key: string) {
  const slice = fetchStore.get(key);
  if (fetchStore.get(key)) {
    fetchStore.set(key, {
      ...slice,
      url: null,
    } as any);

    return true;
  }

  console.debug("store does not contains key ", key);
  return false;
}

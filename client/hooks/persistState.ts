export interface CachedFetchState<T> {
  promises: null | Promise<unknown>;
  prevResponse: T;
  url: string | null;
}
const defaultFetchState: CachedFetchState<any> = {
  promises: null,
  url: null,
  prevResponse: {},
};
const fetchStore = new Map<string, CachedFetchState<any>>();

export { fetchStore, defaultFetchState };

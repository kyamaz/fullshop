export = Shop;
export as namespace Shop;

declare namespace Shop {
  interface ProductModel {
    id: number;
    uuid: string;
    name: string;
    personnalized_content: string | null;
    category_id_fk: number | null;
    price_without_taxes: Prisma.Decimal | null;
    created_at: string;
    description_long: string | null;
    image_main_url: string;
    image_alt_url: string[];
    number_ratings: number | null;
    quantity_id_fk: number | null;
    sel_product_quantity?: Shop.ProductQuantityModel | null;
    ratings: string | null;
    updated_at: string;
    sel_quantity_product?: Shop.ProductQuantityModel | null;
    related_product: string[];
    upsale_product: string[];
    num_available: number | null;
    weight_gram: number;
    height_cm: number;
    type: string;
  }

  interface ProductQueryState {
    skip: number;
    take: number;
  }

  interface ProductCollectionRepositoryResponse<T> {
    data: T[];
    query: ProductQueryState;
  }

  interface ProductQuantityModel {
    id: number;
    num_available: number | null;
    sel_product_quantity: Shop.ProductModel | null;
    product_id_fk: string | null;
    sel_quantity_product: Shop.ProductModel | null;
  }
}

export interface ApiErrorResponse {
  msg: string;
  status: number;
}

export interface ApiOrderAddressPayload {
  id?: number;
  address: string;
  address_more: string;
  postal_code: string;
  city: string;
  country?: any;
  country_id: number;
  phone: string;
  orders?: any[];
}

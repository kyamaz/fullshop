import { FastifyInstance, FastifyRegisterOptions } from "fastify";
import {
  paymentCalculateGetController,
  paymentCreateController,
  paymentGetController,
  paymentIntentController,
} from "./payment.controller";
import {
  paymentCalculateGetSchema,
  paymentCreateSchema,
  paymentGetSchema,
  paymentIntentSchema,
} from "./payment.schema";
export function paymentRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  fastify.post(
    "/intent",
    { schema: paymentIntentSchema, preHandler: [fastify.authenticate] },
    paymentIntentController
  );

  fastify.post(
    "/",
    { schema: paymentCreateSchema, preHandler: [fastify.authenticate] },
    paymentCreateController
  );

  fastify.get(
    "/:payment_id",
    { schema: paymentGetSchema, preHandler: [fastify.authenticate] },
    paymentGetController
  );

  fastify.get(
    "/calculate-order/:order_id",
    { schema: paymentCalculateGetSchema, preHandler: [fastify.authenticate] },
    paymentCalculateGetController
  );

  done();
}

import { fetchCatch } from "@shop/shared_svc";
import { throwCustomError } from "@shop/shared_svc/build";
import { prisma } from "../app";

export function paymentSvcGet(id: number) {
  return prisma.paymentDetail.findFirst({
    where: {
      id: id,
    },
  });
}

export async function paymentSvcCreate({
  stripe_id,
  stripe_client_secret,
}: {
  stripe_id: string;
  stripe_client_secret: string;
}) {
  return prisma.paymentDetail.create({
    data: {
      stripe_id,
      stripe_client_secret,
    },
  });
}
interface ProductItem {
  id: string;
  quantity: number;
  uuid: string;
  price_without_taxes: string;
}
interface Fees {
  id: string;
  delivery_status: string;
  ship_date: null | string;
  estimated_delivery_date: string;
  delivered_date: string | null;
  guaranteed_service: boolean;
  carrier_track_id: string;
  carrier_id_fk: number;
  service_code: string;
  shipping_amount: string;
  insurance_amount: string;
  confirmation_amount: string;
  other_amount: string | null;
  tax_amount: string;
  currency: string;
  shipping_address_id_fk: number;
  orders_id_fk: string;
}
function sum(item: ProductItem) {
  return +(parseFloat(item.price_without_taxes) * item.quantity).toFixed(2);
}

function amountInCents(items: ProductItem[]) {
  const total = items.reduce((acc: number, curr: ProductItem) => {
    return acc + sum(curr);
  }, 0);

  return Math.floor(total * 100);
}
function roundToTwo(num: number) {
  return +(Math.round((num + "e+2") as any) + "e-2");
}
function total(subtotal: number, shipping: number, tax: number) {
  const taxed = roundToTwo(subtotal * (tax / 100));
  return subtotal + taxed + shipping;
}
/**
 * return amount as integer (stripe constraoin)
 *
 * @export
 * @param {string} order_id
 * @param {Record<string, string>} headers
 * @returns
 */
export function paymentSvcCalculateGet(
  order_id: string,
  headers: Record<string, string>
) {
  const details$ = fetchCatch<{ data: ProductItem[] }>(
    "http://order_svc:5020/svc/order/".concat(order_id, "/details"),
    {
      headers,
    }
  );

  const shippingFees$ = fetchCatch<{ data: Fees }>(
    "http://shipping_svc:5050/svc/shipping?order_id=".concat(order_id),
    {
      headers,
    }
  );

  return Promise.all([details$, shippingFees$])
    .then(([{ data: products }, { data: fees }]) => {
      const amount = amountInCents(products);
      const { tax_amount, shipping_amount, currency } = fees;

      const shippingInCents = parseInt(shipping_amount) * 100;
      return [
        Math.floor(total(amount, shippingInCents, parseInt(tax_amount))),
        currency,
      ];
    })

    .catch((e) => {
      console.warn("paymentSvcCalculateGet", e);
      return Promise.reject(
        throwCustomError("failed to calculate sum", "Internal Error", 500)
      );
    });
}

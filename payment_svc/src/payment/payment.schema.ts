export const paymentIntentSchema = {
  body: {
    type: "object",
    properties: {
      order_id: {
        type: "string",
      },
    },
  },
};

export const paymentCreateSchema = {
  body: {
    type: "object",
    required: ["stripe_id", "stripe_client_secret"],
    properties: {
      stripe_id: {
        type: "string",
      },
      stripe_client_secret: {
        type: "string",
      },
    },
  },
};
export const paymentGetSchema = {
  params: {
    type: "object",
    required: ["payment_id"],
    properties: {
      payment_id: {
        type: "string",
      },
    },
  },
};
export const paymentCalculateGetSchema = {
  params: {
    type: "object",
    required: ["order_id"],
    properties: {
      order_id: {
        type: "string",
      },
    },
  },
};

import { fetchCatch, throwCustomError } from "@shop/shared_svc/build";
import { FastifyReply, FastifyRequest } from "fastify";
import Stripe from "stripe";
import {
  paymentSvcCalculateGet,
  paymentSvcCreate,
  paymentSvcGet,
} from "./payment.svc";
interface ProductItem {
  id: string;
  quantity: number;
  uuid: string;
  price_without_taxes: string;
}

type paymentRequest = FastifyRequest<{
  Body: {
    order_id: string;
  };
}>;

const stripe = new Stripe(process.env.PRIVATE_STRIPE_KEY!, {
  apiVersion: "2022-08-01",
});

export async function paymentIntentController(
  request: paymentRequest,
  reply: FastifyReply
) {
  const { order_id } = request.body;
  const headers = {
    cookie: request.headers.cookie as string,
    "content-type": "application/json",
  };

  const calculateUri = "http://localhost:5040/svc/payment/calculate-order/".concat(
    order_id
  );
  return fetchCatch<{ data: [number, string] }>(calculateUri, {
    headers,
  }).then(({ data: [amount, currency] }) => {
    if (isNaN(amount)) {
      return throwCustomError("could not calcute payment", "bad request", 400);
    }

    return stripe.paymentIntents
      .create({
        amount,
        currency,
        automatic_payment_methods: {
          enabled: false,
        },
      })
      .then((stripResp) => {
        return reply.status(201).send({ data: stripResp });
      })
      .catch((e) => {
        console.warn(e, "paymentIntents");
        return throwCustomError(
          "failed to calculate payment intent",
          "Internal error",
          500
        );
      });
  });
}

type paymentCreateRequest = FastifyRequest<{
  Body: {
    stripe_id: string;
    stripe_client_secret: string;
  };
}>;

export async function paymentCreateController(
  request: paymentCreateRequest,
  reply: FastifyReply
) {
  const { stripe_id, stripe_client_secret } = request.body;

  return paymentSvcCreate({ stripe_id, stripe_client_secret })
    .then((data) => {
      return reply.status(201).send({ data });
    })
    .catch((e) => {
      return throwCustomError(e, "error", 500);
    });
}
type paymentGetRequest = FastifyRequest<{
  Params: {
    payment_id: string;
  };
}>;

export async function paymentGetController(
  request: paymentGetRequest,
  reply: FastifyReply
) {
  const { payment_id } = request.params;

  return paymentSvcGet(parseInt(payment_id))
    .then((data) => {
      return reply.status(200).send({ data });
    })
    .catch((e) => {
      return throwCustomError(e, "error", 500);
    });
}
type paymentCalculateGetRequest = FastifyRequest<{
  Params: {
    order_id: string;
  };
}>;
export async function paymentCalculateGetController(
  request: paymentCalculateGetRequest,
  reply: FastifyReply
) {
  const { order_id } = request.params;
  if (!order_id) {
    return throwCustomError("order id missing", "Bad request", 400);
  }

  const headers = {
    cookie: request.headers.cookie as string,
    "content-type": "application/json",
  };

  return paymentSvcCalculateGet(order_id, headers)
    .then((data) => {
      return reply.status(200).send({ data });
    })
    .catch((e) => {
      return throwCustomError(e, "error", 500);
    });
}

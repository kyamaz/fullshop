export interface CartSessionSlice {
  name: string;
  expire_date: string;
}

-- CreateTable
CREATE TABLE "paymentDetail" (
    "id" SERIAL NOT NULL,
    "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ(6),
    "stripe_id" TEXT NOT NULL,
    "stripe_client_secret" TEXT NOT NULL,

    CONSTRAINT "paymentDetail_pkey" PRIMARY KEY ("id")
);

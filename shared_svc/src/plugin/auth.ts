import {
  FastifyError,
  FastifyInstance,
  FastifyPluginOptions,
  FastifyReply,
  FastifyRequest,
} from "fastify";
import { throwCustomError } from "../errors";

declare module "fastify" {
  interface FastifyInstance {
    authenticate: any;
  }
}
export function authPlugin(
  fastify: FastifyInstance,
  opts: FastifyPluginOptions,
  done: (error?: FastifyError) => void
) {
  fastify.register(require("@fastify/jwt"), {
    secret: "supersecret",
    cookie: {
      cookieName: "token",
      signed: false,
    },
  });

  fastify.register(require("@fastify/cookie")),
    fastify.decorate(
      "authenticate",
      async function (
        request: FastifyRequest,
        reply: FastifyReply,
        done: Function
      ) {
        return (request as any)
          .jwtVerify()
          .then((res: any) => {
            return res;
          })
          .catch((err: any) => {
            console.debug("jwt verify failed", err);
            throwCustomError(err, "unauthorized", 401);
          });
      }
    );

  done();
}

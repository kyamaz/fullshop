export function getUtcDate() {
  return new Date(
    Date.UTC(
      new Date().getUTCFullYear(),
      new Date().getUTCMonth(),
      new Date().getUTCDate(),
      new Date().getUTCHours(),
      new Date().getUTCMinutes(),
      new Date().getUTCSeconds()
    )
  );
}
/**
 * simple wrapper to throw exception on fetch status not 200
 *
 * @export
 * @param {Promise<Response>} f
 * @returns
 */
export function fetchCatch<T>(
  input: RequestInfo,
  init?: RequestInit
): Promise<T> {
  return fetch(input, init).then((response) => {
    if (!response.ok) {
      return response.json().then((err) => {
        throw err;
      }) as Promise<T>;
    }

    return response.json() as Promise<T>;
  });
}

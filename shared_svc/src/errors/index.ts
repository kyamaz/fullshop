import { FastifyError } from "fastify";
export interface ErrorCustom {
  detail: string;
  statusCode: number;
  errorName: string;
  error?: string;
}

interface ErrorDefault {
  message: string;
}
function isErrorObj(obj: unknown) {
  return Object.prototype.toString.call(obj) === "[object Error]";
}

function isFastifyError(e: unknown) {
  return isErrorObj(e) && (e as Error).hasOwnProperty("validation");
}
function formatValidationError(err: FastifyError) {
  console.debug(err, "formatValidationError:debug");
  const base = err.validation
    ?.map((v) => v.instancePath.concat(" ", v?.message ?? ""))
    .join(".");
  return base ?? "something went wrong";
}

function formatCustomError(err: ErrorDefault) {
  const message = err?.message;
  if (typeof message !== "string") {
    return "something went wrong";
  }
  try {
    const e: ErrorCustom = JSON.parse(message);
    return e.detail ?? "Failed get e detail";
  } catch (e) {
    console.warn(message, "failed to parse");
    return message;
  }
}

function getCustomError(err: ErrorDefault): ErrorCustom {
  const message = err?.message;
  if (typeof message !== "string") {
    return {
      statusCode: 500,
      errorName: "Internal error",
      detail: "something went wrong ",
    };
  }

  try {
    return JSON.parse(message);
  } catch (e) {
    console.warn(e, "failed to parse ", message);
    return {
      statusCode: 500,
      errorName: "Internal error",
      detail: "something went wrong ",
    };
  }
}

function getFastifyErroStatus(err: FastifyError) {
  if (err.hasOwnProperty("validation")) {
    return 400;
  }

  return 500;
}

export function setApiErrorResp(error: FastifyError | ErrorDefault) {
  const message = isFastifyError(error)
    ? formatValidationError(error as FastifyError)
    : formatCustomError(error as ErrorDefault);

  const status = isFastifyError(error)
    ? getFastifyErroStatus(error as FastifyError)
    : getCustomError(error).statusCode;

  const errorName = isFastifyError(error)
    ? (error as FastifyError).name
    : getCustomError(error).errorName;
  return { status, message, error, errorName };
}
export function setCutomError(m: string, errorName: string, statusCode = 500) {
  return { detail: m, errorName, statusCode } as ErrorCustom;
}
export function throwCustomError(
  msg: string,
  errorName: string,
  status: number
) {
  throw new Error(JSON.stringify(setCutomError(msg, errorName, status)));
}

export function notFoundErrror() {
  return throwCustomError("route not found", "not found", 404);
}

export function isErrorCustom<T>(e: ErrorCustom | T): e is ErrorCustom {
  return (e as any)?.errorName && (e as any)?.statusCode;
}

import { FastifyInstance, FastifyRegisterOptions } from "fastify";
import {
  cartSessionCreateController,
  cartSessionDeleteController,
  cartSessionGetController,
} from "./cart-session.controller";
import { cartSessionCreateSchema } from "./cart-session.schema";
export function cartSessionRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  //  fastify.addHook("preHandler", protectedRoute);

  fastify.get(
    "/cart-session",
    { attachValidation: false },
    cartSessionGetController
  );
  fastify.post(
    "/cart-session",
    { schema: cartSessionCreateSchema },
    cartSessionCreateController
  );

  fastify.delete(
    "/cart-session",
    { attachValidation: false },
    cartSessionDeleteController
  );

  done();
}

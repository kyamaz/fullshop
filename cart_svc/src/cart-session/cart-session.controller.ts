import { getUtcDate, throwCustomError } from "@shop/shared_svc";
import { FastifyReply, FastifyRequest } from "fastify";
import { CartSessionState } from "../app";
type CartSessionRequest = FastifyRequest<{ Body: string }>;

export function cartSessionGetController(
  request: FastifyRequest,
  reply: FastifyReply
) {
  const session = CartSessionState.getSession();

  if (!session) {
    return throwCustomError("session not found", "Not found", 404);
  }

  const expireTime = new Date(session.expire_date as string).getTime();
  const now = getUtcDate().getTime();

  if (now >= expireTime) {
    CartSessionState.clearSession();
    return throwCustomError("session expired", "Gone", 410);
  }
  reply.status(200).send(session);
}

export function cartSessionCreateController(
  request: CartSessionRequest,
  reply: FastifyReply
) {
  CartSessionState.setSession(JSON.parse(request.body as any));

  reply.status(201).send({ msg: "ok" });
}

export function cartSessionDeleteController(
  request: CartSessionRequest,
  reply: FastifyReply
) {
  CartSessionState.clearSession();
  reply.status(200).send({ msg: "ok" });
}

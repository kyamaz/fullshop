import { PrismaClient } from ".prisma/client";
import cors from "@fastify/cors";
import { setApiErrorResp, throwCustomError } from "@shop/shared_svc/build";
import fastify, { FastifyRequest } from "fastify";
import { cartSessionRoute } from "./cart-session/cart-session.routes";
import { CartSessionSlice } from "./model/index";

const server = fastify({
  logger: false,
});

server.register(cors, {
  origin: true, //TODO dev only
  credentials: true,
});

//RO */

// global error response handler

server.setErrorHandler(function (err, _request, reply) {
  //  console.warn(error);
  const { status, message, errorName, error } = setApiErrorResp(err);
  reply.status(status).send({ message, statusCode: status, errorName, error });
});

//ROUTES
server.register(cartSessionRoute, { prefix: "/svc" });

server.get("*", function (req: FastifyRequest, res: any) {
  console.warn(req.url, "cart svc url not found");
  throwCustomError(req.url.concat(" not found"), "Not found", 404);
});

const prisma = new PrismaClient();

class CartStateMap {
  private session: CartSessionSlice | null = null;
  constructor() {}

  getSession() {
    return this.session;
  }
  setSession(payload: any) {
    this.session = payload;
  }

  clearSession() {
    this.session = null;
  }
}
const CartSessionState = new CartStateMap();

export { server, prisma, CartSessionState };

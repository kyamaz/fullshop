-- CreateTable
CREATE TABLE "cart" (
    "id" SERIAL NOT NULL,
    "products_selection" JSON[],
    "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ(6) NOT NULL,
    "expires_at" TIMESTAMPTZ(6),

    CONSTRAINT "stock_pkey" PRIMARY KEY ("id")
);

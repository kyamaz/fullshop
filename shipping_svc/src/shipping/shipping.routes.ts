import { FastifyInstance, FastifyRegisterOptions } from "fastify";
import {
  shippingCreateController,
  shippingDeliveryServiceGetController,
  shippingEstimateController,
  shippingGetController,
  shippingGetFilterController,
  shippingPatchController,
} from "./shipping.controller";
import {
  shippingCreateSchema,
  shippingEstimateSchema,
  shippingGetFilterSchema,
  shippingGetSchema,
  shippingPatchSchema,
} from "./shipping.schema";
export function shippingRoute(
  fastify: FastifyInstance,
  _: FastifyRegisterOptions<unknown>,
  done: Function
) {
  fastify.post(
    "/estimate",
    { schema: shippingEstimateSchema, preHandler: [fastify.authenticate] },
    shippingEstimateController
  );

  fastify.post(
    "/create",
    { schema: shippingCreateSchema, preHandler: [fastify.authenticate] },
    shippingCreateController
  );

  fastify.patch(
    "/update",
    { schema: shippingPatchSchema, preHandler: [fastify.authenticate] },
    shippingPatchController
  );

  fastify.get(
    "/:shipping_id",
    { schema: shippingGetSchema, preHandler: [fastify.authenticate] },
    shippingGetController
  );

  fastify.get(
    "",
    { schema: shippingGetFilterSchema, preHandler: [fastify.authenticate] },
    shippingGetFilterController
  );

  fastify.get(
    "/delivery-service",
    { preHandler: [fastify.authenticate] },
    shippingDeliveryServiceGetController
  );

  done();
}

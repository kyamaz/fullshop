import { getUtcDate } from "@shop/shared_svc";
import { prisma } from "../app";
import {
  ShippingCreatePayload,
  ShippingEstimatePayload,
  ShippingPatchPayload,
} from "./../model/index";

function randomIntFromInterval(min: number, max: number) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

export function shippingSvcEstimate(payload: ShippingEstimatePayload) {
  const rand = randomIntFromInterval(1, 4);
  const estimated_delivery = getUtcDate();
  estimated_delivery.setDate(new Date().getDate() + rand);
  const carrier = randomIntFromInterval(1, 3);

  const ship_from = {
    address: "string",
    address_more: null,
    postal_code: "666",
    city: "hell",
    country_id: 1,
    phone: "dede",
  };

  return Promise.resolve({
    service_code: payload.service_code,
    guaranteed_service: true,
    trackable: true,
    ship_date: getUtcDate().toISOString(),
    estimated_delivery_date: estimated_delivery.toISOString(),
    delivery_days: rand,
    carrier_id_fk: carrier,
    carrier_name: "carrier",
    shipping_amount: randomIntFromInterval(1, 20),
    insurance_amount: randomIntFromInterval(1, 9),
    confirmation_amount: randomIntFromInterval(0, 7),
    other_amount: null,
    tax_amount: randomIntFromInterval(1, 3),
    currency: "eur",
    negotiated_rate: true,
  });
}

export function shippingSvcCreate(payload: ShippingCreatePayload) {
  const formatted = {
    service_code: payload.service_code,
    guaranteed_service: payload.guaranteed_service,
    estimated_delivery_date: payload.estimated_delivery_date,
    carrier_id_fk: payload.carrier_id_fk,
    shipping_amount: payload.shipping_amount,
    insurance_amount: payload.insurance_amount,
    confirmation_amount: payload.confirmation_amount,
    other_amount: payload.other_amount,
    tax_amount: payload.tax_amount,
    currency: payload.currency,
    negotiated_rate: payload.negotiated_rate,
    shipping_address_id_fk: payload.shipping_address_id_fk,
    orders_id_fk: payload.orders_id_fk,
    delivery_days: payload.delivery_days,
  };

  return prisma.shippingDetails.create({
    data: formatted,
  });
}

export function shippingSvcPatch(payload: Partial<ShippingPatchPayload>) {
  return prisma.shippingDetails.update({
    data: payload as any,
    where: { id: payload.id },
  });
}

export function shippingSvcGet(id: string) {
  return prisma.shippingDetails.findFirstOrThrow({
    where: {
      id: id,
    },
    select: {
      id: true,
      delivery_status: true,
      ship_date: true,
      estimated_delivery_date: true,
      delivered_date: true,
      guaranteed_service: true,
      carrier_track_id: true,
      carrier_id_fk: true,
      service_code: true,
      shipping_amount: true,
      insurance_amount: true,
      confirmation_amount: true,
      other_amount: true,
      tax_amount: true,
      currency: true,
      negotiated_rate: true,
      shipping_address_id_fk: true,
      orders_id_fk: true,
      sel_shipping_carrier: true,
      delivery_days: true,
    },
  });
}

export function shippingSvcGetFilter({ order_id }: { order_id: string }) {
  return prisma.shippingDetails.findFirstOrThrow({
    where: {
      orders_id_fk: order_id,
    },
    select: {
      id: true,
      delivery_status: true,
      ship_date: true,
      estimated_delivery_date: true,
      delivered_date: true,
      guaranteed_service: true,
      carrier_track_id: true,
      carrier_id_fk: true,
      service_code: true,
      shipping_amount: true,
      insurance_amount: true,
      confirmation_amount: true,
      other_amount: true,
      tax_amount: true,
      currency: true,
      negotiated_rate: true,
      shipping_address_id_fk: true,
      orders_id_fk: true,
      sel_shipping_carrier: true,
      delivery_days: true,
    },
  });
}

export function shippingSvcGetService() {
  return Promise.resolve([
    {
      value: "normal",
    },
    {
      value: "priority",
    },
    {
      value: "pickup",
    },
  ]);
}

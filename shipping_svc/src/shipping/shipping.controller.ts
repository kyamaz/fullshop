import { throwCustomError } from "@shop/shared_svc/build";
import { FastifyReply, FastifyRequest } from "fastify";
import {
  ShippingCreatePayload,
  ShippingEstimatePayload,
  ShippingPatchPayload,
} from "./../model/index";
import {
  shippingSvcCreate,
  shippingSvcEstimate,
  shippingSvcGet,
  shippingSvcGetFilter,
  shippingSvcGetService,
  shippingSvcPatch,
} from "./shipping.svc";

type shippingIntentRequest = FastifyRequest<{
  Body: ShippingEstimatePayload;
}>;

export async function shippingEstimateController(
  request: shippingIntentRequest,
  reply: FastifyReply
) {
  return shippingSvcEstimate(request.body).then((estimate) => {
    return reply.status(200).send({ data: estimate });
  });
}

type shippingCreateRequest = FastifyRequest<{
  Body: ShippingCreatePayload;
}>;

export async function shippingCreateController(
  request: shippingCreateRequest,
  reply: FastifyReply
) {
  return shippingSvcCreate(request.body)
    .then((shipping) => {
      return reply.status(201).send({ data: shipping });
    })
    .catch((e) => {
      console.warn(e);
      return throwCustomError("oups", "internal error", 500);
    });
}

type shippingDetailRequest = FastifyRequest<{
  Params: {
    shipping_id: string;
  };
}>;

export async function shippingGetController(
  request: shippingDetailRequest,
  reply: FastifyReply
) {
  return shippingSvcGet(request.params.shipping_id)
    .then((shipping) => {
      return reply.status(200).send({ data: shipping });
    })
    .catch((e) => {
      console.warn(e);
      return throwCustomError("oups", "not found", 404);
    });
}

export async function shippingGetFilterController(
  request: FastifyRequest<{
    Querystring: { order_id };
  }>,
  reply: FastifyReply
) {
  const { order_id } = request.query;

  if (!order_id) {
    return throwCustomError("query missing", "Bad request", 400);
  }
  return shippingSvcGetFilter({ order_id })
    .then((shipping) => {
      return reply.status(200).send({ data: shipping });
    })
    .catch((e) => {
      console.warn(e);
      return throwCustomError("oups", "not found", 404);
    });
}

export async function shippingPatchController(
  request: FastifyRequest<{
    Body: ShippingPatchPayload;
  }>,
  reply: FastifyReply
) {
  return shippingSvcPatch(request.body)
    .then((shipping) => {
      return reply.status(200).send({ data: shipping });
    })
    .catch((e) => {
      console.warn(e);
      return throwCustomError("oups", "internal error", 500);
    });
}

export async function shippingDeliveryServiceGetController(
  request: FastifyRequest,
  reply: FastifyReply
) {
  return shippingSvcGetService().then((svc) => {
    return reply.status(200).send({ data: svc });
  });
}

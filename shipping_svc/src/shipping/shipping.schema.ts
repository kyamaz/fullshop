export const shippingEstimateSchema = {
  body: {
    type: "object",
    properties: {
      ship_to: {
        type: "object",
        $ref: "#/$defs/address",
      },

      packages: {
        type: "array",
        items: { $ref: "#/$defs/weight" },
      },
      service_code: {
        type: "string",
      },
    },
    $defs: {
      address: {
        type: "object",
        //TOOO required id update
        properties: {
          id: {
            type: "number",
          },

          address: {
            type: "string",
          },
          address_more: {
            type: "string",
          },
          postal_code: {
            type: "string",
          },
          city: {
            type: "string",
          },
          country_id: {
            type: "number",
          },
          phone: {
            type: "string",
          },
        },
      },

      weight: {
        type: "object",
        properties: {
          value: {
            type: "number",
          },
          unit: {
            type: "string",
          },
        },
      },
    },
  },
};

export const shippingCreateSchema = {
  body: {
    type: "object",
    required: [
      "carrier_id_fk",
      "shipping_amount",
      "orders_id_fk",
      "shipping_address_id_fk",
      "currency",
    ],
    properties: {
      estimated_delivery_date: {
        type: "string",
      },
      carrier_id_fk: {
        type: "number",
      },
      service_code: {
        type: "string",
      },
      shipping_amount: {
        type: "number",
      },
      insurance_amount: {
        type: "number",
      },
      confirmation_amount: {
        type: "number",
      },
      other_amount: {
        type: "number",
        nullable: true,
      },
      tax_amount: {
        type: "number",
      },
      currency: {
        type: "string",
      },

      negotiated_rate: {
        type: "boolean",
      },
      guaranteed_service: {
        type: "boolean",
      },
      shipping_address_id_fk: {
        type: "number",
      },

      orders_id_fk: {
        type: "string",
      },
      carrier_track_id: {
        type: "string",
        nullable: true,
      },
      delivery_days: {
        type: "number",
      },
    },
  },
};

export const shippingPatchSchema = {
  body: {
    type: "object",
    required: ["id"],
    properties: {
      id: {
        type: "string",
      },
      estimated_delivery_date: {
        type: "string",
      },
      delivery_status: {
        type: "string",
      },
      ship_date: {
        type: "string",
      },
      delivered_date: {
        type: "string",
      },
      carrier_track_id: {
        type: "string",
        nullable: true,
      },
      delivery_days: {
        type: "number",
      },
    },
  },
};

export const shippingGetSchema = {
  params: {
    type: "object",
    properties: {
      shipping_id: {
        type: "string",
      },
    },
  },
};

export const shippingGetFilterSchema = {
  querystring: {
    type: "object",
    properties: {
      order_id: {
        type: "string",
      },
    },
  },
};

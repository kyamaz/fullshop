import { ApiOrderAddressPayload } from "@shop/shared_types";

export interface Package {
  weight: {
    unit: string;
    value: number;
  };
}

export interface ShippingEstimatePayload {
  ship_to: ApiOrderAddressPayload;
  packages: Package[];
  service_code: string;
}
type ServiceCode = "priority" | "normal" | "pickup";

export interface ShippingRateEstimationPayload {
  service_code: ServiceCode;
  guaranteed_service: boolean;
  trackable: boolean;

  ship_date: string;
  estimated_delivery_date: string;
  delivery_days: number;

  carrier_id: number;
  carrier_name: string;

  shipping_amount: number;
  insurance_amount: number;
  confirmation_amount: number;
  other_amount: number | null;
  tax_amount: number;
  currency: string;
  negotiated_rate: boolean;
}

export interface ShippingCreatePayload {
  id?: string;
  service_code: ServiceCode;
  guaranteed_service: boolean;

  estimated_delivery_date: string;
  carrier_track_id?: string;

  carrier_id_fk: number;

  shipping_amount: number;
  insurance_amount: number;
  confirmation_amount: number;
  other_amount: number | null;
  tax_amount: number;
  currency: string;
  negotiated_rate: boolean;
  shipping_address_id_fk: number;
  orders_id_fk: string;
  delivery_days: number;
}
export interface ShippingPatchPayload {
  id: string;
  estimated_delivery_date?: string;
  delivery_status?: string;
  ship_date?: string;
  delivered_date?: string;
  carrier_track_id?: string | null;
  delivery_days?: number | number;
}

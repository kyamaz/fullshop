import { PrismaClient } from ".prisma/client";
import cors from "@fastify/cors";
import {
  authPlugin,
  setApiErrorResp,
  throwCustomError,
} from "@shop/shared_svc/build";
import fastify, { FastifyRequest } from "fastify";
import fp from "fastify-plugin";
import { shippingRoute } from "./shipping/shipping.routes";

const server = fastify({
  logger: false,
});

server.register(cors, {
  origin: true, //TODO dev only
  credentials: true,
});

server.register(fp(authPlugin));

// global error response handler

server.setErrorHandler(function (err, _request, reply) {
  //  console.warn(error);
  const { status, message, errorName, error } = setApiErrorResp(err);
  reply.status(status).send({ message, statusCode: status, errorName, error });
});

//ROUTES
server.register(shippingRoute, { prefix: "/svc/shipping" });

server.get("*", function (req: FastifyRequest, res: any) {
  console.warn(req.url, "payment svc url not found");
  throwCustomError(req.url.concat(" not found"), "Not found", 404);
});
const prisma = new PrismaClient();
export { server, prisma };

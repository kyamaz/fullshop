-- CreateEnum
CREATE TYPE "ShippingStatus" AS ENUM ('CREATED', 'SHIPPED', 'CARRIER_HANDOVER', 'CARRIER_DELIVERING', 'CUSTOMER_HANDOVER', 'CUSTOMER_ABSENT', 'ERROR');

-- CreateEnum
CREATE TYPE "ServiceCode" AS ENUM ('priority', 'normal', 'pickup');

-- CreateTable
CREATE TABLE "shippingDetails" (
    "id" SERIAL NOT NULL,
    "created_at" TIMESTAMPTZ(6) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMPTZ(6),
    "delivery_status" "ShippingStatus" NOT NULL DEFAULT 'CREATED',
    "delivered_date" TIMESTAMPTZ(6),
    "carrier_id_fk" INTEGER NOT NULL,
    "carrier_ship_date" TIMESTAMPTZ(6),
    "freight_cost" MONEY,
    "service_code" "ServiceCode" NOT NULL,
    "shipping_address_id_fk" INTEGER NOT NULL,
    "user_id_fk" TEXT NOT NULL,
    "orders_id_fk" TEXT NOT NULL,

    CONSTRAINT "shippingDetails_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "carrierDetails" (
    "id" SERIAL NOT NULL,
    "name" TEXT NOT NULL,

    CONSTRAINT "carrierDetails_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "shippingDetails" ADD CONSTRAINT "shippingDetails_carrier_id_fk_fkey" FOREIGN KEY ("carrier_id_fk") REFERENCES "carrierDetails"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

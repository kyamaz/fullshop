/*
  Warnings:

  - You are about to drop the column `carrier_ship_date` on the `shippingDetails` table. All the data in the column will be lost.
  - You are about to drop the column `freight_cost` on the `shippingDetails` table. All the data in the column will be lost.
  - You are about to drop the column `user_id_fk` on the `shippingDetails` table. All the data in the column will be lost.
  - Added the required column `confirmation_amount` to the `shippingDetails` table without a default value. This is not possible if the table is not empty.
  - Added the required column `currency` to the `shippingDetails` table without a default value. This is not possible if the table is not empty.
  - Added the required column `estimated_delivery_date` to the `shippingDetails` table without a default value. This is not possible if the table is not empty.
  - Added the required column `guaranteed_service` to the `shippingDetails` table without a default value. This is not possible if the table is not empty.
  - Added the required column `insurance_amount` to the `shippingDetails` table without a default value. This is not possible if the table is not empty.
  - Added the required column `negotiated_rate` to the `shippingDetails` table without a default value. This is not possible if the table is not empty.
  - Added the required column `shipping_amount` to the `shippingDetails` table without a default value. This is not possible if the table is not empty.
  - Added the required column `tax_amount` to the `shippingDetails` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "shippingDetails" DROP COLUMN "carrier_ship_date",
DROP COLUMN "freight_cost",
DROP COLUMN "user_id_fk",
ADD COLUMN     "carrier_track_id" TEXT,
ADD COLUMN     "confirmation_amount" MONEY NOT NULL,
ADD COLUMN     "currency" TEXT NOT NULL,
ADD COLUMN     "estimated_delivery_date" TIMESTAMPTZ(6) NOT NULL,
ADD COLUMN     "guaranteed_service" BOOLEAN NOT NULL,
ADD COLUMN     "insurance_amount" MONEY NOT NULL,
ADD COLUMN     "negotiated_rate" BOOLEAN NOT NULL,
ADD COLUMN     "other_amount" MONEY,
ADD COLUMN     "ship_date" TIMESTAMPTZ(6),
ADD COLUMN     "shipping_amount" MONEY NOT NULL,
ADD COLUMN     "tax_amount" MONEY NOT NULL;

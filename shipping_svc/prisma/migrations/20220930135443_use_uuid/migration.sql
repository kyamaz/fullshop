/*
  Warnings:

  - The primary key for the `shippingDetails` table will be changed. If it partially fails, the table could be left without primary key constraint.

*/
-- AlterTable
ALTER TABLE "shippingDetails" DROP CONSTRAINT "shippingDetails_pkey",
ALTER COLUMN "id" DROP DEFAULT,
ALTER COLUMN "id" SET DATA TYPE TEXT,
ADD CONSTRAINT "shippingDetails_pkey" PRIMARY KEY ("id");
DROP SEQUENCE "shippingDetails_id_seq";
